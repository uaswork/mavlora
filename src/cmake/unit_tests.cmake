include_directories(${PROJECT_SOURCE_DIR}/core)

add_executable(unit_tests_runner
    ${UNIT_TEST_SOURCES}
)

target_compile_definitions(unit_tests_runner PRIVATE FAKE_TIME=1)

target_link_libraries(unit_tests_runner
    mavlora
    gtest
    gtest_main
    gmock
)

add_test(unit_tests unit_tests_runner)