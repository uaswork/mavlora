/*
 * Copyright (c) 2019, Frederik Mazur Andersen <fm@mmmi.sdu.dk>/<fm@fmazur.dk>
 *
 * SDU UAS Center, http://sdu.dk/uas, University of Southern Denmark 
 * All rights reserved.
 * 
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE.txt', which is part of this source code package.
 */

#include <utility>
#include <ctime>
#include <sys/time.h>

#include "mavlora.hpp"
#include "version.h"
#include "checksum.h"

namespace mavlora {

    Mavlora::Mavlora() {
        /* initialize variables */
        msg_rx_cnt = 0;
        msg_crc_err = 0;
        msg_tx_cnt = 0;
        msg_buf_overflow = 0;
        tx_seq = 1;
        recorded_sysid = 0;
        param_mission_tout = 0;
    }

    std::string Mavlora::version() const
    {
        return mavlora_version;
    }

    void Mavlora::init(std::string identify_name, unsigned char sys_id, std::function<void(unsigned char *)> callback)
    {
        //save name
        identifier_name = std::move(identify_name);

        //save sys id
        my_sysid = sys_id;

        //save callback
        parse_msg_callback = callback;
    }

    bool Mavlora::enableCommunicationUDP(std::string udp_target, int port) {

        // open udp socket
        return enableCommunicationUDP(udp_target, port, port);
    }

    bool Mavlora::enableCommunicationUDP(std::string udp_target, int port, int listen_port) {

        // open udp socket
        udp_client = UDPsocket(std::move(udp_target), port);
        bool result = udp_client.enablelisten(listen_port, rxbuf_socket_read, RX_BUF_SIZE);
        if (result)
            udp_active = true;

        return result;
    }

    void Mavlora::disableCommunicationUDP() {
        udp_active = false;

        // stop listening on UDP. No need to remove client, as its not connection bound
        udp_client.disableListen();
    }

    bool Mavlora::enableCommunicationSerial(std::string serial_dev, int baudrate) {
        //can only enable if not already enabled
        if (serial_active)
        {
            std::cout << "Serial communication is already active..." << std::endl;
            return false;
        }

        serial_device = std::move(serial_dev);

        printf("Opening serial device for %s: %s baudrate %d \n", identifier_name.c_str(), serial_device.c_str(), baudrate);
        if (ser_open(&ser_ref, &oldtio, (char *) serial_device.c_str(), baudrate)) {
            std::cout << "port couldn't be opened" << std::endl;
            return false;
        } else
        {
            //set variable for serial active
            serial_active = true;
            return true;
        }
    }

    void Mavlora::disableCommunicationSerial() {
        //closing serial communication
        ser_close(ser_ref, oldtio);
        serial_active = false;
    }

    int Mavlora::reopen_serial()
    {
        printf("Opening serial device for %s: %s baudrate %d \n", identifier_name.c_str(), serial_device.c_str(), baudrate);
        if (ser_open(&ser_ref, &oldtio, (char *) serial_device.c_str(), baudrate)) {
            std::cout << "port couldn't be opened" << std::endl;
            return -1;
        }
        return 1;
    }
    void Mavlora::update()
    {
        // TODO we can only handle one active at a time, as they both put bytes into the same rx_buffer in our serialization.
        //  ideally we should maybe change the flow, so update function checks for packages parallel in their own buffers and only copy the
        //  received messages over to the internal rx-buffer.

        // update what is connected
        if (serial_active)
        {
            int cnt = ser_receive(ser_ref, rxbuf_serial_read, RX_BUF_SIZE);
            if(cnt > 0)
            {
                ml_rx_update(millis(), rxbuf_serial_read, cnt);
            }
        }

        //udp update
        if (udp_active)
        {
            int cnt = udp_client.receiveData();
            if (cnt > 0)
            {
                ml_rx_update(millis(), rxbuf_socket_read, cnt);
            }
        }
    }
    void Mavlora::shutdown()
    {
        // close comm interfaces if they are active
        if (udp_active) {
            disableCommunicationUDP();
        }
        if (serial_active) {
            disableCommunicationSerial();
        }

        // clear serial count
        rxbuf_cnt = 0;
        txbuf_cnt = 0;
        msg_begin = -1;
    }
/***************************************************************************/
/// Base functions for queue is here, but individual messages queued is placed in mavlink_queue.cpp
    void Mavlora::ml_queue_msg(unsigned char *buf, unsigned char sys_id)
    {
        unsigned char msg_id;
        unsigned char payload_len;
        unsigned short crc;
        unsigned char crc_extra;

        msg_tx_cnt++;

        /* encode the generic part of the header */
        buf[ML_POS_IDENT] = ML_NEW_PACKET_IDENT_V10;

        // if seq is max,
        if (tx_seq == 255)
            tx_seq = 1;

        /* Set my sys_id. From what system id are we sending */
        buf[ML_POS_SYS_ID] = sys_id;

        /* Set sequence id */
        buf[ML_POS_PACKET_SEQ] = tx_seq;

        // increment sequence
        tx_seq++;

        /* add checksum */
        msg_id = buf[ML_POS_MSG_ID];
        payload_len = buf[ML_POS_PAYLOAD_LEN];
        crc = crc_calculate(buf+1, payload_len+5);
        crc_extra = MAVLINK_MESSAGE_CRCS_V1[msg_id];
        crc_accumulate(crc_extra, &crc);
        (buf+payload_len+ 6)[0] = (crc & 0xff);
        (buf+payload_len+ 7)[0] = (crc >> 8);

        /* update txbuf length */
        txbuf_cnt += buf[ML_POS_PAYLOAD_LEN] + 8;

        // update tx aka. send the bytes
        ml_tx_update();
    }
    void Mavlora::ml_queue_msg_generic (unsigned char sys_id, unsigned char comp_id, unsigned char msg_id, unsigned char payload_len, unsigned char *payload)
    {
        unsigned char i, len;
        unsigned char *buf = (txbuf + txbuf_cnt);

        /* encode part of the header */
        buf[ML_POS_PAYLOAD_LEN] = payload_len;
        buf[ML_POS_COMP_ID] = comp_id;
        buf[ML_POS_MSG_ID] = msg_id;

        /* param_index */
        for (i=0; i<payload_len; i++)
            buf[ML_POS_PAYLOAD + i] = payload[i];

        /* queue message */
        recorded_sysid = 0;
        ml_queue_msg(buf, sys_id);
    }
    void Mavlora::ml_forward_msg(unsigned char *inc_buf)
    {
        unsigned char *buf = (txbuf + txbuf_cnt);

        uint8_t payload_len = inc_buf[ML_POS_PAYLOAD_LEN];
        uint8_t sys_id = inc_buf[ML_POS_SYS_ID];

        buf[ML_POS_PAYLOAD_LEN] = payload_len;
        buf[ML_POS_COMP_ID] = inc_buf[ML_POS_COMP_ID];
        buf[ML_POS_MSG_ID] = inc_buf[ML_POS_MSG_ID];

        for (auto i=0; i<payload_len; i++) {
            buf[ML_POS_PAYLOAD + i] = inc_buf[ML_POS_PAYLOAD + i];
        }

        /* queue message */
//    recorded_sysid = 0;
        ml_queue_msg(buf, sys_id);
    }
/***************************************************************************/
    unsigned long Mavlora::ml_messages_sent()
    {
        return msg_tx_cnt;
    }
    unsigned long Mavlora::ml_messages_received()
    {
        return msg_rx_cnt;
    }
    unsigned long Mavlora::ml_messages_crc_error()
    {
        return msg_crc_err;
    }
    unsigned long Mavlora::ml_messages_lost(unsigned int sys_id)
    {
        return lost_messages_map[sys_id].lostMessages;
    }
    unsigned long Mavlora::millis()
    {
        struct timeval te;
        gettimeofday(&te, nullptr); /* get current time */

        if (secs_init == 0)
        {
            secs_init = te.tv_sec;
        }

        return ((unsigned long) (te.tv_sec - secs_init)*1000 + te.tv_usec/1000);
    }
/***************************************************************************/
    void Mavlora::ml_tx_update()
    {
        //send on the interfaces activated
        if (udp_active) {
            udp_client.sendData(txbuf, txbuf_cnt);
        }

        if (serial_active) {
            ser_send(ser_ref, txbuf, txbuf_cnt);
        }

        // reset tx count
        txbuf_cnt = 0;
    }

    short Mavlora::ml_rx_update(unsigned long now, unsigned char *rxbuf_new, short rxbuf_new_cnt)
    {
        char result = 0;
        short i, j, count;
        unsigned char c;

        /* check for buffer owerflow */
        if (rxbuf_cnt + rxbuf_new_cnt > RX_BUF_SIZE)
        {
            rxbuf_cnt = 0;
            result = -1;
            msg_buf_overflow++;
            if (debug)
                std::cout << "Buffer overflow" << std::endl;
        }
        else
        {
            short seek_from = rxbuf_cnt;
            char maybe_more = 1;
            txbuf_cnt = 0;

            /* add new bytes to buffer */
            memcpy (rxbuf+rxbuf_cnt , rxbuf_new, rxbuf_new_cnt);
            rxbuf_cnt += rxbuf_new_cnt;

            while (maybe_more == 1)
            {
                maybe_more = 0;
                if (msg_begin < 0) /* try to find a packet start */
                {
                    for (i=seek_from; i<rxbuf_cnt; i++)
                    {
                        if (rxbuf[i] == ML_NEW_PACKET_IDENT_V10 && msg_begin < 0)
                            msg_begin = i;
                    }
                }

                /* if we have found a packet start and the packet len > minimum */
                if (msg_begin >= 0 && rxbuf_cnt >= msg_begin + 8)
                {
                    short payload_len = rxbuf[msg_begin + ML_POS_PAYLOAD_LEN];
                    short msg_next = msg_begin + payload_len + 8; /* actually beginning of next */

                    /* if we have a complete packet */
                    if (rxbuf_cnt >= msg_next)
                    {
                        unsigned char crc_ok;
                        unsigned char msg_id = rxbuf[msg_begin + ML_POS_MSG_ID];
                        unsigned char msg_seq = rxbuf[msg_begin + ML_POS_PACKET_SEQ];
                        unsigned char sys_id = rxbuf[msg_begin + ML_POS_SYS_ID];

                        /* if the checksum is valid */
                        unsigned char crc_lsb = rxbuf[msg_begin + payload_len + 6];
                        unsigned char crc_msb = rxbuf[msg_begin + payload_len + 7];
                        unsigned short crc = crc_calculate(rxbuf+msg_begin+1, payload_len+5);
                        unsigned char crc_extra = MAVLINK_MESSAGE_CRCS_V1[msg_id];
                        crc_accumulate(crc_extra, &crc);
                        crc_ok = ((crc & 0xff) == crc_lsb && (crc >> 8) == crc_msb);

                        unsigned char payload_len = rxbuf[msg_begin + ML_POS_PAYLOAD_LEN];
                        unsigned char seq = rxbuf[msg_begin + ML_POS_PACKET_SEQ];

                        // debug
//                        if (!crc_ok)
//                        {
//                            std::cout << "crc result: " << std::to_string(crc_ok) << ", msg_id: " << std::to_string(msg_id) << ", crc lsb: " << std::to_string((crc & 0xff) == crc_lsb) << ", crc_msb: " << std::to_string((crc >> 8) == crc_msb) << std::endl;
//                            std::cout << "crc_lsb: " << std::to_string(crc_lsb) << ", crc_msb: " << std::to_string(crc_msb) << ", crc_ekstra: " << std::to_string(crc_extra) << std::endl;
//                        }

                        if (crc_ok)
                        {
                            msg_rx_cnt++;
                            do_send_msg = 1;

                            // get lost msg map for this sys_id
                            auto& lost_msgs_ref = lost_messages_map[sys_id];

                            /* if first time record the sys_id */
                            if (msg_id == 0 && recorded_sysid == 0)
                            {
                                recorded_sysid = sys_id;
                            }

                            // set next sequence expected for first time
                            if (lost_msgs_ref.last_seq == 0)
                                lost_msgs_ref.last_seq = msg_seq;

                            // Manage check for lost messages
                            if (msg_seq > lost_msgs_ref.last_seq + 1)
                            {
                                // we have lost packages
                                auto numLost = msg_seq - (lost_msgs_ref.last_seq + 1);
                                lost_msgs_ref.lostMessages += numLost;
                            }

                            // save current msg as last msg
                            lost_msgs_ref.last_seq = msg_seq;

                            /* check if param or mission sequence is ongoing */
                            if (do_send_msg == 1)
                            {
                                if (msg_id==20 || msg_id==21 || msg_id==22 || msg_id==23) /* param msgs */
                                {
                                    param_mission_tout = now + PARAM_TOUT;
                                }
                                else if (msg_id==37 || msg_id==39 || msg_id==40 || msg_id==43 || msg_id==44 || msg_id==47) /* mission item transactions */
                                {
                                    param_mission_tout = now + MISSION_TOUT;
                                }
                                else if (msg_id != 0)  /* heartbeat must get through */
                                {
                                    if (now < param_mission_tout)
                                        do_send_msg = 0;
                                }
                            }

                            /* handle packet */
                            if (do_send_msg == 1)
                            {
                                result ++;

                                parse_msg_callback(rxbuf + msg_begin);

                                /*printf ("%ld accepted %d\n", ms, msg_id);*/
                            }
                            /*else printf ("%ld dropped %d\n", ms, msg_id);  */
                        }
                        else
                        {
                            msg_crc_err++;
                            if (debug && msg_crc_err != 1) /* dischard first CRC error which usually occurs at startup */
                            {
                                printf ("CRC error (len %d): ", (msg_next - msg_begin));
                                for (i=msg_begin; i<msg_next; i++)
                                {
                                    printf ("%03d ", rxbuf[i]);
                                }
                                printf ("\n");

                            }
                        }

                        /* remove packet from rxbuf */
                        for (i=msg_next, j=0; i<rxbuf_cnt; i++, j++)
                        {
                            rxbuf[j] = rxbuf[i];
                        }
                        rxbuf_cnt -= msg_next;
                        /* printf ("rxbuf_cnt_after %d\n", rxbuf_cnt); */

                        msg_begin = -1;
                        seek_from = 0;
                        maybe_more = 1;
                        /* printf ("repeat\n");  */
                    }
                }
            }
        }
        return result;
    }
    unsigned char* Mavlora::_getTXBUF()
    {
        return txbuf;
    }
}
