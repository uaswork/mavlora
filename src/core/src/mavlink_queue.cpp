
/*
 * Copyright (c) 2019, Frederik Mazur Andersen <fm@mmmi.sdu.dk>/<fm@fmazur.dk>
 *
 * SDU UAS Center, http://sdu.dk/uas, University of Southern Denmark 
 * All rights reserved.
 * 
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE.txt', which is part of this source code package.
 */

#include <mavlora.hpp>
#include "checksum.h"

namespace mavlora {

    void Mavlora::ml_queue_msg_mission_request(unsigned short seq, unsigned short system_id)
    {
        /* id=51 */
        /* reference: mavlink/common/mavlink_msg_mission_request_int.h */
        unsigned char i, len;
        unsigned char *buf = (txbuf + txbuf_cnt);

        /* encode part of the header */
        buf[ML_POS_PAYLOAD_LEN] = MAVLINK_MSG_ID_MISSION_REQUEST_INT_LEN;
        buf[ML_POS_COMP_ID] = 0;
        buf[ML_POS_MSG_ID] = MAVLINK_MSG_ID_MISSION_REQUEST_INT;

        /* seq */
        buf[ML_POS_PAYLOAD + 0] = seq & 0xff;
        buf[ML_POS_PAYLOAD + 1] = (seq>>8) & 0xff;

        /* system_id (target) */
        buf[ML_POS_PAYLOAD + 2] = system_id; /* UA is the target system */

        /* component_id (target) */
        buf[ML_POS_PAYLOAD + 3] = 0; /* target component */

        /* queue message */
        ml_queue_msg(buf, my_sysid);
    }
/***************************************************************************/
    void Mavlora::ml_queue_msg_mission_request_list()
    {
        /* id=43 */
        /* reference: mavlink/common/mavlink_msg_mission_request_list.h */
        unsigned char i, len;
        unsigned char *buf = (txbuf + txbuf_cnt);

        /* encode part of the header */
        buf[ML_POS_PAYLOAD_LEN] = MAVLINK_MSG_ID_MISSION_REQUEST_LIST_LEN;
        buf[ML_POS_COMP_ID] = 0;
        buf[ML_POS_MSG_ID] = MAVLINK_MSG_ID_MISSION_REQUEST_LIST;

        /* system_id (target) */
        buf[ML_POS_PAYLOAD + 0] = MAV_SYS_ID_UA; /* UA is the target system */

        /* component_id (target) */
        buf[ML_POS_PAYLOAD + 1] = 0; /* target component */

        /* queue message */
        ml_queue_msg(buf, my_sysid);
    }
/***************************************************************************/
    void Mavlora::ml_queue_msg_mission_item_int (float param1, float param2, float param3, float param4, int32_t x, int32_t y, float z, unsigned short seq, unsigned short command, unsigned char frame, unsigned char current, unsigned char autocontinue)
    {
        /* id=73 */
        /* reference: mavlink/common/mavlink_msg_mission_item_int.h */
        unsigned char i, len;
        unsigned char *buf = (txbuf + txbuf_cnt);
        unsigned char *pv;

        /* encode part of the header */
        buf[ML_POS_PAYLOAD_LEN] = MAVLINK_MSG_ID_MISSION_ITEM_INT_LEN;
        buf[ML_POS_COMP_ID] = 0;
        buf[ML_POS_MSG_ID] = MAVLINK_MSG_ID_MISSION_ITEM_INT;

        /* parameters */
        pv = (unsigned char *) &param1;
        buf[ML_POS_PAYLOAD + 0] = pv[0];
        buf[ML_POS_PAYLOAD + 1] = pv[1];
        buf[ML_POS_PAYLOAD + 2] = pv[2];
        buf[ML_POS_PAYLOAD + 3] = pv[3];

        pv = (unsigned char *) &param2;
        buf[ML_POS_PAYLOAD + 4] = pv[0];
        buf[ML_POS_PAYLOAD + 5] = pv[1];
        buf[ML_POS_PAYLOAD + 6] = pv[2];
        buf[ML_POS_PAYLOAD + 7] = pv[3];

        pv = (unsigned char *) &param3;
        buf[ML_POS_PAYLOAD + 8] = pv[0];
        buf[ML_POS_PAYLOAD + 9] = pv[1];
        buf[ML_POS_PAYLOAD + 10] = pv[2];
        buf[ML_POS_PAYLOAD + 11] = pv[3];

        pv = (unsigned char *) &param4;
        buf[ML_POS_PAYLOAD + 12] = pv[0];
        buf[ML_POS_PAYLOAD + 13] = pv[1];
        buf[ML_POS_PAYLOAD + 14] = pv[2];
        buf[ML_POS_PAYLOAD + 15] = pv[3];

        /* coordinates */
        pv = (unsigned char *) &x;
        buf[ML_POS_PAYLOAD + 16] = pv[0];
        buf[ML_POS_PAYLOAD + 17] = pv[1];
        buf[ML_POS_PAYLOAD + 18] = pv[2];
        buf[ML_POS_PAYLOAD + 19] = pv[3];

        pv = (unsigned char *) &y;
        buf[ML_POS_PAYLOAD + 20] = pv[0];
        buf[ML_POS_PAYLOAD + 21] = pv[1];
        buf[ML_POS_PAYLOAD + 22] = pv[2];
        buf[ML_POS_PAYLOAD + 23] = pv[3];

        pv = (unsigned char *) &z;
        buf[ML_POS_PAYLOAD + 24] = pv[0];
        buf[ML_POS_PAYLOAD + 25] = pv[1];
        buf[ML_POS_PAYLOAD + 26] = pv[2];
        buf[ML_POS_PAYLOAD + 27] = pv[3];

        /* sequence */
        buf[ML_POS_PAYLOAD + 28] = seq & 0xff;
        buf[ML_POS_PAYLOAD + 29] = (seq>>8) & 0xff;

        /* command */
        buf[ML_POS_PAYLOAD + 30] = command & 0xff;
        buf[ML_POS_PAYLOAD + 31] = (command>>8) & 0xff;

        /* system_id (target) */
        buf[ML_POS_PAYLOAD + 32] = MAV_SYS_ID_UA; /* UA is the target system */

        /* component_id (target) */
        buf[ML_POS_PAYLOAD + 33] = 0; /* target component, 0 equals ALL */

        /* Frame */
        buf[ML_POS_PAYLOAD + 34] = frame;

        /* Current */
        buf[ML_POS_PAYLOAD + 35] = current;

        /* autocontinue */
        buf[ML_POS_PAYLOAD + 36] = autocontinue;

        /* queue message */
        ml_queue_msg(buf, my_sysid);
    }
/***************************************************************************/
    void Mavlora::ml_queue_msg_mission_count (unsigned short count)
    {
        /* id=44 */
        /* reference: mavlink/common/mavlink_msg_mission_count.h */
        unsigned char i, len;
        unsigned char *buf = (txbuf + txbuf_cnt);

        /* encode part of the header */
        buf[ML_POS_PAYLOAD_LEN] = MAVLINK_MSG_ID_MISSION_COUNT_LEN;
        buf[ML_POS_COMP_ID] = 0;
        buf[ML_POS_MSG_ID] = MAVLINK_MSG_ID_MISSION_COUNT;

        /* count (number of mission items to upload */
        buf[ML_POS_PAYLOAD + 0] = count & 0xff;
        buf[ML_POS_PAYLOAD + 1] = (count>>8) & 0xff;

        /* system_id (target) */
        buf[ML_POS_PAYLOAD + 2] = MAV_SYS_ID_UA; /* UA is the target system */

        /* component_id (target) */
        buf[ML_POS_PAYLOAD + 3] = 0; /* target component */

        /* queue message */
        ml_queue_msg(buf, my_sysid);
    }
/***************************************************************************/
    void Mavlora::ml_queue_msg_mission_clear_all()
    {
        /* id=45 */
        /* reference: mavlink/common/mavlink_msg_mission_clear_all.h */
        unsigned char i, len;
        unsigned char *buf = (txbuf + txbuf_cnt);

        /* encode part of the header */
        buf[ML_POS_PAYLOAD_LEN] = MAVLINK_MSG_ID_MISSION_CLEAR_ALL_LEN;
        buf[ML_POS_COMP_ID] = 0;
        buf[ML_POS_MSG_ID] = MAVLINK_MSG_ID_MISSION_CLEAR_ALL;

        /* system_id (target) */
        buf[ML_POS_PAYLOAD + 0] = MAV_SYS_ID_UA; /* UA is the target system */

        /* component_id (target) */
        buf[ML_POS_PAYLOAD + 1] = 0; /* target component */

        /* queue message */
        ml_queue_msg(buf, my_sysid);
    }
/***************************************************************************/
    void Mavlora::ml_queue_msg_mission_ack(uint8_t result)
    {
        /* id=47 */
        /* reference: mavlink/common/mavlink_msg_mission_ack.h */
        unsigned char i, len;
        unsigned char *buf = (txbuf + txbuf_cnt);

        /* encode part of the header */
        buf[ML_POS_PAYLOAD_LEN] = MAVLINK_MSG_ID_MISSION_ACK_LEN;
        buf[ML_POS_COMP_ID] = 0;
        buf[ML_POS_MSG_ID] = MAVLINK_MSG_ID_MISSION_ACK;

        /* system_id (target) */
        buf[ML_POS_PAYLOAD + 0] = MAV_SYS_ID_UA; /* UA is the target system */

        /* component_id (target) */
        buf[ML_POS_PAYLOAD + 1] = 0; /* target component */

        /* type */
        buf[ML_POS_PAYLOAD + 2] = result; /* type (MAVLINK_MISSION_RESULT enum) */

        /* queue message */
        ml_queue_msg(buf, my_sysid);
    }
/***************************************************************************/
    void Mavlora::ml_queue_msg_command_long(unsigned short cmd_id, float param1, float param2, float param3, float param4, float param5, float param6, float param7, unsigned int confirmation )
    {
        /* id=76 */
        /* reference: mavlink/common/mavlink_msg_command_long */
        unsigned char *buf = (txbuf + txbuf_cnt);
        unsigned char *pv;

        /* encode part of the header */
        buf[ML_POS_PAYLOAD_LEN] = MAVLINK_MSG_ID_COMMAND_LONG_LEN;
        buf[ML_POS_COMP_ID] = 0;
        buf[ML_POS_MSG_ID] = MAVLINK_MSG_ID_COMMAND_LONG;

        /* param_value */
        pv = (unsigned char *) &param1;
        buf[ML_POS_PAYLOAD + 0]  = pv[0];
        buf[ML_POS_PAYLOAD + 1]  = pv[1];
        buf[ML_POS_PAYLOAD + 2]  = pv[2];
        buf[ML_POS_PAYLOAD + 3]  = pv[3];

        pv = (unsigned char *) &param2;
        buf[ML_POS_PAYLOAD + 4]  = pv[0];
        buf[ML_POS_PAYLOAD + 5]  = pv[1];
        buf[ML_POS_PAYLOAD + 6]  = pv[2];
        buf[ML_POS_PAYLOAD + 7]  = pv[3];

        pv = (unsigned char *) &param3;
        buf[ML_POS_PAYLOAD + 8]  = pv[0];
        buf[ML_POS_PAYLOAD + 9]  = pv[1];
        buf[ML_POS_PAYLOAD + 10]  = pv[2];
        buf[ML_POS_PAYLOAD + 11]  = pv[3];

        pv = (unsigned char *) &param4;
        buf[ML_POS_PAYLOAD + 12] = pv[0];
        buf[ML_POS_PAYLOAD + 13] = pv[1];
        buf[ML_POS_PAYLOAD + 14] = pv[2];
        buf[ML_POS_PAYLOAD + 15] = pv[3];

        pv = (unsigned char *) &param5;
        buf[ML_POS_PAYLOAD + 16] = pv[0];
        buf[ML_POS_PAYLOAD + 17] = pv[1];
        buf[ML_POS_PAYLOAD + 18] = pv[2];
        buf[ML_POS_PAYLOAD + 19] = pv[3];

        pv = (unsigned char *) &param6;
        buf[ML_POS_PAYLOAD + 20] = pv[0];
        buf[ML_POS_PAYLOAD + 21] = pv[1];
        buf[ML_POS_PAYLOAD + 22] = pv[2];
        buf[ML_POS_PAYLOAD + 23] = pv[3];

        pv = (unsigned char *) &param7;
        buf[ML_POS_PAYLOAD + 24] = pv[0];
        buf[ML_POS_PAYLOAD + 25] = pv[1];
        buf[ML_POS_PAYLOAD + 26] = pv[2];
        buf[ML_POS_PAYLOAD + 27] = pv[3];

        /* command_id */
        buf[ML_POS_PAYLOAD + 28] = cmd_id & 0xff;
        buf[ML_POS_PAYLOAD + 29] = (cmd_id>>8) & 0xff;

        /* system_id (target) */
        buf[ML_POS_PAYLOAD + 30] = MAV_SYS_ID_UA; /* UA is the target system */

        /* component_id (target) */
        buf[ML_POS_PAYLOAD + 31] = 0; /* target component */

        /* confirmation */
        buf[ML_POS_PAYLOAD + 32] = confirmation;

        /* queue message */
        ml_queue_msg(buf, my_sysid);
    }
    void Mavlora::ml_queue_msg_command_int(unsigned short cmd_id, unsigned char frame, float param1, float param2, float param3, float param4, int32_t x, int32_t y, float z) {
        /* id=76 */
        /* reference: mavlink/common/mavlink_msg_command_long */
        unsigned char *buf = (txbuf + txbuf_cnt);
        unsigned char *pv;

        /* encode part of the header */
        buf[ML_POS_PAYLOAD_LEN] = MAVLINK_MSG_ID_COMMAND_INT_LEN;
        buf[ML_POS_COMP_ID] = 0;
        buf[ML_POS_MSG_ID] = MAVLINK_MSG_ID_COMMAND_INT;

        /* param_value and position */
        pv = (unsigned char *) &x;
        buf[ML_POS_PAYLOAD + 0]  = pv[0];
        buf[ML_POS_PAYLOAD + 1]  = pv[1];
        buf[ML_POS_PAYLOAD + 2]  = pv[2];
        buf[ML_POS_PAYLOAD + 3]  = pv[3];

        pv = (unsigned char *) &y;
        buf[ML_POS_PAYLOAD + 4]  = pv[0];
        buf[ML_POS_PAYLOAD + 5]  = pv[1];
        buf[ML_POS_PAYLOAD + 6]  = pv[2];
        buf[ML_POS_PAYLOAD + 7]  = pv[3];

        pv = (unsigned char *) &param1;
        buf[ML_POS_PAYLOAD + 8]  = pv[0];
        buf[ML_POS_PAYLOAD + 9]  = pv[1];
        buf[ML_POS_PAYLOAD + 10]  = pv[2];
        buf[ML_POS_PAYLOAD + 11]  = pv[3];

        pv = (unsigned char *) &param2;
        buf[ML_POS_PAYLOAD + 12] = pv[0];
        buf[ML_POS_PAYLOAD + 13] = pv[1];
        buf[ML_POS_PAYLOAD + 14] = pv[2];
        buf[ML_POS_PAYLOAD + 15] = pv[3];

        pv = (unsigned char *) &param3;
        buf[ML_POS_PAYLOAD + 16] = pv[0];
        buf[ML_POS_PAYLOAD + 17] = pv[1];
        buf[ML_POS_PAYLOAD + 18] = pv[2];
        buf[ML_POS_PAYLOAD + 19] = pv[3];

        pv = (unsigned char *) &param4;
        buf[ML_POS_PAYLOAD + 20] = pv[0];
        buf[ML_POS_PAYLOAD + 21] = pv[1];
        buf[ML_POS_PAYLOAD + 22] = pv[2];
        buf[ML_POS_PAYLOAD + 23] = pv[3];

        pv = (unsigned char *) &z;
        buf[ML_POS_PAYLOAD + 24] = pv[0];
        buf[ML_POS_PAYLOAD + 25] = pv[1];
        buf[ML_POS_PAYLOAD + 26] = pv[2];
        buf[ML_POS_PAYLOAD + 27] = pv[3];

        /* command_id */
        buf[ML_POS_PAYLOAD + 28] = cmd_id & 0xff;
        buf[ML_POS_PAYLOAD + 29] = (cmd_id>>8) & 0xff;

        /* system_id (target) */
        buf[ML_POS_PAYLOAD + 30] = MAV_SYS_ID_UA; /* UA is the target system */

        /* component_id (target) */
        buf[ML_POS_PAYLOAD + 31] = 0; /* target component */

        /* frame */
        buf[ML_POS_PAYLOAD + 32] = frame; /* Frame: Global POS INT + Relative Alt  == 6 */

        /* Current */
        buf[ML_POS_PAYLOAD + 33] = 0; /* Not used */

        /* autocontinue */
        buf[ML_POS_PAYLOAD + 34] = 0; /* Not used */

        /* queue message */
        ml_queue_msg(buf, my_sysid);
    }
    void Mavlora::ml_queue_msg_command_ack(unsigned short cmd_id, unsigned char result) {
        /* id=77 */
        /* reference: mavlink/common/command_ack */
        unsigned char *buf = (txbuf + txbuf_cnt);
        unsigned char *pv;

        /* encode part of the header */
        buf[ML_POS_PAYLOAD_LEN] = MAVLINK_MSG_ID_COMMAND_ACK_LEN;
        buf[ML_POS_COMP_ID] = 0;
        buf[ML_POS_MSG_ID] = MAVLINK_MSG_ID_COMMAND_ACK;

        /* command_id */
        buf[ML_POS_PAYLOAD + 0] = cmd_id & 0xff;
        buf[ML_POS_PAYLOAD + 1] = (cmd_id>>8) & 0xff;

        /* result */
        buf[ML_POS_PAYLOAD + 2] = result;

        /* queue message */
        ml_queue_msg(buf, my_sysid);
    }
/***************************************************************************/
    void Mavlora::ml_queue_msg_heartbeat(unsigned char type, unsigned char autopilot, unsigned char base_mode, unsigned long custom_mode, unsigned char system_status, unsigned char system_id)
    {
        /* id=0 */
        /* reference: mavlink/common/mavlink_msg_heartbeat */
        unsigned char *buf = (txbuf + txbuf_cnt);
        unsigned char *cm;

        /* encode part of the header */
        buf[ML_POS_PAYLOAD_LEN] = MAVLINK_MSG_ID_HEARTBEAT_LEN;
        buf[ML_POS_SYS_ID] = system_id; //send GCS system_id => 255
        buf[ML_POS_COMP_ID] = 0;
        buf[ML_POS_MSG_ID] = MAVLINK_MSG_ID_HEARTBEAT;

        /* custom mode */
        cm = (unsigned char *) &custom_mode;
        buf[ML_POS_PAYLOAD + 0] = cm[0];
        buf[ML_POS_PAYLOAD + 1] = cm[1];
        buf[ML_POS_PAYLOAD + 2] = cm[2];
        buf[ML_POS_PAYLOAD + 3] = cm[3];

        /* type. 6 = MAV_TYPE_GCS */
        buf[ML_POS_PAYLOAD + 4] = type; //6 == ground control station

        /* autopilot */
        buf[ML_POS_PAYLOAD + 5] = autopilot; //8 = INVALID - Not a drone, but gcs software

        /* base_mode */
        buf[ML_POS_PAYLOAD + 6] = base_mode; //1 = custom mode

        /* system_status */
        buf[ML_POS_PAYLOAD + 7] = system_status; //4 = ACTIVE_STATE, GCS is active. Required to get status messages

        /* Mavlink Version */
        buf[ML_POS_PAYLOAD + 8] = 3; // currently 3, but magic number of mavlink_version...

        /* queue message */
        ml_queue_msg(buf, system_id);
    }
    void Mavlora::ml_queue_msg_radio_comm_status(double lastheard_fc, double lastheard_gcs, uint32_t lost_msgs_gcs, uint32_t lost_msgs_fc, uint32_t received_msgs_gcs, uint32_t received_msgs_fc, uint32_t transmit_msgs_gcs, uint32_t transmit_msgs_fc, uint16_t dualrx_status)
    {
        /* id=15 */
        /* reference: own radio_comm_status message */
        unsigned char *buf = (txbuf + txbuf_cnt);
        unsigned char *var;

        /* encode part of the header */
        buf[ML_POS_PAYLOAD_LEN] = MAVLINK_MSG_ID_RADIO_COMM_STATUS_LEN;
        buf[ML_POS_COMP_ID] = 0;
        buf[ML_POS_MSG_ID] = MAVLINK_MSG_ID_RADIO_COMM_STATUS;

        /* last heard fc */
        var = (unsigned char *) &lastheard_fc;
        buf[ML_POS_PAYLOAD + 0] = var[0];
        buf[ML_POS_PAYLOAD + 1] = var[1];
        buf[ML_POS_PAYLOAD + 2] = var[2];
        buf[ML_POS_PAYLOAD + 3] = var[3];
        buf[ML_POS_PAYLOAD + 4] = var[4];
        buf[ML_POS_PAYLOAD + 5] = var[5];
        buf[ML_POS_PAYLOAD + 6] = var[6];
        buf[ML_POS_PAYLOAD + 7] = var[7];

        /* last heard gcs */
        var = (unsigned char *) &lastheard_gcs;
        buf[ML_POS_PAYLOAD + 8] = var[0];
        buf[ML_POS_PAYLOAD + 9] = var[1];
        buf[ML_POS_PAYLOAD + 10] = var[2];
        buf[ML_POS_PAYLOAD + 11] = var[3];
        buf[ML_POS_PAYLOAD + 12] = var[4];
        buf[ML_POS_PAYLOAD + 13] = var[5];
        buf[ML_POS_PAYLOAD + 14] = var[6];
        buf[ML_POS_PAYLOAD + 15] = var[7];

        /* lost msgs gcs */
        var = (unsigned char *) &lost_msgs_gcs;
        buf[ML_POS_PAYLOAD + 16] = var[0];
        buf[ML_POS_PAYLOAD + 17] = var[1];
        buf[ML_POS_PAYLOAD + 18] = var[2];
        buf[ML_POS_PAYLOAD + 19] = var[3];

        /* lost msgs fc */
        var = (unsigned char *) &lost_msgs_fc;
        buf[ML_POS_PAYLOAD + 20] = var[0];
        buf[ML_POS_PAYLOAD + 21] = var[1];
        buf[ML_POS_PAYLOAD + 22] = var[2];
        buf[ML_POS_PAYLOAD + 23] = var[3];

        /* received msgs gcs */
        var = (unsigned char *) &received_msgs_gcs;
        buf[ML_POS_PAYLOAD + 24] = var[0];
        buf[ML_POS_PAYLOAD + 25] = var[1];
        buf[ML_POS_PAYLOAD + 26] = var[2];
        buf[ML_POS_PAYLOAD + 27] = var[3];

        /* received msgs fc */
        var = (unsigned char *) &received_msgs_fc;
        buf[ML_POS_PAYLOAD + 28] = var[0];
        buf[ML_POS_PAYLOAD + 29] = var[1];
        buf[ML_POS_PAYLOAD + 30] = var[2];
        buf[ML_POS_PAYLOAD + 31] = var[3];

        /* transmit msgs gcs */
        var = (unsigned char *) &transmit_msgs_gcs;
        buf[ML_POS_PAYLOAD + 32] = var[0];
        buf[ML_POS_PAYLOAD + 33] = var[1];
        buf[ML_POS_PAYLOAD + 34] = var[2];
        buf[ML_POS_PAYLOAD + 35] = var[3];

        /* transmit msgs fc */
        var = (unsigned char *) &transmit_msgs_fc;
        buf[ML_POS_PAYLOAD + 36] = var[0];
        buf[ML_POS_PAYLOAD + 37] = var[1];
        buf[ML_POS_PAYLOAD + 38] = var[2];
        buf[ML_POS_PAYLOAD + 39] = var[3];

        /* dualrx value */
        var = (unsigned char *) &dualrx_status;
        buf[ML_POS_PAYLOAD + 40] = var[0];
        buf[ML_POS_PAYLOAD + 41] = var[1];

        /* queue message */
        ml_queue_msg(buf, my_sysid);
    }
    void Mavlora::ml_queue_msg_lte_metrics(uint64_t request_time, uint64_t response_time, int32_t snr, int32_t rsrq, int32_t rssi, int32_t rsrp, int32_t pci, int32_t lat, int32_t lon, int32_t alt_msl, uint16_t heading)
    {
        /* id=13 */
        /* reference: own radio_comm_status message */
        unsigned char *buf = (txbuf + txbuf_cnt);
        unsigned char *var;

        /* encode part of the header */
        buf[ML_POS_PAYLOAD_LEN] = MAVLINK_MSG_ID_LTE_METRICS_LEN;
        buf[ML_POS_COMP_ID] = 0;
        buf[ML_POS_MSG_ID] = MAVLINK_MSG_ID_LTE_METRICS;

        /* request time */
        var = (unsigned char *) &request_time;
        buf[ML_POS_PAYLOAD + 0] = var[0];
        buf[ML_POS_PAYLOAD + 1] = var[1];
        buf[ML_POS_PAYLOAD + 2] = var[2];
        buf[ML_POS_PAYLOAD + 3] = var[3];
        buf[ML_POS_PAYLOAD + 4] = var[4];
        buf[ML_POS_PAYLOAD + 5] = var[5];
        buf[ML_POS_PAYLOAD + 6] = var[6];
        buf[ML_POS_PAYLOAD + 7] = var[7];

        /* response time */
        var = (unsigned char *) &response_time;
        buf[ML_POS_PAYLOAD + 8] = var[0];
        buf[ML_POS_PAYLOAD + 9] = var[1];
        buf[ML_POS_PAYLOAD + 10] = var[2];
        buf[ML_POS_PAYLOAD + 11] = var[3];
        buf[ML_POS_PAYLOAD + 12] = var[4];
        buf[ML_POS_PAYLOAD + 13] = var[5];
        buf[ML_POS_PAYLOAD + 14] = var[6];
        buf[ML_POS_PAYLOAD + 15] = var[7];

        /* snr */
        var = (unsigned char *) &snr;
        buf[ML_POS_PAYLOAD + 16] = var[0];
        buf[ML_POS_PAYLOAD + 17] = var[1];
        buf[ML_POS_PAYLOAD + 18] = var[2];
        buf[ML_POS_PAYLOAD + 19] = var[3];

        /* rsrq */
        var = (unsigned char *) &rsrq;
        buf[ML_POS_PAYLOAD + 20] = var[0];
        buf[ML_POS_PAYLOAD + 21] = var[1];
        buf[ML_POS_PAYLOAD + 22] = var[2];
        buf[ML_POS_PAYLOAD + 23] = var[3];

        /* rssi */
        var = (unsigned char *) &rssi;
        buf[ML_POS_PAYLOAD + 24] = var[0];
        buf[ML_POS_PAYLOAD + 25] = var[1];
        buf[ML_POS_PAYLOAD + 26] = var[2];
        buf[ML_POS_PAYLOAD + 27] = var[3];

        /* rsrp */
        var = (unsigned char *) &rsrp;
        buf[ML_POS_PAYLOAD + 28] = var[0];
        buf[ML_POS_PAYLOAD + 29] = var[1];
        buf[ML_POS_PAYLOAD + 30] = var[2];
        buf[ML_POS_PAYLOAD + 31] = var[3];

        /* pci */
        var = (unsigned char *) &pci;
        buf[ML_POS_PAYLOAD + 32] = var[0];
        buf[ML_POS_PAYLOAD + 33] = var[1];
        buf[ML_POS_PAYLOAD + 34] = var[2];
        buf[ML_POS_PAYLOAD + 35] = var[3];

        /* lat */
        var = (unsigned char *) &lat;
        buf[ML_POS_PAYLOAD + 36] = var[0];
        buf[ML_POS_PAYLOAD + 37] = var[1];
        buf[ML_POS_PAYLOAD + 38] = var[2];
        buf[ML_POS_PAYLOAD + 39] = var[3];

        /* lon */
        var = (unsigned char *) &lon;
        buf[ML_POS_PAYLOAD + 40] = var[0];
        buf[ML_POS_PAYLOAD + 41] = var[1];
        buf[ML_POS_PAYLOAD + 42] = var[2];
        buf[ML_POS_PAYLOAD + 43] = var[3];

        /* alt_msl */
        var = (unsigned char *) &alt_msl;
        buf[ML_POS_PAYLOAD + 44] = var[0];
        buf[ML_POS_PAYLOAD + 45] = var[1];
        buf[ML_POS_PAYLOAD + 46] = var[2];
        buf[ML_POS_PAYLOAD + 47] = var[3];

        /* heading */
        var = (unsigned char *) &heading;
        buf[ML_POS_PAYLOAD + 48] = var[0];
        buf[ML_POS_PAYLOAD + 49] = var[1];

        /* queue message */
        ml_queue_msg(buf, my_sysid);
    }
    void Mavlora::ml_queue_msg_nr5g_metrics(uint64_t request_time, uint64_t response_time, int32_t snr, int32_t rsrp, int32_t pci, int32_t lat, int32_t lon, int32_t alt_msl, uint16_t heading)
    {
        /* id=14 */
        /* reference: own radio_comm_status message */
        unsigned char *buf = (txbuf + txbuf_cnt);
        unsigned char *var;

        /* encode part of the header */
        buf[ML_POS_PAYLOAD_LEN] = MAVLINK_MSG_ID_NR5G_METRICS_LEN;
        buf[ML_POS_COMP_ID] = 0;
        buf[ML_POS_MSG_ID] = MAVLINK_MSG_ID_NR5G_METRICS;

        /* request time */
        var = (unsigned char *) &request_time;
        buf[ML_POS_PAYLOAD + 0] = var[0];
        buf[ML_POS_PAYLOAD + 1] = var[1];
        buf[ML_POS_PAYLOAD + 2] = var[2];
        buf[ML_POS_PAYLOAD + 3] = var[3];
        buf[ML_POS_PAYLOAD + 4] = var[4];
        buf[ML_POS_PAYLOAD + 5] = var[5];
        buf[ML_POS_PAYLOAD + 6] = var[6];
        buf[ML_POS_PAYLOAD + 7] = var[7];

        /* response time */
        var = (unsigned char *) &response_time;
        buf[ML_POS_PAYLOAD + 8] = var[0];
        buf[ML_POS_PAYLOAD + 9] = var[1];
        buf[ML_POS_PAYLOAD + 10] = var[2];
        buf[ML_POS_PAYLOAD + 11] = var[3];
        buf[ML_POS_PAYLOAD + 12] = var[4];
        buf[ML_POS_PAYLOAD + 13] = var[5];
        buf[ML_POS_PAYLOAD + 14] = var[6];
        buf[ML_POS_PAYLOAD + 15] = var[7];

        /* snr */
        var = (unsigned char *) &snr;
        buf[ML_POS_PAYLOAD + 16] = var[0];
        buf[ML_POS_PAYLOAD + 17] = var[1];
        buf[ML_POS_PAYLOAD + 18] = var[2];
        buf[ML_POS_PAYLOAD + 19] = var[3];

        /* rsrp */
        var = (unsigned char *) &rsrp;
        buf[ML_POS_PAYLOAD + 20] = var[0];
        buf[ML_POS_PAYLOAD + 21] = var[1];
        buf[ML_POS_PAYLOAD + 22] = var[2];
        buf[ML_POS_PAYLOAD + 23] = var[3];

        /* pci */
        var = (unsigned char *) &pci;
        buf[ML_POS_PAYLOAD + 24] = var[0];
        buf[ML_POS_PAYLOAD + 25] = var[1];
        buf[ML_POS_PAYLOAD + 26] = var[2];
        buf[ML_POS_PAYLOAD + 27] = var[3];

        /* lat */
        var = (unsigned char *) &lat;
        buf[ML_POS_PAYLOAD + 28] = var[0];
        buf[ML_POS_PAYLOAD + 29] = var[1];
        buf[ML_POS_PAYLOAD + 30] = var[2];
        buf[ML_POS_PAYLOAD + 31] = var[3];

        /* lon */
        var = (unsigned char *) &lon;
        buf[ML_POS_PAYLOAD + 32] = var[0];
        buf[ML_POS_PAYLOAD + 33] = var[1];
        buf[ML_POS_PAYLOAD + 34] = var[2];
        buf[ML_POS_PAYLOAD + 35] = var[3];

        /* alt_msl */
        var = (unsigned char *) &alt_msl;
        buf[ML_POS_PAYLOAD + 36] = var[0];
        buf[ML_POS_PAYLOAD + 37] = var[1];
        buf[ML_POS_PAYLOAD + 38] = var[2];
        buf[ML_POS_PAYLOAD + 39] = var[3];

        /* heading */
        var = (unsigned char *) &heading;
        buf[ML_POS_PAYLOAD + 40] = var[0];
        buf[ML_POS_PAYLOAD + 41] = var[1];

        /* queue message */
        ml_queue_msg(buf, my_sysid);
    }
    /***************************************************************************/
    void Mavlora::ml_queue_msg_gps_rtcm_data(uint8_t flags, uint8_t length, const uint8_t* data)
    {
        /* id=47 */
        /* reference: mavlink/common/mavlink_msg_mission_ack.h */
        unsigned char *buf = (txbuf + txbuf_cnt);

        /* encode part of the header */
        buf[ML_POS_PAYLOAD_LEN] = MAVLINK_MSG_ID_GPS_RTCM_DATA_LEN;
        buf[ML_POS_COMP_ID] = 0;
        buf[ML_POS_MSG_ID] = MAVLINK_MSG_ID_GPS_RTCM_DATA;

        /* flags */
        buf[ML_POS_PAYLOAD + 0] = flags;

        /* data length */
        buf[ML_POS_PAYLOAD + 1] = length;

        /* type */
        for (int i = 0; i < 180; i++)
        {
            buf[ML_POS_PAYLOAD + 2 + i] = data[i];
        }

        /* queue message */
        ml_queue_msg(buf, my_sysid);
    }
/***************************************************************************/
}