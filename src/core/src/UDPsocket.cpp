/*
 * Copyright (c) 2021, Frederik Mazur Andersen <fm@mmmi.sdu.dk>/<fm@fmazur.dk>
 *
 * SDU UAS Center, http://sdu.dk/uas, University of Southern Denmark
 * All rights reserved.
 *
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE.txt', which is part of this source code package.
 */

#include "UDPsocket.hpp"

UDPsocket::UDPsocket() = default;

UDPsocket::UDPsocket(std::string target_address, int target_port)
{
    // save target info
    out_addr = std::move(target_address);
    out_port = target_port;

    // Creating UDPsocket file descriptor
    if ((sockfd = socket(AF_INET, SOCK_DGRAM, 0)) < 0 ) {
        perror("UDPsocket creation failed");
        exit(EXIT_FAILURE);
    }

    // zero out structures
    memset(&serv_addr, 0, sizeof(serv_addr));
    memset(&target_addr, 0, sizeof(target_addr));

    // save size of struct element
    target_addr_len = sizeof(struct sockaddr_in);

    // convert ip string to binary form and save as out_client destination
    if (inet_pton(AF_INET,out_addr.c_str() , &target_addr.sin_addr) == 0)
    {
        std::cout << "inet_aton() failed to convert ip to valid representation" << std::endl;
        exit(1);
    }

    // set out_client details
    target_addr.sin_family = AF_INET; //IPv4
    target_addr.sin_port = htons(out_port);
}

bool UDPsocket::enablelisten(int recv_p, unsigned char *buffer, unsigned int buffer_size) {
    // if already going, return
    if (listen_enabled)
    {
        std::cout << "Listen is already enabled..." << std::endl;
        return false;
    }

    // save recv info
    recv_port = recv_p;
    recv_buffer = buffer;
    recv_buffer_size = buffer_size;

    // Filling server information
    serv_addr.sin_family = AF_INET; // IPv4
    serv_addr.sin_addr.s_addr = INADDR_ANY; //Any IP, should == 0.0.0.0
    serv_addr.sin_port = htons(recv_port);

    // Bind the UDPsocket with the server address
    if ( bind(sockfd, (const struct sockaddr *)&serv_addr, sizeof(serv_addr)) < 0 )
    {
        std::cout << "Failed to bind udp socket for listen: " << strerror(errno) << std::endl;
        return false;
    }
    std::cout << "Listening on: port: " << ntohs(serv_addr.sin_port) << ", IP: " << inet_ntoa(serv_addr.sin_addr) << std::endl;

    // set active
    listen_enabled = true;
    return true;
}

void UDPsocket::disableListen() {
    close(sockfd);
    listen_enabled = false;
    printf("Closed listen on UDP socket, ip: %s, port %d \n", inet_ntoa(serv_addr.sin_addr), ntohs(serv_addr.sin_port));
}

// checks for data received and put into buffer, returns -1 on error, otherwise the amount of data received
int UDPsocket::receiveData() {
    // check for data on serial and put it into buffer - non-blocking
    return recvfrom(sockfd, recv_buffer, recv_buffer_size, MSG_DONTWAIT, (struct sockaddr *)&received_addr, &received_addr_len);
}

int UDPsocket::sendData(unsigned char *tx_buf, short &tx_buf_cnt ) {
    return sendto(sockfd, tx_buf, tx_buf_cnt, 0, (struct sockaddr*)&target_addr, target_addr_len);
}
