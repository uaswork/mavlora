
/*
 * Copyright (c) 2019, Frederik Mazur Andersen <fm@mmmi.sdu.dk>/<fm@fmazur.dk>
 *
 * SDU UAS Center, http://sdu.dk/uas, University of Southern Denmark 
 * All rights reserved.
 * 
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE.txt', which is part of this source code package.
 */

#include <mavlora.hpp>

namespace mavlora {

    mavlink_sys_status_t Mavlora::ml_unpack_msg_sys_status (unsigned char *payload)
    {
        /* id=1 */
        /* mavlink/common/mavlink_msg_sys_status.h */
        mavlink_sys_status_t sys_status;

        sys_status.load = payload[12] | (payload[13] << 8);
        sys_status.voltage_battery = payload[14] | (payload[15] << 8);
        sys_status.current_battery = payload[16] | (payload[17] << 8);
        sys_status.battery_remaining = payload[30];
        sys_status.errors_comm = payload[20] | (payload[21] << 8);

        return sys_status;
    }
    mavlink_gps_raw_int_t Mavlora::ml_unpack_msg_gps_raw_int (unsigned char *payload)
    {
        /* id=24 */
        /* mavlink/common/mavlink_gps_raw_int.h */
        mavlink_gps_raw_int_t gri;
        uint64_t *ui64p;
        int32_t *i32p;
        uint8_t *ui8p;
        uint16_t *ui16p;

        ui64p = (uint64_t *) (payload + 0);
        gri.time_usec = *ui64p;
        i32p = (int32_t *) (payload + 8);
        gri.lat = *i32p;
        i32p = (int32_t *) (payload + 12);
        gri.lon = *i32p;
        i32p = (int32_t *) (payload + 16);
        gri.alt = *i32p;
        ui16p = (uint16_t *) (payload + 20);
        gri.eph = *ui16p;
        ui16p = (uint16_t *) (payload + 22);
        gri.epv = *ui16p;
        ui16p = (uint16_t *) (payload + 24);
        gri.vel = *ui16p;
        ui16p = (uint16_t *) (payload + 26);
        gri.cog = *ui16p;
        ui8p = (uint8_t *) (payload + 28);
        gri.fix_type = *ui8p;
        ui8p = (uint8_t *) (payload + 29);
        gri.satellites_visible = *ui8p;

        return gri;
    }
    mavlink_attitude_t Mavlora::ml_unpack_msg_attitude (unsigned char *payload)
    {
        /* id=30 */
        /* mavlink/common/mavlink_attitude.h */
        mavlink_attitude_t atti;
        uint32_t *ui32p;
        float *fp;

        ui32p = (uint32_t *) (payload + 0);
        atti.time_boot_ms = *ui32p;
        fp = (float *) (payload + 4);
        atti.roll = *fp;
        fp = (float *) (payload + 8);
        atti.pitch = *fp;
        fp = (float *) (payload + 12);
        atti.yaw = *fp;

        return atti;
    }
    mavlink_global_position_int_t Mavlora::ml_unpack_msg_global_position_int (unsigned char *payload)
    {
        /* id=33 */
        /* mavlink/common/mavlink_global_position_int.h */
        mavlink_global_position_int_t gpi;
        int32_t *i32p;
        int16_t *i16p;
        uint16_t *ui16p;

        i32p = (int32_t *) (payload + 0);
        gpi.time_boot_ms = *i32p;
        i32p = (int32_t *) (payload + 4);
        gpi.lat = *i32p;
        i32p = (int32_t *) (payload + 8);
        gpi.lon = *i32p;
        i32p = (int32_t *) (payload + 12);
        gpi.alt = *i32p;
        i32p = (int32_t *) (payload + 16);
        gpi.relative_alt = *i32p;
        i16p = (int16_t *) (payload + 20);
        gpi.vx = *i16p;
        i16p = (int16_t *) (payload + 22);
        gpi.vy = *i16p;
        i16p = (int16_t *) (payload + 24);
        gpi.vz = *i16p;
        ui16p = (uint16_t *) (payload + 26);
        gpi.hdg = *ui16p;

        return gpi;
    }
    mavlink_mission_item_t Mavlora::ml_unpack_msg_mission_item (unsigned char *payload)
    {
        /* id=39 */
        /* mavlink/common/mavlink_msg_mission_item.h */
        mavlink_mission_item_t item;
        float *fp;

        fp = (float *) (payload + 0);
        item.param1 = *fp;
        fp = (float *) (payload + 4);
        item.param2 = *fp;
        fp = (float *) (payload + 8);
        item.param3 = *fp;
        fp = (float *) (payload + 12);
        item.param4 = *fp;
        fp = (float *) (payload + 16);
        item.x = *fp;
        fp = (float *) (payload + 20);
        item.y = *fp;
        fp = (float *) (payload + 24);
        item.z = *fp;

        item.seq = payload[28] | (payload[29] << 8);
        item.command = payload[30] | (payload[31] << 8);
        item.target_system = payload[32];
        item.target_component = payload[33];
        item.frame = payload[34];
        item.current = payload[35];
        item.autocontinue = payload[36];

        return item;
    }
    mavlink_mission_item_int_t Mavlora::ml_unpack_msg_mission_item_int (unsigned char *payload)
    {
        /* id=73 */
        /* mavlink/common/mavlink_msg_mission_item_int.h */
        mavlink_mission_item_int_t item;
        float *fp;
        uint32_t *ui32p;

        fp = (float *) (payload + 0);
        item.param1 = *fp;
        fp = (float *) (payload + 4);
        item.param2 = *fp;
        fp = (float *) (payload + 8);
        item.param3 = *fp;
        fp = (float *) (payload + 12);
        item.param4 = *fp;
        ui32p = (uint32_t *) (payload + 16);
        item.x = *ui32p;
        ui32p = (uint32_t *) (payload + 20);
        item.y = *ui32p;
        fp = (float *) (payload + 24);
        item.z = *fp;

        item.seq = payload[28] | (payload[29] << 8);
        item.command = payload[30] | (payload[31] << 8);
        item.target_system = payload[32];
        item.target_component = payload[33];
        item.frame = payload[34];
        item.current = payload[35];
        item.autocontinue = payload[36];

        return item;
    }
    mavlink_mission_request_int_t Mavlora::ml_unpack_msg_mission_request_int (unsigned char *payload)
    {
        /* id=51 */
        /* mavlink/common/mavlink_msg_mission_request_int.h */
        mavlink_mission_request_int_t item;
        uint16_t *ui16p;

        ui16p = (uint16_t *) (payload + 0);
        item.seq = *ui16p;
        item.target_system = payload[2];
        item.target_component = payload[3];

        return item;
    }
    mavlink_heartbeat_t Mavlora::ml_unpack_msg_heartbeat (unsigned char *payload)
    {
        /* id=0 */
        /* mavlink/common/heartbeat.h */
        mavlink_heartbeat_t item;
        uint32_t *ui32p;

        ui32p = (uint32_t *) (payload + 0);
        item.custom_mode = *ui32p;

        item.type = payload[4];
        item.autopilot = payload[5];
        item.base_mode = payload[6];
        item.system_status = payload[7];
        item.mavlink_version = payload[8];

        return item;
    }
    mavlink_mission_ack_t Mavlora::ml_unpack_msg_mission_ack (unsigned char *payload)
    {
        /* id=47 */
        /* mavlink/common/mavlink_mission_ack */
        mavlink_mission_ack_t ack;

        ack.target_system = payload[0];
        ack.target_component = payload[1];
        ack.type = payload[2];

        return ack;
    }
    unsigned short Mavlora::ml_unpack_msg_mission_current (unsigned char *payload)
    {
        /* id=42 */
        /* mavlink/common/mavlink_msg_mission_current.h */
        return payload[0] | (payload[1] << 8);
    }
    mavlink_mission_count_t Mavlora::ml_unpack_msg_mission_count (unsigned char *payload)
    {
        /* id=44 */
        /* mavlink/common/mavlink_msg_mission_count.h */
        mavlink_mission_count_t item;

        item.count = payload[0] | (payload[1] << 8);
        item.target_system = payload[2];
        item.target_component = payload[3];

        return item;
    }
    mavlink_mission_partial_write_list_t Mavlora::ml_unpack_msg_mission_partial_write_list(unsigned char *payload)
    {
        /* id=38 */
        /* mavlink/common/mavlink_msg_mission_partial_write_list */
        mavlink_mission_partial_write_list_t item;

        item.start_index = payload[0] | (payload[1] << 8);
        item.end_index = payload[2] | (payload[3] << 8);
        item.target_system = payload[4];
        item.target_component = payload[5];

        return item;
    }
    mavlink_statustext_t Mavlora::ml_unpack_msg_statustext (unsigned char *payload)
    {
        /* id=253 */
        /* mavlink/common/mavlink_msg_statustext.h */
        mavlink_statustext_t statustext;

        statustext.severity = payload[0];
        memcpy (statustext.text, payload+1, 50);

        return statustext;
    }
    mavlink_command_ack_t Mavlora::ml_unpack_msg_command_ack (unsigned char *payload)
    {
        /* id=77 */
        /* mavlink/common/mavlink_command_ack */
        mavlink_command_ack_t ack;

        ack.command = payload[0] | (payload[1] << 8);
        ack.result = payload[2];

        return ack;
    }
    mavlink_radio_status_t Mavlora::ml_unpack_msg_radio_status (unsigned char *payload)
    {
        /* id=109 */
        /* mavlink/common/mavlink_radio_status */
        mavlink_radio_status_t radioStatus;
        uint16_t *ui16p;

        ui16p = (uint16_t *) (payload + 0);
        radioStatus.rxerrors = *ui16p;

        ui16p = (uint16_t *) (payload + 2);
        radioStatus.fixed = *ui16p;

        radioStatus.rssi = payload[4];
        radioStatus.remrssi = payload[5];
        radioStatus.txbuf = payload[6];
        radioStatus.noise = payload[7];
        radioStatus.remnoise = payload[8];

        return radioStatus;
    }
    mavlink_system_time_t Mavlora::ml_unpack_msg_system_time(unsigned char *payload) {
        /* id=2 */
        /* mavlink/common/system_time */
        mavlink_system_time_t systemTime;

        uint32_t *ui32p;
        uint64_t *ui64p;

        ui64p = (uint64_t *) (payload + 0);
        ui32p = (uint32_t *) (payload + 8);

        systemTime.time_unix_usec = *ui64p;
        systemTime.time_boot_ms = *ui32p;

        return systemTime;
    }

    mavlink_altitude_t Mavlora::ml_unpack_msg_altitude(unsigned char *payload) {
        /* id=141 */
        /* mavlink/common/altitude */
        mavlink_altitude_t altitude;

        uint64_t *ui64p;
        float *fp;

        ui64p = (uint64_t *) (payload + 0);
        altitude.time_usec = *ui64p;

        fp = (float * ) (payload + 8);
        altitude.altitude_monotonic = *fp;

        fp = (float * ) (payload + 12);
        altitude.altitude_amsl = *fp;

        fp = (float * ) (payload + 16);
        altitude.altitude_local = *fp;

        fp = (float * ) (payload + 20);
        altitude.altitude_relative = *fp;

        fp = (float * ) (payload + 24);
        altitude.altitude_terrain = *fp;

        fp = (float * ) (payload + 28);
        altitude.bottom_clearence = *fp;

        return altitude;
    }

    mavlink_battery_status_t Mavlora::ml_unpack_msg_battery_status(unsigned char *payload) {
        /* id=147 */
        /* mavlink/common/battery_status */
        mavlink_battery_status_t batteryStatus;

        int32_t *i32p;
        float *fp;
        int16_t *i16p;
        uint16_t *ui16p;

        i32p = (int32_t *) (payload + 0);
        batteryStatus.current_consumed = *i32p;

        i32p = (int32_t *) (payload + 4);
        batteryStatus.energy_consumed = *i32p;

        i16p = (int16_t *) (payload + 8);
        batteryStatus.temperature = *i16p;

        //VOLTAGES
        for ( int i = 0; i < 10; i++)
        {
            ui16p = (uint16_t *) (payload + (10 + (i * 2) ));
            batteryStatus.voltages[i] = *ui16p;
        }

        i16p = (int16_t *) (payload + 30);
        batteryStatus.current_battery = *i16p;

        batteryStatus.id = payload[32];
        batteryStatus.battery_function = payload[33];
        batteryStatus.type = payload[34];
        batteryStatus.battery_remaining = payload[35];

        return batteryStatus;
    }

    mavlink_vibration_t Mavlora::ml_unpack_msg_vibration(unsigned char *payload) {
        /* id=241 */
        /* mavlink/common/vibration */
        mavlink_vibration_t vibration;

        uint64_t *ui64p;
        uint32_t *ui32p;
        float *fp;

        ui64p = (uint64_t *) (payload + 0);
        vibration.time_usec = *ui64p;

        fp = (float * ) (payload + 8);
        vibration.vibration_x = *fp;

        fp = (float * ) (payload + 12);
        vibration.vibration_y = *fp;

        fp = (float * ) (payload + 16);
        vibration.vibration_z = *fp;

        ui32p = (uint32_t *) (payload + 20);
        vibration.clipping_0 = *ui32p;

        ui32p = (uint32_t *) (payload + 24);
        vibration.clipping_1 = *ui32p;

        ui32p = (uint32_t *) (payload + 28);
        vibration.clipping_2 = *ui32p;

        return vibration;
    }

    mavlink_extended_sys_status_t Mavlora::ml_unpack_msg_extended_sys_status(unsigned char *payload) {
        /* id=245 */
        /* mavlink/common/extended_sys_status */
        mavlink_extended_sys_status_t extendedSysStatus;

        extendedSysStatus.vtol_state = payload[0];
        extendedSysStatus.landed_state = payload[1];

        return extendedSysStatus;
    }
    mavlink_command_long_t Mavlora::ml_unpack_msg_command_long(unsigned char *payload) {
        /* id=76 */
        /* mavlink/common/command_long */
        mavlink_command_long_t command_long;

        uint16_t *ui16p;
        float *fp;

        fp = (float * ) (payload + 0);
        command_long.param1 = *fp;

        fp = (float * ) (payload + 4);
        command_long.param2 = *fp;

        fp = (float * ) (payload + 8);
        command_long.param3 = *fp;

        fp = (float * ) (payload + 12);
        command_long.param4 = *fp;

        fp = (float * ) (payload + 16);
        command_long.param5 = *fp;

        fp = (float * ) (payload + 20);
        command_long.param6 = *fp;

        fp = (float * ) (payload + 24);
        command_long.param7 = *fp;

        ui16p = (uint16_t *) (payload + 28);
        command_long.command = *ui16p;

        command_long.target_system = payload[30];
        command_long.target_component = payload[31];
        command_long.confirmation = payload[32];

        return command_long;
    }

    mavlink_command_int_t Mavlora::ml_unpack_msg_command_int(unsigned char *payload) {
        /* id=75 */
        /* mavlink/common/command_int */
        mavlink_command_int_t command_int;

        int32_t *i32p;
        uint16_t *ui16p;
        float *fp;

        i32p = (int32_t * ) (payload + 0);
        command_int.x = *i32p;

        i32p = (int32_t * ) (payload + 4);
        command_int.y = *i32p;

        fp = (float *) (payload + 8);
        command_int.param1 = *fp;

        fp = (float *) (payload + 12);
        command_int.param2 = *fp;

        fp = (float *) (payload + 16);
        command_int.param3 = *fp;

        fp = (float *) (payload + 20);
        command_int.param4 = *fp;

        fp = (float *) (payload + 24);
        command_int.z = *fp;

        ui16p = (uint16_t *) (payload + 28);
        command_int.command = *ui16p;

        command_int.target_system = payload[30];
        command_int.target_component = payload[31];
        command_int.frame = payload[32];
        command_int.current = payload[33];
        command_int.autocontinue = payload[34];

        return command_int;
    }

    mavlink_home_position_t Mavlora::ml_unpack_msg_home_position(unsigned char *payload) {
        /* id=242 */
        /* mavlink/common/home_position */
        mavlink_home_position_t home_position;

        int32_t *i32p;
        float *fp;

        i32p = (int32_t * ) (payload + 0);
        home_position.latitude = *i32p;

        i32p = (int32_t * ) (payload + 4);
        home_position.longitude = *i32p;

        i32p = (int32_t * ) (payload + 8);
        home_position.altitude = *i32p;

        fp = (float * ) (payload + 12);
        home_position.x = *fp;

        fp = (float * ) (payload + 16);
        home_position.y = *fp;

        fp = (float * ) (payload + 20);
        home_position.z = *fp;

        for ( int i = 0; i < 4; i++)
        {
            fp = (float *) (payload + (24 + (i * 4) ));
            home_position.q[i] = *fp;
        }

        fp = (float * ) (payload + 40);
        home_position.approach_x = *fp;

        fp = (float * ) (payload + 44);
        home_position.approach_y = *fp;

        fp = (float * ) (payload + 48);
        home_position.approach_z = *fp;

        return home_position;
    }
    mavlink_rc_channels_raw_t Mavlora::ml_unpack_msg_rc_channels_raw(unsigned char *payload) {
        /* id=35 */
        /* mavlink/common/rc_channels_raw */
        mavlink_rc_channels_raw_t rc_channels_raw;

        uint32_t *ui32p;
        uint16_t *ui16p;

        // system time
        ui32p = (uint32_t * ) (payload + 0);
        rc_channels_raw.time_boot_ms = *ui32p;

        // channel 1
        ui16p = (uint16_t * ) (payload + 4);
        rc_channels_raw.chan1_raw = *ui16p;

        // channel 2
        ui16p = (uint16_t * ) (payload + 6);
        rc_channels_raw.chan2_raw = *ui16p;

        // channel 3
        ui16p = (uint16_t * ) (payload + 8);
        rc_channels_raw.chan3_raw = *ui16p;

        // channel 4
        ui16p = (uint16_t * ) (payload + 10);
        rc_channels_raw.chan4_raw = *ui16p;

        // channel 5
        ui16p = (uint16_t * ) (payload + 12);
        rc_channels_raw.chan5_raw = *ui16p;

        // channel 6
        ui16p = (uint16_t * ) (payload + 14);
        rc_channels_raw.chan6_raw = *ui16p;

        // channel 7
        ui16p = (uint16_t * ) (payload + 16);
        rc_channels_raw.chan7_raw = *ui16p;

        // channel 8
        ui16p = (uint16_t * ) (payload + 18);
        rc_channels_raw.chan8_raw = *ui16p;

        rc_channels_raw.port = payload[20];
        rc_channels_raw.rssi = payload[21];

        return rc_channels_raw;
    }

    mavlink_rc_channels_t Mavlora::ml_unpack_msg_rc_channels(unsigned char *payload) {
        /* id=35 */
        /* mavlink/common/rc_channels_raw */
        mavlink_rc_channels_t rcChannels;

        uint32_t *ui32p;
        uint16_t *ui16p;

        // system time
        ui32p = (uint32_t * ) (payload + 0);
        rcChannels.time_boot_ms = *ui32p;

        // channel 1
        ui16p = (uint16_t * ) (payload + 4);
        rcChannels.chan1_raw = *ui16p;

        // channel 2
        ui16p = (uint16_t * ) (payload + 6);
        rcChannels.chan2_raw = *ui16p;

        // channel 3
        ui16p = (uint16_t * ) (payload + 8);
        rcChannels.chan3_raw = *ui16p;

        // channel 4
        ui16p = (uint16_t * ) (payload + 10);
        rcChannels.chan4_raw = *ui16p;

        // channel 5
        ui16p = (uint16_t * ) (payload + 12);
        rcChannels.chan5_raw = *ui16p;

        // channel 6
        ui16p = (uint16_t * ) (payload + 14);
        rcChannels.chan6_raw = *ui16p;

        // channel 7
        ui16p = (uint16_t * ) (payload + 16);
        rcChannels.chan7_raw = *ui16p;

        // channel 8
        ui16p = (uint16_t * ) (payload + 18);
        rcChannels.chan8_raw = *ui16p;

        // channel 9
        ui16p = (uint16_t * ) (payload + 20);
        rcChannels.chan9_raw = *ui16p;

        // channel 10
        ui16p = (uint16_t * ) (payload + 22);
        rcChannels.chan10_raw = *ui16p;

        // channel 11
        ui16p = (uint16_t * ) (payload + 24);
        rcChannels.chan11_raw = *ui16p;

        // channel 12
        ui16p = (uint16_t * ) (payload + 26);
        rcChannels.chan12_raw = *ui16p;

        // channel 13
        ui16p = (uint16_t * ) (payload + 28);
        rcChannels.chan13_raw = *ui16p;

        // channel 14
        ui16p = (uint16_t * ) (payload + 30);
        rcChannels.chan14_raw = *ui16p;

        // channel 15
        ui16p = (uint16_t * ) (payload + 32);
        rcChannels.chan15_raw = *ui16p;

        // channel 16
        ui16p = (uint16_t * ) (payload + 34);
        rcChannels.chan16_raw = *ui16p;

        // channel 17
        ui16p = (uint16_t * ) (payload + 36);
        rcChannels.chan17_raw = *ui16p;

        // channel 18
        ui16p = (uint16_t * ) (payload + 38);
        rcChannels.chan18_raw = *ui16p;

        rcChannels.chancount = payload[40];
        rcChannels.rssi = payload[41];

        return rcChannels;
    }
    mavlink_radio_comm_status_t Mavlora::ml_unpack_msg_radio_comm_status(unsigned char *payload) {
        /* id=15 */
        /* Own message defined */
        mavlink_radio_comm_status_t comm_status;

        uint32_t *u32p;
        double *doub;
        uint16_t *ui16p;

        doub = (double * ) (payload + 0);
        comm_status.lastheard_fc = *doub;

        doub = (double * ) (payload + 8);
        comm_status.lastheard_gcs = *doub;

        u32p = (uint32_t * ) (payload + 16);
        comm_status.lost_messages_gcs = *u32p;

        u32p = (uint32_t * ) (payload + 20);
        comm_status.lost_messages_fc = *u32p;

        u32p = (uint32_t * ) (payload + 24);
        comm_status.received_msgs_gcs = *u32p;

        u32p = (uint32_t * ) (payload + 28);
        comm_status.received_msgs_fc = *u32p;

        u32p = (uint32_t * ) (payload + 32);
        comm_status.transmitted_msgs_gcs = *u32p;

        u32p = (uint32_t * ) (payload + 36);
        comm_status.transmitted_msgs_fc = *u32p;

        ui16p = (uint16_t *) (payload + 40);
        comm_status.dualrx_status = *ui16p;

        return comm_status;
    }
    mavlink_gps_rtcm_data_t Mavlora::ml_unpack_msg_gps_rtcm_data(unsigned char *payload) {
        /* id=233 */
        /* Own message defined */
        mavlink_gps_rtcm_data_t rtk_data;

        rtk_data.flags = payload[0];

        rtk_data.len = payload[1];

        for (int i = 0; i < 180; i++)
        {
            rtk_data.data[i] = payload[2 + i];
        }

        return rtk_data;
    }
    mavlink_lte_metrics_t Mavlora::ml_unpack_msg_lte_metrics(unsigned char *payload) {
        /* id=13 */
        /* Own message defined */
        mavlink_lte_metrics_t msg;

        uint64_t *u64p;
        int32_t *i32p;
        uint16_t *u16p;

        u64p = (uint64_t * ) (payload + 0);
        msg.request_time = *u64p;

        u64p = (uint64_t * ) (payload + 8);
        msg.response_time = *u64p;

        i32p = (int32_t * ) (payload + 16);
        msg.snr = *i32p;

        i32p = (int32_t * ) (payload + 20);
        msg.rsrq = *i32p;

        i32p = (int32_t * ) (payload + 24);
        msg.rssi = *i32p;

        i32p = (int32_t * ) (payload + 28);
        msg.rsrp = *i32p;

        i32p = (int32_t * ) (payload + 32);
        msg.pci = *i32p;

        i32p = (int32_t * ) (payload + 36);
        msg.lat = *i32p;

        i32p = (int32_t * ) (payload + 40);
        msg.lon = *i32p;

        i32p = (int32_t * ) (payload + 44);
        msg.alt_msl = *i32p;

        u16p = (uint16_t *) (payload + 48);
        msg.heading = *u16p;

        return msg;
    }
    mavlink_nr5g_metrics_t Mavlora::ml_unpack_msg_nr5g_metrics(unsigned char *payload) {
        /* id=13 */
        /* Own message defined */
        mavlink_nr5g_metrics_t msg;

        uint64_t *u64p;
        int32_t *i32p;
        uint16_t *u16p;

        u64p = (uint64_t * ) (payload + 0);
        msg.request_time = *u64p;

        u64p = (uint64_t * ) (payload + 8);
        msg.response_time = *u64p;

        i32p = (int32_t * ) (payload + 16);
        msg.snr = *i32p;

        i32p = (int32_t * ) (payload + 20);
        msg.rsrp = *i32p;

        i32p = (int32_t * ) (payload + 24);
        msg.pci = *i32p;

        i32p = (int32_t * ) (payload + 28);
        msg.lat = *i32p;

        i32p = (int32_t * ) (payload + 32);
        msg.lon = *i32p;

        i32p = (int32_t * ) (payload + 36);
        msg.alt_msl = *i32p;

        u16p = (uint16_t *) (payload + 40);
        msg.heading = *u16p;

        return msg;
    }
}