/*
 * Copyright (c) 2019, Frederik Mazur Andersen <fm@mmmi.sdu.dk>/<fm@fmazur.dk>
 *
 * SDU UAS Center, http://sdu.dk/uas, University of Southern Denmark
 * All rights reserved.
 *
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE.txt', which is part of this source code package.
 */

#include "include/mavlora.hpp"
#include <gtest/gtest.h>
#include <vector>
#include <chrono>

using namespace mavlora;

TEST(serial, transmit)
{
    Mavlora mavlora;

    // make buffer
    unsigned char rxbuf_serial_read[RX_BUF_SIZE];
    unsigned char txbuf[TX_BUF_SIZE];
    short txbuf_cnt = 0;

    // make virtual serial ports
    std::cout << "Starting virtuel serial ports: /tmp/PX2 and /tmp/PX3" << std::endl;
    system("socat -d -d pty,raw,echo=0,link=/tmp/PX2 pty,raw,echo=0,link=/tmp/PX3 &");
    // sleep to ensure socat has created the devices before we progress
    sleep(1);

    std::string serial_device_tx = "/tmp/PX2";
    std::string serial_device_rx = "/tmp/PX3";

    // serial vars
    int ser_ref_tx;
    int ser_ref_rx;
    struct termios oldtio_tx;
    struct termios oldtio_rx;

    ser_open(&ser_ref_tx, &oldtio_tx, (char *) serial_device_tx.c_str(), 57600);
    ser_open(&ser_ref_rx, &oldtio_rx, (char *) serial_device_rx.c_str(), 57600);

    // make data to send
    unsigned char *buf = (txbuf + txbuf_cnt);
    buf[0] = 0;
    buf[1] = 1;
    buf[2] = 2;
    buf[3] = 3;
    buf[4] = 4;
    buf[5] = 5;
    buf[6] = 6;
    buf[7] = 7;
    buf[8] = 8;
    buf[9] = 9;
    buf[10] = 10;
    buf[11] = 11;
    buf[12] = 12;
    buf[13] = 13;
    buf[14] = 14;
    buf[15] = 15;

    txbuf_cnt = 16;

    bool testing = true;
    auto _now = std::chrono::system_clock::now();
    auto _lastSent = std::chrono::system_clock::now();

    // verify server received correct data
    while (testing)
    {
        _now = std::chrono::system_clock::now();
        std::chrono::duration<double> _diff = _now - _lastSent;
        int received_bytes = ser_receive(ser_ref_rx, rxbuf_serial_read, RX_BUF_SIZE);
        if (received_bytes > 0)
        {
            std::cout << "received data: ";
            for (auto i = 0; i < txbuf_cnt; i++)
            {
                std::cout << std::to_string(rxbuf_serial_read[i]) << ", ";
            }
            std::cout << std::endl;

            ASSERT_EQ(rxbuf_serial_read[0], buf[0]);
            ASSERT_EQ(rxbuf_serial_read[1], buf[1]);
            ASSERT_EQ(rxbuf_serial_read[2], buf[2]);
            ASSERT_EQ(rxbuf_serial_read[3], buf[3]);
            ASSERT_EQ(rxbuf_serial_read[4], buf[4]);
            ASSERT_EQ(rxbuf_serial_read[5], buf[5]);
            ASSERT_EQ(rxbuf_serial_read[6], buf[6]);
            ASSERT_EQ(rxbuf_serial_read[7], buf[7]);
            ASSERT_EQ(rxbuf_serial_read[8], buf[8]);
            ASSERT_EQ(rxbuf_serial_read[9], buf[9]);
            ASSERT_EQ(rxbuf_serial_read[10], buf[10]);
            ASSERT_EQ(rxbuf_serial_read[11], buf[11]);
            ASSERT_EQ(rxbuf_serial_read[12], buf[12]);
            ASSERT_EQ(rxbuf_serial_read[13], buf[13]);
            ASSERT_EQ(rxbuf_serial_read[14], buf[14]);
            ASSERT_EQ(rxbuf_serial_read[15], buf[15]);
            testing = false;
            break;
        }
        if (_diff.count() > 2)
        {
            int result = ser_send(ser_ref_tx, txbuf, txbuf_cnt);
            _lastSent = std::chrono::system_clock::now();
        }
    }
    ser_close(ser_ref_tx, oldtio_tx);
    ser_close(ser_ref_rx, oldtio_rx);
}

TEST(MavLora, transmitMavlinkMsgSerial) {
    Mavlora mavlora_1;
    Mavlora mavlora_2;

    // make virtual serial ports
    std::cout << "Starting virtuel serial ports: /tmp/PX2 and /tmp/PX3" << std::endl;
    system("socat -d -d pty,raw,echo=0,link=/tmp/PX2 pty,raw,echo=0,link=/tmp/PX3 &");
    // sleep to ensure socat has created the devices before we progress
    sleep(1);

    bool testing = true;
    auto _now = std::chrono::system_clock::now();
    auto _lastSent = std::chrono::system_clock::now();

    // lambda function for receiving the message
    auto func = [&testing](unsigned char *msg) {
        // extract message info
        struct mavlink_msg_t m;
        m.payload_len = msg[ML_POS_PAYLOAD_LEN];
        m.seq = msg[ML_POS_PACKET_SEQ];
        m.sys_id = msg[ML_POS_SYS_ID];
        m.comp_id = msg[ML_POS_COMP_ID];
        m.msg_id = msg[ML_POS_MSG_ID];

        for (auto i=0; i<m.payload_len; i++) {
            m.payload.push_back(msg[ML_POS_PAYLOAD + i]);
        }

        unsigned char crc_lsb = msg[6 + m.payload_len];
        unsigned char crc_msb = msg[7 + m.payload_len];
        m.checksum = (8 << crc_msb) | crc_lsb;

        std::cout << "received mavlink message ID: " << std::to_string(m.msg_id) << std::endl;

        // check if msg is mission request, and then check if payload matches
        if (m.msg_id == MAVLINK_MSG_ID_MISSION_REQUEST_INT)
        {
            mavlink_mission_request_int_t request = Mavlora::ml_unpack_msg_mission_request_int(&m.payload.front());
            std::cout << "Expected mission request 240, received: " << std::to_string(request.seq) << std::endl;
            ASSERT_EQ(request.seq, 240);
            testing = false;
        }
    };

    // init
    mavlora_1.init("Sender", 0, func);
    mavlora_2.init("receiver", 1, func);

    mavlora_1.enableCommunicationSerial("/tmp/PX2", 57600);
    mavlora_2.enableCommunicationSerial("/tmp/PX3", 57600);

    // send initial msg
    mavlora_1.ml_queue_msg_mission_request(240, 0);

    // loop receiver
    while (testing)
    {
        // loop update
        mavlora_1.update();
        mavlora_2.update();

        // keep resending every second to make sure we didn't loose initial msg
        _now = std::chrono::system_clock::now();
        std::chrono::duration<double> _diff = _now - _lastSent;

        if (_diff.count() > 2)
        {
            mavlora_1.ml_queue_msg_mission_request(240, 0);
            _lastSent = std::chrono::system_clock::now();
        }
    }

    // cleanup
    mavlora_1.disableCommunicationSerial();
    mavlora_2.disableCommunicationSerial();
}