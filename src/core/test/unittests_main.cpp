#include <gtest/gtest.h>
#include <gmock/gmock.h>
#include "include/mavlora.hpp"

using namespace mavlora;

int main(int argc, char* argv[])
{
    ::testing::InitGoogleMock(&argc, argv);
    return RUN_ALL_TESTS();
}
