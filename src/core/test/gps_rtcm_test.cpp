/*
 * Copyright (c) 2019, Frederik Mazur Andersen <fm@mmmi.sdu.dk>/<fm@fmazur.dk>
 *
 * SDU UAS Center, http://sdu.dk/uas, University of Southern Denmark 
 * All rights reserved.
 * 
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE.txt', which is part of this source code package.
 */

#include "include/mavlora.hpp"
#include <gtest/gtest.h>
#include <vector>

using namespace mavlora;

TEST(GPS_RTCM, packing)
{
    Mavlora mavlora;

    // make test data
    uint8_t data_input[180] = {0};
    data_input[0] = 150;
    data_input[1] = 2;
    data_input[2] = 5;
    data_input[3] = 9;
    data_input[4] = 240;
    data_input[5] = 1;
    data_input[6] = 0;
    data_input[7] = 255;

    uint8_t flag = 0b01001010;
    uint8_t len = 22;

    // pack and queue data
    mavlora.ml_queue_msg_gps_rtcm_data(flag, len, data_input);
    // get data from tx-buffer
    auto txbuf = mavlora._getTXBUF();

    // copy payload into own array for unpack
    struct mavlink_msg_t m;
    for (auto i = 0; i < MAVLINK_MSG_ID_GPS_RTCM_DATA_LEN; i++) {
        m.payload.push_back(txbuf[ML_POS_PAYLOAD + i]);
    }

    // unpack txbuffer
    auto result = Mavlora::ml_unpack_msg_gps_rtcm_data(&m.payload.front());

    // validate results
    ASSERT_EQ(flag, result.flags);
    ASSERT_EQ(len, result.len);

    uint32_t buffer_size = sizeof(data_input) / sizeof(data_input[0]);

    for (int i = 0; i < buffer_size; ++i) {
        EXPECT_EQ(data_input[i], result.data[i]) << "Vectors x and y differ at index " << i;
    }
}