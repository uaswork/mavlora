/*
 * Copyright (c) 2022, Frederik Mazur Andersen <fm@mmmi.sdu.dk>/<fm@fmazur.dk>
 *
 * SDU UAS Center, http://sdu.dk/uas, University of Southern Denmark 
 * All rights reserved.
 * 
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE.txt', which is part of this source code package.
 */

#include "include/mavlora.hpp"
#include <gtest/gtest.h>
#include <vector>

using namespace mavlora;

TEST(LTE_METRICS, packing)
{
    Mavlora mavlora;

    // make test data
    uint64_t request_time = 05600;
    uint64_t response_time = 05601;
    int32_t snr = -5;
    int32_t rsrq = 10;
    int32_t rssi = -40;
    int32_t rsrp = -5;
    int32_t pci = 12;
    int32_t lat = 55471893;
    int32_t lon = 10323331;
    int32_t alt_msl = 22;
    int32_t heading = 225;

    // pack and queue data
    mavlora.ml_queue_msg_lte_metrics(request_time, response_time, snr, rsrq, rssi, rsrp, pci, lat, lon, alt_msl, heading);
    // get data from tx-buffer
    auto txbuf = mavlora._getTXBUF();

    // copy payload into own array for unpack
    struct mavlink_msg_t m;
    for (auto i = 0; i < MAVLINK_MSG_ID_LTE_METRICS_LEN; i++) {
        m.payload.push_back(txbuf[ML_POS_PAYLOAD + i]);
    }

    // unpack txbuffer
    auto result = Mavlora::ml_unpack_msg_lte_metrics(&m.payload.front());

    // validate results
    ASSERT_EQ(request_time, result.request_time);
    ASSERT_EQ(response_time, result.response_time);
    ASSERT_EQ(snr, result.snr);
    ASSERT_EQ(rsrq, result.rsrq);
    ASSERT_EQ(rssi, result.rssi);
    ASSERT_EQ(rsrp, result.rsrp);
    ASSERT_EQ(pci, result.pci);
    ASSERT_EQ(lat, result.lat);
    ASSERT_EQ(lon, result.lon);
    ASSERT_EQ(alt_msl, result.alt_msl);
    ASSERT_EQ(heading, result.heading);
}
TEST(NR5G_METRICS, packing)
{
    Mavlora mavlora;

    // make test data
    uint64_t request_time = 05600;
    uint64_t response_time = 05601;
    int32_t snr = -5;
    int32_t rsrp = 10;
    int32_t pci = 12;
    int32_t lat = 55471893;
    int32_t lon = 10323331;
    int32_t alt_msl = 22;
    int32_t heading = 225;

    // pack and queue data
    mavlora.ml_queue_msg_nr5g_metrics(request_time, response_time, snr, rsrp, pci, lat, lon, alt_msl, heading);
    // get data from tx-buffer
    auto txbuf = mavlora._getTXBUF();

    // copy payload into own array for unpack
    struct mavlink_msg_t m;
    for (auto i = 0; i < MAVLINK_MSG_ID_NR5G_METRICS_LEN; i++) {
        m.payload.push_back(txbuf[ML_POS_PAYLOAD + i]);
    }

    // unpack txbuffer
    auto result = Mavlora::ml_unpack_msg_nr5g_metrics(&m.payload.front());

    // validate results
    ASSERT_EQ(request_time, result.request_time);
    ASSERT_EQ(response_time, result.response_time);
    ASSERT_EQ(snr, result.snr);
    ASSERT_EQ(rsrp, result.rsrp);
    ASSERT_EQ(pci, result.pci);
    ASSERT_EQ(lat, result.lat);
    ASSERT_EQ(lon, result.lon);
    ASSERT_EQ(alt_msl, result.alt_msl);
    ASSERT_EQ(heading, result.heading);
}