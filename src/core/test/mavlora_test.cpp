/*
 * Copyright (c) 2019, Frederik Mazur Andersen <fm@mmmi.sdu.dk>/<fm@fmazur.dk>
 *
 * SDU UAS Center, http://sdu.dk/uas, University of Southern Denmark 
 * All rights reserved.
 * 
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE.txt', which is part of this source code package.
 */

#include "include/mavlora.hpp"
#include <gtest/gtest.h>
#include <vector>

using namespace mavlora;

TEST(Mavlora, version)
{
    Mavlora mavlora;
    std::cout << mavlora.version() << ", size: " << std::to_string(mavlora.version().size()) << std::endl;
    ASSERT_GE(mavlora.version().size(), 5);
}