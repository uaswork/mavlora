/*
 * Copyright (c) 2021, Frederik Mazur Andersen <fm@mmmi.sdu.dk>/<fm@fmazur.dk>
 *
 * SDU UAS Center, http://sdu.dk/uas, University of Southern Denmark
 * All rights reserved.
 *
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE.txt', which is part of this source code package.
 */

#ifndef MAVLORA_SOCKET_HPP
#define MAVLORA_SOCKET_HPP

#include <iostream>
#include <string>
#include <cstring>
#include <utility>
#include <sys/types.h>  // data types
#include <sys/socket.h> // UDPsocket(), connect(), send(), recv()
#include <arpa/inet.h>  // inet_addr()
#include <unistd.h>     // close()
#include <netinet/in.h> // sockaddr_in
#include <cerrno>

#pragma once


class UDPsocket {

public:
    UDPsocket();
    UDPsocket(std::string target_address, int target_port);

    // gives the buffer to put new data on
    bool enablelisten(int recv_p, unsigned char *buffer, unsigned int buffer_size);
    void disableListen();
    int receiveData();
    int sendData(unsigned char* tx_buf, short &tx_buf_cnt);

private:
    // UDPsocket details
    int sockfd;
    struct sockaddr_in serv_addr, target_addr;
    socklen_t target_addr_len;

    // pointer to receiving buffer
    unsigned char* recv_buffer;
    unsigned int recv_buffer_size;

    // struct for connection information from where we last received data from
    struct sockaddr_storage received_addr;
    socklen_t received_addr_len = sizeof(received_addr);

    // target ip and port
    std::string out_addr;
    int out_port;

    // enable listening aka UDP server
    bool listen_enabled = false;
    int recv_port;
};


#endif //MAVLORA_SOCKET_HPP
