/*
 * Copyright (c) 2019, Frederik Mazur Andersen <fm@mmmi.sdu.dk>/<fm@fmazur.dk>
 *
 * SDU UAS Center, http://sdu.dk/uas, University of Southern Denmark 
 * All rights reserved.
 * 
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE.txt', which is part of this source code package.
 */

#ifndef MAVLORA_MAVLORA_HPP
#define MAVLORA_MAVLORA_HPP

#include <string>
#include <vector>
#include <functional>
#include <iostream>
#include <ctime>
#include <cstring>
#include <unordered_map>
#include <cmath>
#include "mavlink_messages.hpp"
#include "UDPsocket.hpp"

extern "C"
{
#include "serial.h"
}

namespace mavlora {

    /* Defines */
    #define RX_BUF_SIZE	16000 /* size of the receive buffer, depends on system and application */
    #define PARAM_TOUT 2000 /* [ms] timeout for other msgs when exchanging parameters */
    #define MISSION_TOUT 2000 /* [ms] timeout for other msgs when exchanging mission */
    #define TX_BUF_SIZE 200

    struct LostMessages
    {
        unsigned long last_seq = 0;
        unsigned long lostMessages = 0;
    };

    class Mavlora {

    public:
        Mavlora();
        std::string version() const;

        /// program workers
        // Init for serial communication
        void init(std::string identify_name, unsigned char sys_id, std::function<void(unsigned char *)> callback);

        // communication enable/disable
        bool enableCommunicationUDP(std::string udp_target, int port);
        bool enableCommunicationUDP(std::string udp_target, int port, int listen_port);
        bool enableCommunicationSerial(std::string serial_dev, int baudrate);
        void disableCommunicationUDP();
        void disableCommunicationSerial();

        void update();
        void shutdown();
        int reopen_serial();

        /// Serial
        void ml_forward_msg(unsigned char * inc_buf);

        /// Static unpack functions
        static mavlink_sys_status_t ml_unpack_msg_sys_status (unsigned char *payload);
        static mavlink_attitude_t ml_unpack_msg_attitude (unsigned char *payload);
        static mavlink_gps_raw_int_t ml_unpack_msg_gps_raw_int (unsigned char *payload);
        static mavlink_global_position_int_t ml_unpack_msg_global_position_int (unsigned char *payload);
        static mavlink_mission_count_t ml_unpack_msg_mission_count (unsigned char *payload);
        static mavlink_mission_partial_write_list_t ml_unpack_msg_mission_partial_write_list (unsigned char *payload);
        static mavlink_mission_item_t ml_unpack_msg_mission_item (unsigned char *payload);
        static mavlink_mission_item_int_t ml_unpack_msg_mission_item_int (unsigned char *payload);
        static unsigned short ml_unpack_msg_mission_current (unsigned char *payload);
        static mavlink_mission_request_int_t ml_unpack_msg_mission_request_int (unsigned char *payload);
        static mavlink_mission_ack_t ml_unpack_msg_mission_ack (unsigned char *payload);
        static mavlink_statustext_t ml_unpack_msg_statustext (unsigned char *payload);
        static mavlink_command_ack_t ml_unpack_msg_command_ack (unsigned char *payload);
        static mavlink_heartbeat_t ml_unpack_msg_heartbeat (unsigned char *payload);
        static mavlink_radio_status_t ml_unpack_msg_radio_status (unsigned char *payload);
        static mavlink_system_time_t ml_unpack_msg_system_time (unsigned char *payload);
        static mavlink_altitude_t ml_unpack_msg_altitude (unsigned char *payload);
        static mavlink_battery_status_t ml_unpack_msg_battery_status (unsigned char *payload);
        static mavlink_vibration_t ml_unpack_msg_vibration (unsigned char *payload);
        static mavlink_extended_sys_status_t ml_unpack_msg_extended_sys_status (unsigned char *payload);
        static mavlink_command_long_t ml_unpack_msg_command_long (unsigned char *payload);
        static mavlink_command_int_t ml_unpack_msg_command_int (unsigned char *payload);
        static mavlink_home_position_t ml_unpack_msg_home_position (unsigned char *payload);
        static mavlink_radio_comm_status_t ml_unpack_msg_radio_comm_status(unsigned char *payload);
        static mavlink_rc_channels_raw_t ml_unpack_msg_rc_channels_raw(unsigned char *payload);
        static mavlink_rc_channels_t ml_unpack_msg_rc_channels(unsigned char *payload);
        static mavlink_gps_rtcm_data_t ml_unpack_msg_gps_rtcm_data(unsigned char *payload);
        static mavlink_lte_metrics_t ml_unpack_msg_lte_metrics(unsigned char *payload);
        static mavlink_nr5g_metrics_t ml_unpack_msg_nr5g_metrics(unsigned char *payload);


        /// Functions
        void ml_queue_msg_generic (unsigned char sys_id, unsigned char comp_id, unsigned char msg_id, unsigned char payload_len, unsigned char *payload);
        void ml_queue_msg_mission_request(unsigned short seq, unsigned short system_id);
        void ml_queue_msg_mission_request_list ();
        void ml_queue_msg_mission_ack (uint8_t result);
        void ml_queue_msg_mission_count (unsigned short count);
        void ml_queue_msg_mission_clear_all ();
        void ml_queue_msg_mission_item_int (float param1, float param2, float param3, float param4, int32_t x, int32_t y, float z, unsigned short seq, unsigned short command, unsigned char frame, unsigned char current, unsigned char autocontinue);
        void ml_queue_msg_command_long (unsigned short cmd_id, float param1, float param2, float param3, float param4, float param5, float param6, float param7, unsigned int confirmation);
        void ml_queue_msg_command_int (unsigned short cmd_id, unsigned char frame, float param1, float param2, float param3, float param4, int32_t x, int32_t y, float z);
        void ml_queue_msg_command_ack (unsigned short cmd_id, unsigned char result);
        void ml_queue_msg_heartbeat(unsigned char type, unsigned char autopilot, unsigned char base_mode, unsigned long custom_mode, unsigned char system_status, unsigned char system_id);
        void ml_queue_msg_radio_comm_status(double lastheard_fc, double lastheard_gcs, uint32_t lost_msgs_gcs, uint32_t lost_msgs_fc, uint32_t received_msgs_gcs, uint32_t received_msgs_fc, uint32_t transmit_msgs_gcs, uint32_t transmit_msgs_fc, uint16_t dualrx_status);
        void ml_queue_msg_gps_rtcm_data(uint8_t flags, uint8_t len, const uint8_t* data);
        void ml_queue_msg_lte_metrics(uint64_t request_time, uint64_t response_time, int32_t snr, int32_t rsrq, int32_t rssi, int32_t rsrp, int32_t pci, int32_t lat, int32_t lon, int32_t alt_msl, uint16_t heading);
        void ml_queue_msg_nr5g_metrics(uint64_t request_time, uint64_t response_time, int32_t snr, int32_t rsrp, int32_t pci, int32_t lat, int32_t lon, int32_t alt_msl, uint16_t heading);

        /// Communication helpers
        unsigned long ml_messages_sent();
        unsigned long ml_messages_received();
        unsigned long ml_messages_crc_error();
        unsigned long ml_messages_lost(unsigned int sys_id);

        /// Test helper (for packing tests I need a copy of the txbuf)
        unsigned char* _getTXBUF();

    private:
        // serial device
        int ser_ref;
        struct termios oldtio;

        // udp connection
        UDPsocket udp_client;

        // variables for what communication is active
        // TODO we can only handle one active at a time, as they both put bytes into the same rx_buffer in our serialization.
        //  ideally we should maybe change the flow, so update function checks for packages parallel in their own buffers and only copy the
        //  received messages over to the internal rx-buffer.
        bool udp_active = false;
        bool serial_active = false;

        std::string identifier_name;
        std::string serial_device;
        int baudrate;

        // Buffers
        unsigned char txbuf[TX_BUF_SIZE] = {0};
        unsigned char rxbuf_serial_read[RX_BUF_SIZE] = {0};   //Required as raw buffer to store inc. bytes from serial.
        unsigned char rxbuf_socket_read[RX_BUF_SIZE] = {0};   //Required as raw buffer to store inc. bytes from UDPsocket.
        unsigned char rxbuf[RX_BUF_SIZE] = {0};               //Is the internal queue of bytes as we receive them.

        short txbuf_cnt = 0;
        short rxbuf_cnt = 0;
        int tx_seq = 0;
        short msg_begin = -1;

        unsigned long msg_rx_cnt, msg_crc_err, msg_tx_cnt;
        unsigned long msg_buf_overflow;

        // hashmap for last seq received and number of lost messages
        std::unordered_map<unsigned int, LostMessages> lost_messages_map;

        unsigned char recorded_sysid;
        unsigned char my_sysid;

        unsigned char do_send_msg;
        unsigned long param_mission_tout;

        unsigned long secs_init;

        bool debug = false;

        unsigned long millis();

        //CRC Check - Own custom messages have ID 15, and is thus placed in position 15 of the array, originally 0, now 68
        const std::vector<unsigned char> MAVLINK_MESSAGE_CRCS_V1 = {50, 124, 137, 0, 237, 217, 104, 119, 0, 0,
                                                                    0, 89, 0, 222, 78, 58, 0, 0, 0, 0,
                                                                    214, 159, 220, 168, 24, 23, 170, 144, 67, 115,
                                                                    39, 246, 185, 104, 237, 244, 222, 212, 9, 254,
                                                                    230, 28, 28, 132, 221, 232, 11, 153, 41, 39,
                                                                    78, 196, 0, 0, 15, 3, 0, 0, 0, 0,
                                                                    0, 167, 183, 119, 191, 118, 148, 21, 0, 243,
                                                                    124, 0, 0, 38, 20, 158, 152, 143, 0, 0,
                                                                    0, 106, 49, 22, 143, 140, 5, 150, 0, 231,
                                                                    183, 63, 54, 47, 0, 0, 0, 0, 0, 0,
                                                                    175, 102, 158, 208, 56, 93, 138, 108, 32, 185,
                                                                    84, 34, 174, 124, 237, 4, 76, 128, 56, 116,
                                                                    134, 237, 203, 250, 87, 203, 220, 25, 226, 46,
                                                                    29, 223, 85, 6, 229, 203, 1, 195, 109, 168,
                                                                    181, 47, 72, 131, 127, 0, 103, 154, 178, 200,
                                                                    134, 219, 208, 188, 84, 22, 19, 21, 134, 0,
                                                                    78, 68, 189, 127, 154, 21, 21, 144, 1, 234,
                                                                    73, 181, 22, 83, 167, 138, 234, 240, 47, 189,
                                                                    52, 174, 229, 85, 159, 186, 72, 0, 0, 0,
                                                                    0, 92, 36, 71, 98, 0, 0, 0, 0, 0,
                                                                    134, 205, 0, 0, 0, 0, 0, 0, 0, 0,
                                                                    0, 0, 0, 0, 69, 101, 50, 202, 17, 162,
                                                                    0, 0, 0, 0, 0, 0, 207, 0, 0, 0,
                                                                    163, 105, 151, 35, 150, 0, 0, 0, 0, 0,
                                                                    0, 90, 104, 85, 95, 130, 184, 81, 8, 204,
                                                                    49, 170, 44, 83, 46, 0};

        /// Serial Functions
        short ml_rx_update(unsigned long ms, unsigned char *buf, short buf_cnt);
        void ml_tx_update();
        void ml_queue_msg(unsigned char *buf, unsigned char sys_id);

        /// Callback function
        std::function<void(unsigned char *msg)> parse_msg_callback;
    };
}

#endif //MAVLORA_MAVLORA_HPP
