#!/usr/bin/env python

import sys, textwrap
from argparse import ArgumentParser

import mavxml

msgs = []
enums = []

m = mavxml.MAVXML('../../docs/mavlora.xml')
msgs.extend(m.message)

print("Found %u MAVLink messages" % len(msgs))
print("CRC_EXTRA:")
print(m.message_crcs)

