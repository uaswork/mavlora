#!/usr/bin/env python
# /***************************************************************************
# MavLink LoRa node (ROS) upload mission example script
# Copyright (c) 2018, Kjeld Jensen <kjen@mmmi.sdu.dk> <kj@kjen.dk>
# SDU UAS Center, http://sdu.dk/uas 
# All rights reserved.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met:
#    * Redistributions of source code must retain the above copyright
#      notice, this list of conditions and the following disclaimer.
#    * Redistributions in binary form must reproduce the above copyright
#      notice, this list of conditions and the following disclaimer in the
#      documentation and/or other materials provided with the distribution.
#    * Neither the name of the copyright holder nor the names of its
#      contributors may be used to endorse or promote products derived from
#      this software without specific prior written permission.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
# ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
# WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
# DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
# DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
# (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
# LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
# ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
# (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
# SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
# ****************************************************************************
'''
This example script shows how to upload a mission to a drone and how long the upload took.

It has been tested using a Pixhawk 2.1 flight controller running PX4 and on
an AutoQuad flight controller. Please remember to set the correct
target_system value below.

Revision
2018-06-13 FMA First published version
2018-03-14 FMA Cleaned scripts and made them better as examples
2023-06-20 EW Ported to ROS2
'''
# parameters
mavlink_lora_pub_topic = '/mavlink_interface/mission/uploader/mission_item'
mavlink_lora_req = '/mavlink_interface/mission/uploader/mission_count'

mavlink_lora_sub_topic = '/mavlink_interface/mission/ack'
mavlink_lora_sub_req = '/mavlink_interface/mission/uploader/request'


# imports
import rclpy
from rclpy.node import Node

from mavlink_msgs.msg import MavloraMissionItemInt, MavloraMissionAck
from std_msgs.msg import UInt16

from asyncio import Future


# variables
target_sys = 0  # reset by first message
target_comp = 0
home_lat = 55.472078
home_lon = 10.414736
time_start = 0
time_end = 0
alt = 40.


'''NOTE Define the mission here'''
# make 4-5 waypoints and save in list.
missionList = []

# To see what the different settings mean see: https://mavlink.io/en/messages/common.html#MAV_CMD_NAV_WAYPOINT
# scroll down and find the ids for each waypoint and you can see what it means and what it can do

# CHANGE SPEED
speed = MavloraMissionItemInt()
speed.target_system = 0
speed.target_component = 0
speed.seq = 0  # Sequence in the list. Starts from 0 and every item increments by one
speed.frame = 2  # mission command frame
speed.command = 178  # change speed id
speed.param1 = 1.  # air_speed
speed.param2 = 10.  # m/s
speed.param3 = -1.  # no change
speed.param4 = 0.  # absolute or relative. relative = 1
speed.autocontinue = 1  # automatic continue to next waypoint when this is reached

takeoff = MavloraMissionItemInt()
takeoff.target_system = 0
takeoff.target_component = 0
takeoff.seq = 1
takeoff.frame = 6
takeoff.command = 22
takeoff.param1 = 15.
takeoff.param2 = 0.
takeoff.param3 = 0.
takeoff.param4 = 90.
takeoff.x = int(55.4719762 * 10000000)
takeoff.y = int(10.3248095 * 10000000)
takeoff.z = alt
takeoff.autocontinue = 1

# waypoint
way1 = MavloraMissionItemInt()
way1.target_system = 0
way1.target_component = 0
way1.seq = 2
way1.frame = 6  # global pos, relative alt
way1.command = 16
way1.x = int(55.4736667 * 10000000)
way1.y = int(10.3230822 * 10000000)
way1.z = alt
way1.param1 = 0.
way1.param2 = 5.
way1.param4 = 90.
way1.autocontinue = 1

# WAYPOINT 1
way2 = MavloraMissionItemInt()
way2.target_system = 0
way2.target_component = 0
way2.seq = 3
way2.frame = 6  # global pos, relative alt
way2.command = 16
way2.param1 = 0.  # hold time
way2.param2 = 5.
way2.param4 = 90.
way2.x = int(55.4722863 * 10000000)
way2.y = int(10.3187584 * 10000000)
way2.z = alt
way2.autocontinue = 1

# WAYPOINT 2
way3 = MavloraMissionItemInt()
way3.target_system = 0
way3.target_component = 0
way3.seq = 4
way3.frame = 6  # global pos, relative alt_int
way3.command = 16
way3.param1 = 0.  # hold time
way3.param2 = 5.
way3.param4 = 90.
way3.x = int(55.4716843 * 10000000)
way3.y = int(10.3194022 * 10000000)
way3.z = alt
way3.autocontinue = 1

# WAYPOINT 3
way4 = MavloraMissionItemInt()
way4.target_system = 0
way4.target_component = 0
way4.seq = 5
way4.frame = 6  # global pos, relative alt
way4.command = 16
way4.param1 = 0.  # hold time
way4.param2 = 5.
way4.param4 = 90.
way4.x = int(55.4712464 * 10000000)
way4.y = int(10.3226315 * 10000000)
way4.z = alt
way4.autocontinue = 1

# waypoint
way5 = MavloraMissionItemInt()
way5.target_system = 0
way5.target_component = 0
way5.seq = 6
way5.frame = 6  # global pos, relative alt
way5.command = 16
way5.param1 = 0.  # hold time
way5.param2 = 5.
way5.param4 = 90.
way5.x = int(55.4719093 * 10000000)
way5.y = int(10.3247022 * 10000000)
way5.z = alt
way5.autocontinue = 1

# waypoint
# way6 = MavloraMissionItemInt()
# way6.target_system = 0
# way6.target_component = 0
# way6.seq = 7
# way6.frame = 6.  # global pos, relative alt
# way6.command = 16.
# way6.param1 = 0.  # hold time
# way6.x = int(55.4719299 * 10000000)
# way6.y = int(10.3249537 * 10000000)
# way6.z = 100.
# way6.autocontinue = 1

# WAYPOINT 4 LANDING
land = MavloraMissionItemInt()
land.target_system = 0
land.target_component = 0
land.seq = 7
land.frame = 6  # global pos, relative alt
land.command = 21
land.param1 = 0.  # abort alt
land.param2 = 0.
land.param3 = 0.
land.x = int(55.4719093 * 10000000)
land.y = int(10.3247022 * 10000000)
land.z = 0.
land.autocontinue = 1

# add waypoints to list
missionList.append(speed)
missionList.append(takeoff)
missionList.append(way1)
missionList.append(way2)
missionList.append(way3)
missionList.append(way4)
missionList.append(way5)
#missionList.append(way6)
missionList.append(land)


class UploadMission(Node):
    def __init__(self):
        global time_start
        super().__init__('mission_upload')
        # create future object
        self.future = Future()
        # create publisher
        self.mavlink_msg_pub = self.create_publisher(MavloraMissionItemInt, mavlink_lora_pub_topic, 10)
        self.start_req_pub = self.create_publisher(UInt16, mavlink_lora_req, 10)

        # create subscriber
        self.mavlink_mission_ack_sub = self.create_subscription(MavloraMissionAck, mavlink_lora_sub_topic,self.on_ack_received_callback, 10)
        self.mavlink_lora_sub_req = self.create_subscription(UInt16, mavlink_lora_sub_req, self.on_req_received_callback, 10)

        

        time_start = self.get_clock().now().nanoseconds/10e9

    def request_mission_upload(self):
        # initiate the mission upload
        global missionList
        lenMission = UInt16()
        lenMission.data = len(missionList)

        self.start_req_pub.publish(lenMission)

    def on_req_received_callback(self,msg):
        # initiate the mission upload
        self.publish_mission(int(msg.data))


    def on_ack_received_callback(self,msg):
        global time_start, time_end
        # Calculate the time used
        time_end = self.get_clock().now().nanoseconds/10e9
        diff_time = time_end - time_start
        print("Time taken for transmission:" + str(diff_time))
        self.get_logger().info('Received Ack: %s' % msg)

        self.future.set_result(True)

    def publish_mission(self,listIndex):
        global missionList
        self.get_logger().info('Publishing: "%s"' % listIndex)
        item = MavloraMissionItemInt()
        item = missionList[listIndex]
        self.mavlink_msg_pub.publish(item)


def main():
    global missionList
    #init node
    rclpy.init()
    upload_mission = UploadMission()
    rclpy.spin_once(upload_mission,timeout_sec=1)
    upload_mission.request_mission_upload()
    rclpy.spin_until_future_complete(upload_mission, upload_mission.future,timeout_sec=100)
    print("Exiting - Mission uploaded or Timed out")
    upload_mission.destroy_node()
    rclpy.shutdown()

if __name__ == '__main__':
    main()



