#!/usr/bin/env python
# /***************************************************************************
# MavLink LoRa node (ROS) upload mission example script
# Copyright (c) 2018, Kjeld Jensen <kjen@mmmi.sdu.dk> <kj@kjen.dk>
# SDU UAS Center, http://sdu.dk/uas 
# All rights reserved.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met:
#    * Redistributions of source code must retain the above copyright
#      notice, this list of conditions and the following disclaimer.
#    * Redistributions in binary form must reproduce the above copyright
#      notice, this list of conditions and the following disclaimer in the
#      documentation and/or other materials provided with the distribution.
#    * Neither the name of the copyright holder nor the names of its
#      contributors may be used to endorse or promote products derived from
#      this software without specific prior written permission.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
# ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
# WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
# DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
# DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
# (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
# LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
# ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
# (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
# SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
# ****************************************************************************
'''
This example script shows how to upload a mission with more than 20 items to a drone.
This is mainly used to test upload time of large missions while flying.

It has been tested using a Pixhawk 2.1 flight controller running PX4 and on
an AutoQuad flight controller. Please remember to set the correct
target_system value below.

Revision
2018-06-13 FMA First published version
2018-03-14 FMA Cleaned scripts and made them better as examples
2023-06-08 EW Updated to ROS2
'''
# parameters
mavlink_lora_pub_topic = '/mavlink_interface/mission/uploader/mission_item'
mavlink_lora_req = '/mavlink_interface/mission/uploader/mission_count'

mavlink_lora_sub_topic = '/mavlink_interface/mission/ack'
mavlink_lora_sub_req = '/mavlink_interface/mission/uploader/request'


# imports
import rclpy
from rclpy.node import Node

from mavlink_msgs.msg import MavloraMissionItemInt, MavloraMissionAck
from std_msgs.msg import UInt16

from asyncio import Future


# variables
target_sys = 0  # reset by first message
target_comp = 0
home_lat = 55.472078
home_lon = 10.414736
time_start = 0
time_end = 0


'''NOTE Define the mission here'''

# To see what the different settings mean see: https://mavlink.io/en/messages/common.html#MAV_CMD_NAV_WAYPOINT
# scroll down and find the ids for each waypoint and you can see what it means and what it can do

# make 20+ waypoints and save in list.
missionList = []

# CHANGE SPEED
speed = MavloraMissionItemInt()
speed.target_system = 0
speed.target_component = 0
speed.seq = 0
speed.frame = 2  # mission command frame
speed.command = 178
speed.param1 = 5.  # air_speed
speed.param2 = 5.  # m/s
speed.param3 = -1.  # no change
speed.param4 = 0.  # abosulte or relative. relative = 1
speed.autocontinue = 1

speed2 = MavloraMissionItemInt()
speed2.target_system = 0
speed2.target_component = 0
speed2.seq = 1
speed2.frame = 2  # mission command frame
speed2.command = 178
speed2.param1 = 1.  # air_speed
speed2.param2 = 5.  # m/s
speed2.param3 = -1.  # no change
speed2.param4 = 0.  # abosulte or relative. relative = 1
speed2.autocontinue = 1

# TAKEOFF waypoint
way1 = MavloraMissionItemInt()
way1.target_system = 0
way1.target_component = 0
way1.seq = 2
way1.frame = 6  # global pos, relative alt_int
way1.command = 22
way1.x = int(home_lat * 10000000)
way1.y = int(home_lon * 10000000)
way1.z = 20.
way1.param1 = 5.
way1.current = 1
way1.autocontinue = 1

# WAYPOINT 1
way2 = MavloraMissionItemInt()
way2.target_system = 0
way2.target_component = 0
way2.seq = 3
way2.frame = 6  # global pos, relative alt_int
way2.command = 16
way2.param1 = 0.  # hold time
way2.param2 = 5.  # acceptance radius in m
way2.param3 = 0.  # pass though waypoint, no trajectory control
way2.x = int(55.4720010 * 10000000)
way2.y = int(10.4164463 * 10000000)
way2.z = 20.
way2.autocontinue = 1

# WAYPOINT 2
way3 = MavloraMissionItemInt()
way3.target_system = 0
way3.target_component = 0
way3.seq = 4
way3.frame = 6  # global pos, relative alt_int
way3.command = 16
way3.param1 = 0.  # hold time
way3.param2 = 5.  # acceptance radius in m
way3.param3 = 0.  # pass though waypoint, no trajectory control
way3.x = int(55.4720615 * 10000000)
way3.y = int(10.4161885 * 10000000)
way3.z = 40.
way3.autocontinue = 1

# WAYPOINT 3
way4 = MavloraMissionItemInt()
way4.target_system = 0
way4.target_component = 0
way4.seq = 5
way4.frame = 6  # global pos, relative alt_int
way4.command = 16
way4.param1 = 0.  # hold time
way4.param2 = 5.  # acceptance radius in m
way4.param3 = 0.  # pass though waypoint, no trajectory control
way4.x = int(55.4721370 * 10000000)
way4.y = int(10.4164410 * 10000000)
way4.z = 30.
way4.autocontinue = 1

# WAYPOINT 4
way5 = MavloraMissionItemInt()
way5.target_system = 0
way5.target_component = 0
way5.seq = 6
way5.frame = 6  # global pos, relative alt_int
way5.command = 16
way5.param1 = 0. # hold time
way5.param1 = 5. # acceptance radius in m
way5.param3 = 0. # pass though waypoint, no trajectory control
way5.x = int(55.4720680 * 10000000)
way5.y = int(10.4161800 * 10000000)
way5.z = 20.
way5.autocontinue = 1

way6 = MavloraMissionItemInt()
way6.target_system = 0
way6.target_component = 0
way6.seq = 7
way6.frame = 6  # global pos, relative alt_int
way6.command = 16
way6.param1 = 0. # hold time
way6.param1 = 5. # acceptance radius in m
way6.param3 = 0. # pass though waypoint, no trajectory control
way6.x = int(55.4720650 * 10000000)
way6.y = int(10.4161785 * 10000000)
way6.z = 20.
way6.autocontinue = 1

way7 = MavloraMissionItemInt()
way7.target_system = 0
way7.target_component = 0
way7.seq = 8
way7.frame = 6  # global pos, relative alt_int
way7.command = 16
way7.param1 = 0. # hold time
way7.param1 = 5. # acceptance radius in m
way7.param3 = 0. # pass though waypoint, no trajectory control
way7.x = int(55.4720600 * 10000000)
way7.y = int(10.4161862 * 10000000)
way7.z = 20.
way7.autocontinue = 1

way8 = MavloraMissionItemInt()
way8.target_system = 0
way8.target_component = 0
way8.seq = 9
way8.frame = 6  # global pos, relative alt_int
way8.command = 16
way8.param1 = 0. # hold time
way8.param1 = 5. # acceptance radius in m
way8.param3 = 0. # pass though waypoint, no trajectory control
way8.x = int(55.4720667 * 10000000)
way8.y = int(10.4161899 * 10000000)
way8.z = 20.
way8.autocontinue = 1

way9 = MavloraMissionItemInt()
way9.target_system = 0
way9.target_component = 0
way9.seq = 10
way9.frame = 6  # global pos, relative alt_int
way9.command = 16
way9.param1 = 0. # hold time
way9.param1 = 5. # acceptance radius in m
way9.param3 = 0. # pass though waypoint, no trajectory control
way9.x = int(55.4720636 * 10000000)
way9.y = int(10.4161800 * 10000000)
way9.z = 20.
way9.autocontinue = 1

way10 = MavloraMissionItemInt()
way10.target_system = 0
way10.target_component = 0
way10.seq = 11
way10.frame = 6  # global pos, relative alt_int
way10.command = 16
way10.param1 = 0. # hold time
way10.param1 = 5. # acceptance radius in m
way10.param3 = 0. # pass though waypoint, no trajectory control
way10.x = int(55.4720699 * 10000000)
way10.y = int(10.4161885 * 10000000)
way10.z = 20.
way10.autocontinue = 1

way11 = MavloraMissionItemInt()
way11.target_system = 0
way11.target_component = 0
way11.seq = 12
way11.frame = 6  # global pos, relative alt_int
way11.command = 16
way11.param1 = 0. # hold time
way11.param1 = 5. # acceptance radius in m
way11.param3 = 0. # pass though waypoint, no trajectory control
way11.x = int(55.4720615 * 10000000)
way11.y = int(10.4161890 * 10000000)
way11.z = 20.
way11.autocontinue = 1

way12 = MavloraMissionItemInt()
way12.target_system = 0
way12.target_component = 0
way12.seq = 13
way12.frame = 6  # global pos, relative alt_int
way12.command = 16
way12.param1 = 0. # hold time
way12.param1 = 5. # acceptance radius in m
way12.param3 = 0. # pass though waypoint, no trajectory control
way12.x = int(55.4720680 * 10000000)
way12.y = int(10.4161822 * 10000000)
way12.z = 20.
way12.autocontinue = 1

way13 = MavloraMissionItemInt()
way13.target_system = 0
way13.target_component = 0
way13.seq = 14
way13.frame = 6  # global pos, relative alt_int
way13.command = 16
way13.param1 = 0. # hold time
way13.param1 = 5. # acceptance radius in m
way13.param3 = 0. # pass though waypoint, no trajectory control
way13.x = int(55.4720622 * 10000000)
way13.y = int(10.4161885 * 10000000)
way13.z = 20.
way13.autocontinue = 1

way14 = MavloraMissionItemInt()
way14.target_system = 0
way14.target_component = 0
way14.seq = 15
way14.frame = 6  # global pos, relative alt_int
way14.command = 16
way14.param1 = 0. # hold time
way14.param1 = 5. # acceptance radius in m
way14.param3 = 0. # pass though waypoint, no trajectory control
way14.x = int(55.4720695 * 10000000)
way14.y = int(10.4161800 * 10000000)
way14.z = 20.
way14.autocontinue = 1

way15 = MavloraMissionItemInt()
way15.target_system = 0
way15.target_component = 0
way15.seq = 16
way15.frame = 6  # global pos, relative alt_int
way15.command = 16
way15.param1 = 0. # hold time
way15.param1 = 5. # acceptance radius in m
way15.param3 = 0. # pass though waypoint, no trajectory control
way15.x = int(55.4720620 * 10000000)
way15.y = int(10.4161876 * 10000000)
way15.z = 20.
way15.autocontinue = 1

way16 = MavloraMissionItemInt()
way16.target_system = 0
way16.target_component = 0
way16.seq = 17
way16.frame = 6  # global pos, relative alt_int
way16.command = 16
way16.param1 = 0. # hold time
way16.param1 = 5. # acceptance radius in m
way16.param3 = 0. # pass though waypoint, no trajectory control
way16.x = int(55.4720600 * 10000000)
way16.y = int(10.4161885 * 10000000)
way16.z = 20.
way16.autocontinue = 1

way17 = MavloraMissionItemInt()
way17.target_system = 0
way17.target_component = 0
way17.seq = 18
way17.frame = 6  # global pos, relative alt_int
way17.command = 16
way17.param1 = 0. # hold time
way17.param1 = 5. # acceptance radius in m
way17.param3 = 0. # pass though waypoint, no trajectory control
way17.x = int(55.4720699 * 10000000)
way17.y = int(10.4161823 * 10000000)
way17.z = 20.
way17.autocontinue = 1

# WAYPOINT 4 LANDING
land = MavloraMissionItemInt()
land.target_system = 0
land.target_component = 0
land.seq = 19
land.frame = 6  # global pos, relative alt_int
land.command = 21
land.param1 = 5.  # abort alt
land.param2 = 0.  # precision landing. 0 = normal landing
land.x = int(home_lat * 10000000)
land.y = int(home_lon * 10000000)
land.z = 20.
land.autocontinue = 1

# add waypoints to list
missionList.append(speed)
missionList.append(speed2)
missionList.append(way1)
missionList.append(way2)
missionList.append(way3)
missionList.append(way4)
missionList.append(way5)
missionList.append(way6)
missionList.append(way7)
missionList.append(way8)
missionList.append(way9)
missionList.append(way10)
missionList.append(way11)
missionList.append(way12)
missionList.append(way13)
missionList.append(way14)
missionList.append(way15)
missionList.append(way16)
missionList.append(way17)
missionList.append(land)


class UploadMission(Node):
    def __init__(self):
        global time_start
        super().__init__('mission_upload')
        # create future object
        self.future = Future()
        # create publisher
        self.mavlink_msg_pub = self.create_publisher(MavloraMissionItemInt, mavlink_lora_pub_topic, 10)
        self.start_req_pub = self.create_publisher(UInt16, mavlink_lora_req, 10)

        # create subscriber
        self.mavlink_mission_ack_sub = self.create_subscription(MavloraMissionAck, mavlink_lora_sub_topic,self.on_ack_received_callback, 10)
        self.mavlink_lora_sub_req = self.create_subscription(UInt16, mavlink_lora_sub_req, self.on_req_received_callback, 10)

        

        time_start = self.get_clock().now().nanoseconds/10e9

    def request_mission_upload(self):
        # initiate the mission upload
        global missionList
        lenMission = UInt16()
        lenMission.data = len(missionList)

        self.start_req_pub.publish(lenMission)

    def on_req_received_callback(self,msg):
        # initiate the mission upload
        self.publish_mission(int(msg.data))


    def on_ack_received_callback(self,msg):
        global time_start, time_end
        # Calculate the time used
        time_end = self.get_clock().now().nanoseconds/10e9
        diff_time = time_end - time_start
        print("Time taken for transmission:" + str(diff_time))
        self.get_logger().info('Received Ack: %s' % msg)

        self.future.set_result(True)

    def publish_mission(self,listIndex):
        global missionList
        self.get_logger().info('Publishing: "%s"' % listIndex)
        item = MavloraMissionItemInt()
        item = missionList[listIndex]
        self.mavlink_msg_pub.publish(item)


def main():
    global missionList
    #init node
    rclpy.init()
    upload_mission = UploadMission()
    rclpy.spin_once(upload_mission,timeout_sec=1)
    upload_mission.request_mission_upload()
    rclpy.spin_until_future_complete(upload_mission, upload_mission.future,timeout_sec=100)
    print("Exiting - Mission uploaded or Timed out")
    upload_mission.destroy_node()
    rclpy.shutdown()

if __name__ == '__main__':
    main()



