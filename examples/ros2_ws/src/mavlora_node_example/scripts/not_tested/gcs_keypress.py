#!/usr/bin/env python
#/***************************************************************************
# MavLink LoRa node (ROS) example script
# Copyright (c) 2018, Kjeld Jensen <kjen@mmmi.sdu.dk> <kj@kjen.dk>
# SDU UAS Center, http://sdu.dk/uas 
# All rights reserved.
#
# This software is based on Python keypress software written by 
# Steven D'Aprano released under the MIT license.
# https://opensource.org/licenses/MIT
# http://code.activestate.com/recipes/577977-get-single-keypress/
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met:
#    * Redistributions of source code must retain the above copyright
#      notice, this list of conditions and the following disclaimer.
#    * Redistributions in binary form must reproduce the above copyright
#      notice, this list of conditions and the following disclaimer in the
#      documentation and/or other materials provided with the distribution.
#    * Neither the name of the copyright holder nor the names of its
#      contributors may be used to endorse or promote products derived from
#      this software without specific prior written permission.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
# ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
# WARRANTIESs OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
# DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
# DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
# (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
# LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
# ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
# (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
# SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
#****************************************************************************
'''
Revision
2023-04-25 EW Updated tos ROS2
'''
# parameters
mavlink_lora_keypress_pub_topic = '/keypress'
update_interval = 10

# imports
import rclpy
from rclpy.node import Node
from std_msgs.msg import Int8
import sys
import termios
import tty
import threading

class KeyPressNode(Node):
	def __init__(self):
		super().__init__('mavlink_lora_gcs_keypress')

		# setup publisher
		self.key_press_pub = self.create_publisher(Int8, mavlink_lora_keypress_pub_topic, 0)


	def getch(self):
		"""
		getch() -> key character

		Read a single keypress from stdin and return the resulting character. 
		Nothing is echoed to the console. This call will block if a keypress 
		is not already available, but will not wait for Enter to be pressed. 

		If the pressed key was a modifier key, nothing will be detected; if
		it were a special function key, it may return the first character of
		of an escape sequence, leaving additional characters in the buffer.
		"""
		fd = sys.stdin.fileno()
		old_settings = termios.tcgetattr(fd)
		try:
			tty.setraw(fd)
			ch = sys.stdin.read(1)
		finally:
			termios.tcsetattr(fd, termios.TCSADRAIN, old_settings)
		return ch




def main():
	rclpy.init()
	mavlink_lora_gcs_keypress = KeyPressNode()
	thread = threading.Thread(target=rclpy.spin, args=(mavlink_lora_gcs_keypress,), daemon=True)
	thread.start()

	rate = mavlink_lora_gcs_keypress.create_rate(update_interval)
	try:
		while rclpy.ok():
			msg = Int8()
			ch = mavlink_lora_gcs_keypress.getch()
			msg.data = ord(ch)
			mavlink_lora_gcs_keypress.key_press_pub.publish(msg)
			
			if ch == 'q':
				print("q pressed - exiting")
				break
			if KeyboardInterrupt:
				print("KeyboardInterrupt - exiting")
				break

			rate.sleep()
	except KeyboardInterrupt:
		pass
	
	mavlink_lora_gcs_keypress.destroy_node()
	rclpy.shutdown()
	thread.join()



if __name__ == '__main__':
	main()
