#!/usr/bin/env python
#/***************************************************************************
# MavLink LoRa node (ROS) param list example script
# Copyright (c) 2018, Kjeld Jensen <kjen@mmmi.sdu.dk> <kj@kjen.dk>
# SDU UAS Center, http://sdu.dk/uas 
# All rights reserved.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met:
#    * Redistributions of source code must retain the above copyright
#      notice, this list of conditions and the following disclaimer.
#    * Redistributions in binary form must reproduce the above copyright
#      notice, this list of conditions and the following disclaimer in the
#      documentation and/or other materials provided with the distribution.
#    * Neither the name of the copyright holder nor the names of its
#      contributors may be used to endorse or promote products derived from
#      this software without specific prior written permission.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
# ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
# WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
# DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
# DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
# (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
# LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
# ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
# (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
# SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
#****************************************************************************
'''
This example script shows how to set mode on a drone.

It has been tested using a Pixhawk 2.1 flight controller running PX4 and on
an AutoQuad flight controller. Please remember to set the correct
target_system value below.

Revision
2018-06-13 FMA First published version
2019-03-14 FMA Redid the script to properly utilize custom modes for px4 as example
2023-05-07 EW Ported to ROS2
'''

# parameters
mavlink_lora_cmd_ack_topic = '/mavlink_interface/command/ack'
mavlink_lora_set_mode_pub_topic = '/mavlink_interface/command/set_mode'

# imports
import rclpy
from rclpy.node import Node
from mavlink_msgs.msg import MavloraCommandSetMode, MavloraCommandAck
from asyncio import Future

# variables
target_sys = 0 # reset by first message
target_comp = 0

class MavlinkLoraSetMode(Node):
    def __init__(self):
        super().__init__('mavlink_lora_set_mode')
        self.never = Future()
        # subscriptions
        self.subscription = self.create_subscription(
            MavloraCommandAck,
            mavlink_lora_cmd_ack_topic,
            self.on_mavlink_msg,
            10)
        self.subscription
        # publishers
        self.mavlink_set_mode_pub = self.create_publisher(MavloraCommandSetMode, mavlink_lora_set_mode_pub_topic, 10)

    def on_mavlink_msg (self,msg):
        #print cmd ack
        print(msg)


    def send_mavlink_set_mode(self,mode, custom_mode, custom_sub_mode):
        # make msg and publish it
        msg = MavloraCommandSetMode()
        msg.mode = mode
        msg.custom_mode = custom_mode
        msg.custom_sub_mode = custom_sub_mode

        self.mavlink_set_mode_pub.publish(msg)



# ##### Custom Modes ######
# base modes: https://mavlink.io/en/messages/common.html#MAV_MODE_FLAG
# PX4 specific modes: https://github.com/PX4/Firmware/blob/master/src/modules/commander/px4_custom_mode.h

# enum PX4_CUSTOM_MAIN_MODE {
# 	PX4_CUSTOM_MAIN_MODE_MANUAL = 1,
# 	PX4_CUSTOM_MAIN_MODE_ALTCTL,
# 	PX4_CUSTOM_MAIN_MODE_POSCTL,
# 	PX4_CUSTOM_MAIN_MODE_AUTO,
# 	PX4_CUSTOM_MAIN_MODE_ACRO,
# 	PX4_CUSTOM_MAIN_MODE_OFFBOARD,
# 	PX4_CUSTOM_MAIN_MODE_STABILIZED,
# 	PX4_CUSTOM_MAIN_MODE_RATTITUDE,
# 	PX4_CUSTOM_MAIN_MODE_SIMPLE /* unused, but reserved for future use */
# };
#
# enum PX4_CUSTOM_SUB_MODE_AUTO {
# 	PX4_CUSTOM_SUB_MODE_AUTO_READY = 1,
# 	PX4_CUSTOM_SUB_MODE_AUTO_TAKEOFF,
# 	PX4_CUSTOM_SUB_MODE_AUTO_LOITER,
# 	PX4_CUSTOM_SUB_MODE_AUTO_MISSION,
# 	PX4_CUSTOM_SUB_MODE_AUTO_RTL,
# 	PX4_CUSTOM_SUB_MODE_AUTO_LAND,
# 	PX4_CUSTOM_SUB_MODE_AUTO_RTGS,
# 	PX4_CUSTOM_SUB_MODE_AUTO_FOLLOW_TARGET,
# 	PX4_CUSTOM_SUB_MODE_AUTO_PRECLAND
# };

# Using custom mode requires to send 1 as basemode, as that means: Enabled_custom_modes
# Some flightmodes could also be done though basemodes, but custom modes means we utilize the exact flightmodes PX4 has

def  main():
    rclpy.init()
    mavlink_lora_set_mode = MavlinkLoraSetMode()
    
    # set mode to Auto Hold (Loiter)
    print("set_mode: Auto Hold (Loiter)")
    mavlink_lora_set_mode.send_mavlink_set_mode(1., 4., 3.)
    rclpy.spin_until_future_complete(mavlink_lora_set_mode, mavlink_lora_set_mode.never, timeout_sec=5)


    # set mode to Stabilized
    print("set_mode: STABILIZED")
    mavlink_lora_set_mode.send_mavlink_set_mode(1., 7., 0.)
    rclpy.spin_until_future_complete(mavlink_lora_set_mode, mavlink_lora_set_mode.never, timeout_sec=5)

    # set mode to Altitude Control
    print("set_mode: Altitude Control")
    mavlink_lora_set_mode.send_mavlink_set_mode(1., 2., 0.)
    rclpy.spin_until_future_complete(mavlink_lora_set_mode, mavlink_lora_set_mode.never, timeout_sec=5)
    
    print("Exiting")
    # destroy node  
    mavlink_lora_set_mode.destroy_node()
    rclpy.shutdown()


if __name__ == '__main__':
    main()