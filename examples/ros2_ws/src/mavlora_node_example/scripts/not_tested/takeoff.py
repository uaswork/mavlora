#!/usr/bin/env python
#/***************************************************************************
# MavLink LoRa node (ROS) param list example script
# Copyright (c) 2018, Kjeld Jensen <kjen@mmmi.sdu.dk> <kj@kjen.dk>
# SDU UAS Center, http://sdu.dk/uas 
# All rights reserved.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met:
#    * Redistributions of source code must retain the above copyright
#      notice, this list of conditions and the following disclaimer.
#    * Redistributions in binary form must reproduce the above copyright
#      notice, this list of conditions and the following disclaimer in the
#      documentation and/or other materials provided with the distribution.
#    * Neither the name of the copyright holder nor the names of its
#      contributors may be used to endorse or promote products derived from
#      this software without specific prior written permission.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
# ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
# WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
# DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
# DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
# (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
# LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
# ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
# (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
# SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
#****************************************************************************
'''
This example script shows how to send takeoff to a drone.

It has been tested using a Pixhawk 2.1 flight controller running PX4 and on
an AutoQuad flight controller. Please remember to set the correct
target_system value below.

Revision
2018-06-13 FMA First published version
2018-03-14 FMA Cleaned scripts and made them better as examples
2023-04-27 EW Ported to ROS2
'''

# parameters
mavlink_lora_cmd_ack_topic = '/mavlink_interface/command/ack'
mavlink_lora_pos_topic = '/mavlink_pos'
mavlink_lora_arm_pub_topic = '/mavlink_interface/command/arm_disarm'
mavlink_lora_takeoff_pub_topic = '/mavlink_interface/command/takeoff'

# imports
import rclpy
from rclpy.node import Node
from mavlink_msgs.msg import MavloraCommandAck, MavloraCommandTakeoff, MavloraPos
import threading


class TakeoffNode(Node):
    def __init__(self):
        super().__init__('takeoff_node')
        # set Qos profile
        self.sensorQoS = rclpy.qos.QoSPresetProfiles.get_from_short_key('sensor_data')
        
        # pubs
        self.mavlink_takeoff_pub = self.create_publisher(MavloraCommandTakeoff, mavlink_lora_takeoff_pub_topic, 10)
        # ack and pos sub
        self.mavlink_ack_sub = self.create_subscription(MavloraCommandAck, mavlink_lora_cmd_ack_topic, self.on_mavlink_msg, 10)
        self.mavlink_pos_sub = self.create_subscription(MavloraPos, mavlink_lora_pos_topic, self.on_gps_global_pos, self.sensorQoS )

        # init variables
        self.target_sys = 0 # reset by first message
        self.target_comp = 0
        self.current_lat = 0 #55.057883
        self.current_lon = 0 #10.569678
        self.current_alt = 0
        self.pos_received = False
        self.land_sent = False


    def on_mavlink_msg(self,msg):
        # print cmd ack
        print(msg)


    def on_gps_global_pos(self,msg):

        if self.pos_received:
            return
        self.current_lat = msg.lat
        self.current_lon = msg.lon
        self.current_alt = msg.alt

        self.pos_received = True

        print("Received global pos")


    def send_mavlink_takeoff(self,alt):

        msg = MavloraCommandTakeoff()
        msg.lat = self.current_lat
        msg.lon = self.current_lon
        msg.alt = float(alt)
        msg.yaw_angle = float('NaN') # unchanged angle
        msg.pitch = 0.

        self.mavlink_takeoff_pub.publish(msg)


def main():
    rclpy.init()
    takeoff_node = TakeoffNode()
    rclpy.spin_once(takeoff_node, timeout_sec=0.1)

    thread = threading.Thread(target=rclpy.spin, args=(takeoff_node,), daemon=True)
    thread.start()
    
    rate = takeoff_node.create_rate(1)
    try:
        while rclpy.ok():     
            if takeoff_node.pos_received and takeoff_node.land_sent == False:
                print("takeoff sent")
                takeoff_node.send_mavlink_takeoff(10)
                takeoff_node.land_sent = True

            rate.sleep()
    except KeyboardInterrupt:
        pass

    takeoff_node.destroy_node()
    rclpy.shutdown()
    thread.join()

if __name__ == '__main__':
    main()


