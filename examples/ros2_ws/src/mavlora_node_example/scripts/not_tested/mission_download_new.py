#!/usr/bin/env python
#/***************************************************************************
# MavLink LoRa node (ROS) mission download script
# Copyright (c) 2018, Kjeld Jensen <kjen@mmmi.sdu.dk> <kj@kjen.dk>
# SDU UAS Center, http://sdu.dk/uas 
# All rights reserved.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met:
#    * Redistributions of source code must retain the above copyright
#      notice, this list of conditions and the following disclaimer.
#    * Redistributions in binary form must reproduce the above copyright
#      notice, this list of conditions and the following disclaimer in the
#      documentation and/or other materials provided with the distribution.
#    * Neither the name of the copyright holder nor the names of its
#      contributors may be used to endorse or promote products derived from
#      this software without specific prior written permission.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
# ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
# WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
# DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
# DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
# (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
# LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
# ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
# (INCLUDING NEGIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
# SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
#****************************************************************************
'''
Revision
2018-06-12 KJ First published version
2023-04-27 EW Ported to ROS2
'''
# parameters
target_sys = 92 # target system (1 = Pixhawk2 PX4, 66 = AutoQuad)
target_comp = 0 # target component

# other defines
ros_node_name = 'mission_download'
ros_node_update_interval = 10
mavlink_lora_rx_sub = '/mavlink_rx'
mavlink_lora_tx_pub = '/mavlink_tx'


mavlink_lora_sub_topic = '/mavlink_interface/mission/uploader/mission_item'
mavlink_lora_req = '/mavlink_interface/mission/uploader/request'

mavlora_sub_length_topic = '/mavlink_interface/mission/uploader/mission_count'
mavlink_lora_ack_topic = '/mavlink_interface/mission/ack'



# imports
import rclpy
from rclpy.node import Node
from mavlink_msgs.msg import MavloraMsg
from mavlink_lora.msg import MavloraMissionRequest
from mavlink_msgs.msg import MavloraMissionList
from mavlink_msgs.msg import MavloraMissionItemInt

from std_msgs.msg import UInt16
from mission_lib import *
import threading


class MissionDownload(Node):
	def __init__(self, target_sys, target_comp):
		super().__init__("mission_download")
		self.sensorQoS = rclpy.qos.QoSPresetProfiles.get_from_short_key('sensor_data')

		# initiate variables
		self.stop = False
		self.first_msg_ok = False
		self.request_sent = False
		self.mission_count = 0
		self.mission_id_next = 0
		self.mi = mission_lib()
		self.sys_id = 0 # reset when receiving first msg
		self.comp_id = 0	

		# Add Subscriber and Publisher
		#self.mavlink_msg_pub = self.create_publisher(MavloraMsg, mavlink_lora_tx_pub, 10)
		#self.mavlink_msg_sub = self.create_subscription(MavloraMsg, mavlink_lora_rx_sub, self.on_mavlink_msg, self.sensorQoS)

		# create publisher
		self.req_list_pub = self.create_publisher(MavloraMissionRequest, mavlink_lora_req, 10)


        # create subscriber
		self.start_req_pub = self.create_subscription(UInt16, mavlora_sub_length_topic,self.on_get_length, 10)
		self.mavlink_get_mission_item_sub = self.create_publisher(MavloraMissionItemInt, mavlink_lora_sub_topic,self.on_item_received, 10)

		self.mavlink_mission_ack_sub = self.create_subscription(MavloraMissionAck, mavlink_lora_ack_topic,self.on_ack_received_callback, 10)
		self.mavlink_lora_sub_req = self.create_subscription(UInt16, mavlink_lora_sub_req, self.on_req_received_callback, 10)

		self.missionlist = MavloraMissionList()
		self.length = 0

	def request_mission(self):
		global target_sys, target_comp
		self.get_logger().info('Requesting Mission')
		msg = MavloraMissionRequest()
		msg.target_system = target_sys
		msg.target_component = target_comp
		msg.seq = 0
		msg.mission_type = 0

		self.req_list_pub.publish(msg)
		self.request_sent = True

	def on_get_length(self, msg):
		self.get_logger().info('Dowloading Mission with length: "%d"' % msg.data)
		self.length = msg.data

		for i in range(self.length):
			self.send_mavlink_mission_req(i)
			print("Requesting mission item %d" % i)

	def on_item_received(self, msg):
		self.get_logger().info('Received Mission Item: "%s"' % msg)
		self.missionlist.mission_items.append(msg)





	def on_mavlink_msg (self, msg):
		if self.first_msg_ok == False:
			self.first_msg_ok = True
			self.sys_id = msg.sys_id
			self.mi.set_target(self.sys_id, self.comp_id)

		if msg.msg_id == MAVLINK_MSG_ID_MISSION_ITEM:
			print(self.mi.unpack_mission_item(msg.payload))
			if self.mission_id_next < self.mission_count:
				self.mission_id_next += 1
				self.send_mavlink_mission_req(self.mission_id_next)
			else:
				self.stop = True
		elif msg.msg_id == MAVLINK_MSG_ID_MISSION_COUNT:
			self.mission_count = self.mi.unpack_mission_count(msg.payload)
			print("Mission count: %d" % self.mission_count)
			if self.mission_count > 0:
				self.mission_id_next = 1
				self.send_mavlink_mission_req(self.mission_id_next)
			else:
				self.stop = True

	def send_mavlink_mission_req_list(self):
		self.mi.msg.header.stamp = self.get_clock().now().to_msg()
		self.mi.pack_mission_req_list()
		self.mavlink_msg_pub.publish(self.mi.msg)


		


def main(args=None):
	rclpy.init(args=args)
	mission_download = MissionDownload(target_sys, target_comp)

	# Spin in a separate thread
	thread = threading.Thread(target=rclpy.spin, args=(mission_download, ), daemon=True)
	thread.start()

	rate = mission_download.create_rate(ros_node_update_interval)

	try:
		while(rclpy.ok() and not mission_download.stop):
			if mission_download.request_sent == False and mission_download.first_msg_ok == True:
				print("Requesting mission list")
				mission_download.send_mavlink_mission_req_list()
				mission_download.request_sent = True	
			# sleep the defined interval
			rate.sleep()

	except KeyboardInterrupt:
		pass

	mission_download.destroy_node()
	rclpy.shutdown()
	thread.join()


if __name__ == "__main__":
	main()


