#!/usr/bin/env python
#/***************************************************************************
# MavLink LoRa node (ROS) param list example script
# Copyright (c) 2018, Kjeld Jensen <kjen@mmmi.sdu.dk> <kj@kjen.dk>
# SDU UAS Center, http://sdu.dk/uas 
# All rights reserved.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met:
#    * Redistributions of source code must retain the above copyright
#      notice, this list of conditions and the following disclaimer.
#    * Redistributions in binary form must reproduce the above copyright
#      notice, this list of conditions and the following disclaimer in the
#      documentation and/or other materials provided with the distribution.
#    * Neither the name of the copyright holder nor the names of its
#      contributors may be used to endorse or promote products derived from
#      this software without specific prior written permission.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
# ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
# WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
# DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
# DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
# (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
# LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
# ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
# (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
# SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
#****************************************************************************
'''
This example script shows how to precision land where the drone currently is.

It has been tested using a Pixhawk 2.1 flight controller running PX4 and on
an AutoQuad flight controller. Please remember to set the correct
target_system value below.

Revision
2018-06-13 FMA First published version
2018-03-14 FMA Cleaned scripts and made them better as examples
2023-04-26 EW Ported to ROS2
'''

# parameters
mavlink_lora_cmd_ack_topic = '/mavlink_interface/command/ack'
mavlink_lora_pos_topic = '/mavlink_pos'
mavlink_lora_land_pub_topic = '/mavlink_interface/command/land'

# imports
import rclpy
from rclpy.node import Node
from mavlink_msgs.msg import MavloraCommandAck, MavloraPos, MavloraCommandLand, MavloraCommandLong
from asyncio import Future

# variables
target_sys = 0  # reset by first message
target_comp = 0
current_lat = 0
current_lon = 0
current_alt = 0
pos_received = False
land_sent = False

class MavlinkLoraLandNode(Node):
    def __init__(self):
        super().__init__('mavlink_lora_land')
        # Set sensor QoS profile
        self.sensorQoS = rclpy.qos.QoSPresetProfiles.get_from_short_key('sensor_data')
        # Add future for when first message is received
        self.future = Future()
        # Add publisher
        self.mavlink_land_pub = self.create_publisher(MavloraCommandLong, mavlink_lora_land_pub_topic, 10)

        # Add subscriber
        self.mavlink_ack_sub = self.create_subscription(MavloraCommandAck, mavlink_lora_cmd_ack_topic, self.on_mavlink_msg, 10)
        self.mavlink_pos_sub = self.create_subscription(MavloraPos, mavlink_lora_pos_topic, self.on_gps_global_pos, self.sensorQoS)

        self.timer = self.create_timer(1, self.on_timer)

    def on_timer(self):
        self.send_mavlink_land()

        
    def on_mavlink_msg (self,msg):
    # print ack msg
        self.get_logger().info('Received ack message: %s' % msg)
        # job is done
        self.future.set_result(True)


    def on_gps_global_pos(self,msg):
        global pos_received, current_lon, current_lat, current_alt

        # only set it once
        if pos_received:
            return

        current_lat = msg.lat
        current_lon = msg.lon
        current_alt = msg.alt

        pos_received = True

        print("Received global pos")
        self.send_mavlink_land()


    
    def send_mavlink_land(self):
        global current_alt, current_lat, current_lon,target_sys, target_comp

        # make msg with curr coordinates to make it land at current position
        msg = MavloraCommandLand()
        msg.lat = current_lat
        msg.lon = current_lon
        msg.altitude = 10.  # make it go to 10m altitude before starting landing. Can be used for precision landing systems
        msg.yaw_angle = float('NaN')  # unchanged angle
        msg.abort_alt = 5.
        msg.precision_land_mode = 0.  # 2=required precision landing, 1= opportunistic precision land, 0=gps landing

        msg = MavloraCommandLong()
        msg.target_system = target_sys
        msg.target_component = target_comp
        msg.command = 21  # MAV_CMD_NAV_LAND
        msg.param1 = 5.  # Abort alt (0 = unused)
        msg.param2 = 0.  # Precision land mode (# 2=required precision landing, 1= opportunistic precision land, 0=gps landing)
        msg.param3 = 0.  # Empty
        msg.param4 = float('NaN')  # Yaw angle (NaN = unchanged)
        msg.param5 = current_lat  # Lat
        msg.param6 = current_lon  # Lon
        msg.param7 = current_alt  # Alt


        # publish msg
        self.mavlink_land_pub.publish(msg)
        print("Landing command sent")


        



def main(args=None):
    rclpy.init(args=args)

    mavlink_lora_land = MavlinkLoraLandNode()
    rclpy.spin_until_future_complete(mavlink_lora_land, mavlink_lora_land.future, timeout_sec=10.0)
    print("Timeout or Ack received")

    mavlink_lora_land.destroy_node()
    rclpy.shutdown()


if __name__ == '__main__':
    main()
