#!/usr/bin/env python
#/***************************************************************************
# MavLink LoRa node (ROS) example script
# Copyright (c) 2018, Kjeld Jensen <kjen@mmmi.sdu.dk> <kj@kjen.dk>
# SDU UAS Center, http://sdu.dk/uas 
# All rights reserved.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met:
#    * Redistributions of source code must retain the above copyright
#      notice, this list of conditions and the following disclaimer.
#    * Redistributions in binary form must reproduce the above copyright
#      notice, this list of conditions and the following disclaimer in the
#      documentation and/or other materials provided with the distribution.
#    * Neither the name of the copyright holder nor the names of its
#      contributors may be used to endorse or promote products derived from
#      this software without specific prior written permission.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
# ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
# WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
# DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
# DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
# (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
# LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
# ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
# (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
# SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
#****************************************************************************
'''
This example script shows how to obtain RSSI of the SIK radios.

NOT YET: It has been tested using a Pixhawk 2.1 flight controller running PX4 and on
an AutoQuad flight controller. Please remember to set the correct
target_system value below.

Revision
2019-03-12 FMA First version
2018-03-14 FMA Cleaned scripts and made them better as examples
2023-04-25 EW Ported to ROS2
'''
# imports
import rclpy
from rclpy.node import Node
from mavlink_msgs.msg import MavloraRadioStatus

# parameters
mavlink_lora_radio_status_topic = '/mavlink_radio_status'

'''
NOTE: This only works if a heartbeat is sent. Otherwise the SIK radios isn't injecting radio_status packages.
Either enable so mavlink_lora sends heartbeats with "heartbeats:=true", or have another node that publish 
heartbeats to /mavlink_heartbeat_tx
'''

class RSSINode(Node):
	def __init__(self):
		super().__init__('mavlink_lora_radio_status')
		self.sensorQoS = rclpy.qos.QoSPresetProfiles.get_from_short_key('sensor_data')
		# create a subscriber for the mavlink_lora_radio_status topic
		self.subscription = self.create_subscription(MavloraRadioStatus, mavlink_lora_radio_status_topic, self.on_mavlink_msg, self.sensorQoS)

		# wait until everything is running
		#rclpy.sleep(1)
	
	def on_mavlink_msg(self, msg):
		# print rssi and remote rssi
		print("RSSI: {0} dbm, Remote RSSI: {1} dbm".format(msg.rssi, msg.remrssi))

def main(args=None):
	rclpy.init(args=args)
	mavlink_lora_radio_status = RSSINode()
	rclpy.spin(mavlink_lora_radio_status)
	mavlink_lora_radio_status.destroy_node()
	rclpy.shutdown()


if __name__ == '__main__':
	main()
	

