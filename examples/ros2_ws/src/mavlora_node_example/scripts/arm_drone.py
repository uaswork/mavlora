#!/usr/bin/env python
#/***************************************************************************
# MavLink LoRa node (ROS) param list example script
# Copyright (c) 2018, Kjeld Jensen <kjen@mmmi.sdu.dk> <kj@kjen.dk>
# SDU UAS Center, http://sdu.dk/uas 
# All rights reserved.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met:
#    * Redistributions of source code must retain the above copyright
#      notice, this list of conditions and the following disclaimer.
#    * Redistributions in binary form must reproduce the above copyright
#      notice, this list of conditions and the following disclaimer in the
#      documentation and/or other materials provided with the distribution.
#    * Neither the name of the copyright holder nor the names of its
#      contributors may be used to endorse or promote products derived from
#      this software without specific prior written permission.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
# ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
# WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
# DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
# DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
# (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
# LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
# ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
# (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
# SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
#****************************************************************************
'''
This example script shows how to arm a drone.

It has been tested using a Pixhawk 2.1 flight controller running PX4 and on
an AutoQuad flight controller. Please remember to set the correct
target_system value below.

Revision
2018-06-13 FMA First published version
2018-03-14 FMA Cleaned scripts and made them better as examples
2023-04-25 EW Updated to ROS2
'''

# imports
import rclpy
from rclpy.node import Node
from mavlink_msgs.msg import MavloraCommandAck, MavloraCommandStartMission
from std_msgs.msg import Bool
from _asyncio import Future

# parameters
mavlink_lora_cmd_ack_topic = '/mavlink_interface/command/ack'
mavlink_lora_arm_pub_topic = '/mavlink_interface/command/arm_disarm'
mavlink_lora_start_mission_pub_topic = '/mavlink_interface/command/start_mission'

# global variables - not used
target_sys = 0 # reset by first message
target_comp = 0
home_lat = 55.057883
home_lon = 10.569678


class ArmDrone(Node):

    def __init__(self):
        super().__init__('arm_drone')
        self.future = Future()
        # create subscription to mavlink_lora_command_ack topic
        self.mavlink_msg_subscription = self.create_subscription(
            MavloraCommandAck,
            mavlink_lora_cmd_ack_topic,
            self.on_mavlink_msg,
            10)
        self.mavlink_msg_subscription  # prevent unused variable warning

        # create publishers to mavlink_lora_command_arm_disarm and mavlink_lora_command_start_mission topics
        self.mavlink_arm_pub = self.create_publisher(Bool, mavlink_lora_arm_pub_topic, 10)
        self.mavlink_start_mission_pub = self.create_publisher(MavloraCommandStartMission, mavlink_lora_start_mission_pub_topic, 10)




    def send_mavlink_arm(self):
        # make msg and publish it
        msg = Bool()
        msg.data = True
        self.get_logger().info("Sending Arm Command")

        self.mavlink_arm_pub.publish(msg)

    def send_mavlink_start_mission(self):
        # make msg and publish it
        cmd = MavloraCommandStartMission()

        # last item isn't needed as PX4 doesn't use last_item for anything.
        cmd.first_item = 0.0
        cmd.last_item = 0.0
        self.get_logger().info("Sending Start Mission Command")
        self.mavlink_start_mission_pub.publish(cmd)


    def on_mavlink_msg(self, msg):
        # print ack message
        self.get_logger().info('Received message: %s' % msg)
        '''NOTE Make future event dependend on ack message'''
        self.future.set_result(True)



def main(args=None):
    rclpy.init(args=args)
    arm_drone = ArmDrone()
    # Use one of the following to options (comment out the other)

    #arm_drone.send_mavlink_arm()
    arm_drone.send_mavlink_start_mission()

    arm_drone.get_logger().info('Waiting for Callback')
    rclpy.spin_until_future_complete(arm_drone,arm_drone.future,timeout_sec=10.0)
    print("Exiting Node")
    
    arm_drone.destroy_node()
    rclpy.shutdown()


if __name__ == '__main__':
    main()