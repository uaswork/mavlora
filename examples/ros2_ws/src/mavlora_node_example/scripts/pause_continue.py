#!/usr/bin/env python
#/***************************************************************************
# MavLink LoRa node (ROS) param list example script
# Copyright (c) 2018, Kjeld Jensen <kjen@mmmi.sdu.dk> <kj@kjen.dk>
# SDU UAS Center, http://sdu.dk/uas 
# All rights reserved.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met:
#    * Redistributions of source code must retain the above copyright
#      notice, this list of conditions and the following disclaimer.
#    * Redistributions in binary form must reproduce the above copyright
#      notice, this list of conditions and the following disclaimer in the
#      documentation and/or other materials provided with the distribution.
#    * Neither the name of the copyright holder nor the names of its
#      contributors may be used to endorse or promote products derived from
#      this software without specific prior written permission.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
# ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
# WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
# DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
# DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
# (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
# LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
# ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
# (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
# SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
#****************************************************************************
'''
This example script shows how to pause or continue a mission on a drone.

Currently unsupported on PX4

Revision
2018-06-13 FMA First published version
2018-03-14 FMA Cleaned scripts and made them better as examples
2023-05-07 EW Ported to ROS2
'''

# parameters
mavlink_lora_cmd_ack_topic = '/mavlink_interface/command/ack'
mavlink_lora_pause_continue_pub_topic = '/mavlink_interface/command/pause_continue'

# imports
import rclpy
from rclpy.node import Node
from sys import argv
from mavlink_msgs.msg import MavloraCommandAck
from std_msgs.msg import Bool
import threading

# variables
global current_lat, current_lon, current_alt, pos_received, land_sent

class Mavlink_lora_pause_continue(Node):
    def __init__(self):
        super().__init__('mavlink_lora_pause_continue')
        self.mavlink_pause_hold_pub = self.create_publisher(Bool, mavlink_lora_pause_continue_pub_topic, 10)
        self.subscription = self.create_subscription(
            MavloraCommandAck,
            mavlink_lora_cmd_ack_topic,
            self.on_mavlink_msg,
            10)
        self.subscription  # prevent unused variable warning

        self.pause_sent = False


    def on_mavlink_msg (self,msg):
        # print cmd ack
        print(msg)

    def timer_callback(self):
        self.send_mavlink_pause_continue(0)

    def send_mavlink_pause_continue(self,hold):
        # make msg
        msg = Bool()
        if (int(hold) > 0):
            send = True
        else:
            send = False

        msg.data = send

        self.mavlink_pause_hold_pub.publish(msg)




def main():
    # check input argv
    if len(argv) == 1:
        print("Usage: pause_continue.py [continue/pause]")
        print("Sending 0 means pause and hold current position, 1 means continue mission")

    # save what option we took
    sendText = ""
    if int(argv[1]) > 0:
        sendText = "sent mission continue"
    else:
        sendText = "sent mission pause. Holding current position"

    rclpy.init()
    mavlink_lora_pause_continue = Mavlink_lora_pause_continue()
    rclpy.spin_once(mavlink_lora_pause_continue, timeout_sec=1)
    
    thread = threading.Thread(target=rclpy.spin, args=(mavlink_lora_pause_continue,), daemon=True)
    thread.start()

    rate = mavlink_lora_pause_continue.create_rate(5)
    try:
        while(rclpy.ok()):
            if not mavlink_lora_pause_continue.pause_sent:
                print(sendText)
                mavlink_lora_pause_continue.send_mavlink_pause_continue(argv[1])
                mavlink_lora_pause_continue.pause_sent = True
            rate.sleep()
    except KeyboardInterrupt:
        pass

    mavlink_lora_pause_continue.destroy_node()
    rclpy.shutdown()
    thread.join()

    
if __name__ == '__main__':
    main()

