#!/usr/bin/env python
# /***************************************************************************
# MavLink LoRa node (ROS) clear mission example script
# Copyright (c) 2018, Kjeld Jensen <kjen@mmmi.sdu.dk> <kj@kjen.dk>
# SDU UAS Center, http://sdu.dk/uas
# All rights reserved.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met:
#    * Redistributions of source code must retain the above copyright
#      notice, this list of conditions and the following disclaimer.
#    * Redistributions in binary form must reproduce the above copyright
#      notice, this list of conditions and the following disclaimer in the
#      documentation and/or other materials provided with the distribution.
#    * Neither the name of the copyright holder nor the names of its
#      contributors may be used to endorse or promote products derived from
#      this software without specific prior written permission.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
# ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
# WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
# DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
# DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
# (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
# LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
# ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
# (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
# SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
# ****************************************************************************
'''
This example script shows how to clear a mission on a drone.

It has been tested using a Pixhawk 2.1 flight controller running PX4 and on
an AutoQuad flight controller. Please remember to set the correct
target_system value below.

Revision
2018-06-13 FMA First published version
2018-03-14 FMA Cleaned scripts and made them better as examples
2023-04-27 EW Ported to ROS2
'''
# parameters
mavlink_lora_mission_start_topic = '/mavlink_interface/command/start_mission'

# imports
import rclpy
from rclpy.node import Node
from mavlink_msgs.msg import MavloraCommandStartMission, MavloraMissionAck
from asyncio import Future

# variables - not used
target_sys = 0  # reset by first message
target_comp = 0

''' Find Appropriate QoS Profile 
Message is first received for second transmission'''
class MissionStart(Node):

    def __init__(self):
        super().__init__('mavlink_lora_mission_start')
        self.future = Future()

        self.stdQoS = rclpy.qos.QoSProfile(
        depth=1,
        reliability=rclpy.qos.QoSReliabilityPolicy.RMW_QOS_POLICY_RELIABILITY_RELIABLE,
        durability=rclpy.qos.QoSDurabilityPolicy.VOLATILE)


        # pubs
        self.mavlink_msg_pub = self.create_publisher(MavloraCommandStartMission, mavlink_lora_mission_start_topic, 10)

        # subs
        self.mavlink_mission_ack_sub = self.create_subscription(MavloraMissionAck, 'mavlink_interface/mission/ack', self.on_ack_received_callback, 0)

        self.timer = self.create_timer(10, self.timer_callback)


    def timer_callback(self):
        print('No acknowledgement received, resending command until timeout')
        self.send_command()


    def on_ack_received_callback(self,msg):
        # result
        print('Mission start ack received')
        print(msg.result_text)
        self.future.set_result(True)


    def send_command(self):
        print('Sending command')
        msg = MavloraCommandStartMission()
        msg.first_item = 0.

        self.mavlink_msg_pub.publish(msg)



def main(args=None):
    rclpy.init(args=args)
    mavlink_lora_mission_start = MissionStart()

    mavlink_lora_mission_start.send_command()
    rclpy.spin_until_future_complete(mavlink_lora_mission_start, mavlink_lora_mission_start.future,timeout_sec=100)

    mavlink_lora_mission_start.destroy_node()
    rclpy.shutdown()

if __name__ == '__main__':
    main()