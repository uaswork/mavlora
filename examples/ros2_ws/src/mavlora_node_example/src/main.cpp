/*
 * Copyright (c) 2022, Frederik Mazur Andersen <fm@mmmi.sdu.dk>/<fm@fmazur.dk>
 *
 * SDU UAS Center, http://sdu.dk/uas, University of Southern Denmark
 * All rights reserved.
 *
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE.txt' at the root of the repo.
 */

#include <cstdio>
#include "rclcpp/rclcpp.hpp"
#include "mavlora_node.hpp"


int main(int argc, char ** argv)
{
    // Launch ROS 2
    rclcpp::init(argc, argv);

    // make a node and spin
    rclcpp::spin(std::make_shared<mavlora_node>());

    // Shutdown routine for ROS2
    rclcpp::shutdown();
    return 0;
}



