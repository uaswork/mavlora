/*
 * Copyright (c) 2022, Frederik Mazur Andersen <fm@mmmi.sdu.dk>/<fm@fmazur.dk>
 *
 * SDU UAS Center, http://sdu.dk/uas, University of Southern Denmark
 * All rights reserved.
 *
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE.txt' at the root of the repo.
 */

#include "mavlora_node.hpp"

mavlora_node::mavlora_node() : Node("mavlora_node")
{
  // init Lora lib
  RCLCPP_INFO(this->get_logger(), "%s Initiating MavLora Node with system_id: %d", _logging_name.c_str(), my_system_id);
  loraLib.init("FC", my_system_id, bind(&mavlora_node::parse_msg_from_fc, this, placeholders::_1));

  // make timers and stop them to avoid firing first time. But are ready for use
  mission_command_timeout = this->create_wall_timer(DEFAULT_TIMEOUT_TIME, std::bind(&mavlora_node::mission_clear_all_timeout_callback, this));
  command_long_timeout = this->create_wall_timer(DEFAULT_TIMEOUT_TIME, std::bind(&mavlora_node::command_long_timeout_callback, this));
  command_int_timeout = this->create_wall_timer(DEFAULT_TIMEOUT_TIME, std::bind(&mavlora_node::command_int_timeout_callback, this));

  mission_command_timeout->cancel();
  command_long_timeout->cancel();
  command_int_timeout->cancel();

  // initial timestamps
  status_msg_sent = this->now();
  heartbeat_msg_sent = this->now();
  last_heard_sys = this->now();
  last_heard_serial = this->now();
  last_heard = this->now();

  // QoS profiles
  auto qos_reliable = rclcpp::QoS(
      rclcpp::QoSInitialization(
          // The history policy determines how messages are saved until taken by
          // the reader.
          // KEEP_ALL saves all messages until they are taken.
          // KEEP_LAST enforces a limit on the number of messages that are saved,
          // specified by the "depth" parameter.
          RMW_QOS_POLICY_HISTORY_KEEP_LAST,
          // Depth represents how many messages to store in history when the
          // history policy is KEEP_LAST.
          1
      ));
  qos_reliable.reliable().durability_volatile();

  auto qos_volatile_besteffort = rclcpp::QoS(
      rclcpp::QoSInitialization(
          RMW_QOS_POLICY_HISTORY_KEEP_LAST,
          1
      ));
  qos_volatile_besteffort.durability_volatile().best_effort();


  // initializing, we subscribe to topics
  mission_clear_all_sub = this->create_subscription<std_msgs::msg::Empty>("mavlink_interface/mission/clear_all", qos_volatile_besteffort, std::bind(&mavlora_node::ros_mission_clear_all_callback, this, _1));
  command_arm_disarm_sub = this->create_subscription<std_msgs::msg::Bool>("mavlink_interface/command/arm_disarm", qos_reliable, std::bind(&mavlora_node::ros_command_arm_disarm_callback, this, _1));
  command_start_mission_sub = this->create_subscription<mavlink_msgs::msg::MavloraCommandStartMission>("mavlink_interface/command/start_mission", qos_reliable, std::bind(&mavlora_node::ros_command_start_mission_callback, this, _1));
  command_set_mode_sub = this->create_subscription<mavlink_msgs::msg::MavloraCommandSetMode>("mavlink_interface/command/set_mode", qos_reliable, std::bind(&mavlora_node::ros_command_set_mode_callback, this, _1));
  command_takeoff_sub = this->create_subscription<mavlink_msgs::msg::MavloraCommandTakeoff>("mavlink_interface/command/takeoff", qos_reliable, std::bind(&mavlora_node::ros_command_takeoff_callback, this, _1));
  command_land_sub = this->create_subscription<mavlink_msgs::msg::MavloraCommandLand>("mavlink_interface/command/land", qos_reliable, std::bind(&mavlora_node::ros_command_land_callback, this, _1));
  command_reposition_sub = this->create_subscription<mavlink_msgs::msg::MavloraCommandReposition>("mavlink_interface/command/reposition", qos_reliable, std::bind(&mavlora_node::ros_command_reposition_callback, this, _1));
  command_pause_continue_sub = this->create_subscription<std_msgs::msg::Bool>("mavlink_interface/command/pause_continue", qos_reliable, std::bind(&mavlora_node::ros_command_pause_continue_callback, this, _1));
  command_rtl = this->create_subscription<mavlink_msgs::msg::MavloraCommandLong>("mavlink_interface/command/return_to_home", qos_reliable, std::bind(&mavlora_node::ros_command_rtl, this, _1));
  command_message_interval_sub = this->create_subscription<mavlink_msgs::msg::MavloraCommandMessageInteval>("mavlink_interface/command/message_interval", qos_reliable, std::bind(&mavlora_node::ros_command_message_interval, this, _1));
  heartbeat_sub = this->create_subscription<mavlink_msgs::msg::MavloraHeartbeat>("mavlink_heartbeat_tx", qos_volatile_besteffort, std::bind(&mavlora_node::ros_send_heartbeat_callback, this, _1));
  command_stop_current_sub = this->create_subscription<std_msgs::msg::Empty>("mavlink_interface/command/cancel", qos_reliable, std::bind(&mavlora_node::ros_command_stop_current, this, _1));
  mission_item_int_sub = this->create_subscription<mavlink_msgs::msg::MavloraMissionItemInt>("mavlink_interface/mission/uploader/mission_item", qos_reliable, std::bind(&mavlora_node::ros_send_mission_int_callback, this, _1));
  mission_count_sub = this->create_subscription<std_msgs::msg::UInt16>("mavlink_interface/mission/uploader/mission_count", qos_reliable, std::bind(&mavlora_node::ros_mission_count_send_callback, this, _1));

  // we make publishers
  msg_pub = this->create_publisher<mavlink_msgs::msg::MavloraMsg>("mavlink_rx", qos_volatile_besteffort);
  pos_pub = this->create_publisher<mavlink_msgs::msg::MavloraPos>("mavlink_pos", qos_volatile_besteffort);
  attitude_pub = this->create_publisher<mavlink_msgs::msg::MavloraAttitude>("mavlink_attitude", qos_volatile_besteffort);
  status_pub = this->create_publisher<mavlink_msgs::msg::MavloraStatus>("mavlink_status", qos_volatile_besteffort);
  statustext_pub = this->create_publisher<mavlink_msgs::msg::MavloraStatustext>("mavlink_statustext", 10);
  gps_raw_pub = this->create_publisher<mavlink_msgs::msg::MavloraGpsRaw>("mavlink_gps_raw", qos_volatile_besteffort);
  radio_status_pub = this->create_publisher<mavlink_msgs::msg::MavloraRadioStatus>("mavlink_radio_status", qos_volatile_besteffort);
  comm_status_pub = this->create_publisher<mavlink_msgs::msg::MavloraCommStatus>("mavlink_comm_status", qos_volatile_besteffort);
  rc_channels_pub = this->create_publisher<mavlink_msgs::msg::MavloraRcChannels>("mavlink_rc_channels", qos_volatile_besteffort);
  mission_ack_pub = this->create_publisher<mavlink_msgs::msg::MavloraMissionAck>("mavlink_interface/mission/ack", qos_reliable);
  mission_current_pub = this->create_publisher<std_msgs::msg::UInt16>("mavlink_interface/mission/current", qos_volatile_besteffort);
  command_ack_pub = this->create_publisher<mavlink_msgs::msg::MavloraCommandAck>("mavlink_interface/command/ack", qos_reliable);
  mission_request_pub = this->create_publisher<std_msgs::msg::UInt16>("mavlink_interface/mission/uploader/request", qos_reliable);
  heartbeat_pub = this->create_publisher<mavlink_msgs::msg::MavloraHeartbeat>("mavlink_heartbeat_rx", qos_volatile_besteffort);

  // Enable communication based on ROS parameters
  this->declare_parameter("serial_device", "/dev/ttyUSB0");
  this->declare_parameter("serial_baudrate", 57600);
  this->declare_parameter("heartbeats", false);

  rclcpp::Parameter param_serial_device = this->get_parameter("serial_device");
  rclcpp::Parameter param_serial_baudrate = this->get_parameter("serial_baudrate");
  rclcpp::Parameter param_heartbeats_mock = this->get_parameter("heartbeats");
  bool heartbeats_mock = param_heartbeats_mock.as_bool();

  RCLCPP_INFO(this->get_logger(), "%s Opening serial device: %s baudrate: %d", _logging_name.c_str(), param_serial_device.value_to_string().c_str(), (int)param_serial_baudrate.as_int());
  if (!loraLib.enableCommunicationSerial(param_serial_device.as_string(), param_serial_baudrate.as_int()))
  {
    RCLCPP_ERROR(this->get_logger(), "Unable to open port");
    return;
  }
  RCLCPP_INFO(this->get_logger(),"Serial Port initialized");


  /* ros main loop */
  RCLCPP_DEBUG(this->get_logger(),"Starting main loop");
  rclcpp::Rate loop_rate(100);

  while(rclcpp::ok())
  {

    /* check if it is time to send a status message */
    if (status_msg_sent + rclcpp::Duration(1,0) <= this->now()) {
      ml_send_status();
    }

    if (heartbeat_msg_sent + rclcpp::Duration(HEARTBEAT_RATE_SEC,HEARTBEAT_RATE_NANO) <= this->now() && heartbeats_mock) {
      ml_send_heartbeat();
    }

    //update tick for mavlink_lib
    loraLib.update();

    // spin
    rclcpp::spin_some(this->get_node_base_interface());

    //sleep
    loop_rate.sleep();
  }

  /* closing down */
  RCLCPP_INFO(this->get_logger(), "Shutting down MavLora Node");
  loraLib.shutdown();
}


bool mavlora_node::enableSerial(string serial_fc, int baud_fc)
{
  // init the lora libs
  bool result = loraLib.enableCommunicationSerial(std::move(serial_fc), baud_fc);

  //check if we opened ports successfully
  if (!result)
  {
    // failed, retry opening the port
    reopen_ports = true;
  }
  else
  {
    // reset flag to reopen ports, and set last heard to now.
    reopen_ports = false;
    last_heard = this->now(); // Set initial last_heard when ports have been opened
    last_heard_serial = this->now();
  }

  return result;
}
bool mavlora_node::enableUDP(const string& address, int port)
{
  // enable UDP
  bool result = loraLib.enableCommunicationUDP(address, port);

  return result;
}
int mavlora_node::reopen_serial()
{
  int result = loraLib.reopen_serial();

  if (result == -1)
    reopen_ports = true;
  else
  {
    // reopen successfully, resetting reopen flag and setting last heard to now
    reopen_ports = false;
    last_heard_serial = this->now();
  }

  return result;
}

/***************************************************************************/
/************** From FC *************/
void mavlora_node::parse_msg_from_fc(unsigned char *msg)
{
  auto now = this->now();

  // last heard from all serial, to detect and reopen serials
  last_heard_serial = now;

  // extract message info
  struct mavlink_msg_t m;
  m.payload_len = msg[ML_POS_PAYLOAD_LEN];
  m.seq = msg[ML_POS_PACKET_SEQ];
  m.sys_id = msg[ML_POS_SYS_ID];
  m.comp_id = msg[ML_POS_COMP_ID];
  m.msg_id = msg[ML_POS_MSG_ID];

  for (auto i=0; i<m.payload_len; i++) {
    m.payload.push_back(msg[ML_POS_PAYLOAD + i]);
  }

  unsigned char crc_lsb = msg[6 + m.payload_len];
  unsigned char crc_msb = msg[7 + m.payload_len];
  m.checksum = (8 << crc_msb) | crc_lsb;

  // if message isn't a radio_status then we update last heard, as radio always get injected from our own radio
  if (m.msg_id != MAVLINK_MSG_ID_RADIO_STATUS)
    last_heard = this->now();

  mavlink_msgs::msg::MavloraMsg base_msg;
  base_msg.msg_id = m.msg_id;
  base_msg.payload = m.payload;
  base_msg.payload_len = m.payload_len;
  base_msg.comp_id = m.comp_id;
  base_msg.sys_id = m.sys_id;
  base_msg.seq = m.seq;
  base_msg.checksum = m.checksum;
  base_msg.header.stamp = now;

  msg_pub->publish(base_msg);

  // handle status messages
  if	(m.msg_id == MAVLINK_MSG_ID_SYS_STATUS)
  {
    last_heard_sys = now;
    mavlink_sys_status_t sys_status = Mavlora::ml_unpack_msg_sys_status (&m.payload.front());
    sys_status_voltage_battery = sys_status.voltage_battery;
    sys_status_battery_remaining = sys_status.battery_remaining;
    sys_status_cpu_load = sys_status.load;
  }

  // handle attitude messages
  if	(m.msg_id == MAVLINK_MSG_ID_ATTITUDE)
  {
    last_heard_sys = now;
    mavlink_attitude_t attitude = Mavlora::ml_unpack_msg_attitude(&m.payload.front());

    // make ros msg
    mavlink_msgs::msg::MavloraAttitude attitude_msg;
    attitude_msg.pitch = attitude.pitch;
    attitude_msg.roll = attitude.roll;
    attitude_msg.yaw = attitude.yaw;
    attitude_msg.time_usec = attitude.time_boot_ms;

    //publish
    attitude_pub->publish(attitude_msg);
  }

  //handle statustext
  if (m.msg_id == MAVLINK_MSG_ID_STATUSTEXT)
  {
    //unpack msg
    mavlink_statustext_t stat_text = Mavlora::ml_unpack_msg_statustext(&m.payload.front());

    //create ros msg
    mavlink_msgs::msg::MavloraStatustext status_text;
    status_text.severity = stat_text.severity;
    status_text.text = stat_text.text;

    //publish
    statustext_pub->publish(status_text);
  }

  // handle pos messages
  if (m.msg_id == MAVLINK_MSG_ID_GLOBAL_POSITION_INT)
  {
    mavlink_msgs::msg::MavloraPos pos;
    pos.header.stamp = now;
    mavlink_global_position_int_t glob_pos = Mavlora::ml_unpack_msg_global_position_int (&m.payload.front());
    pos.time_usec = (uint64_t) glob_pos.time_boot_ms*1000;
    pos.lat = glob_pos.lat / 1e7;
    pos.lon = glob_pos.lon / 1e7;
    pos.alt = glob_pos.alt / 1e3;
    pos.vx = glob_pos.vx;
    pos.vy = glob_pos.vy;
    pos.vz = glob_pos.vz;
    pos.relative_alt = glob_pos.relative_alt / 1e3;
    pos.heading = glob_pos.hdg /100;
    pos_pub->publish(pos);
    msg_id_global_position_int_received = true;
  }

  //Send raw until system starts to report estimated position with global_pos
  if (m.msg_id == MAVLINK_MSG_ID_GPS_RAW_INT && !msg_id_global_position_int_received)
  {
    mavlink_msgs::msg::MavloraPos pos;
    pos.header.stamp = now;
    mavlink_gps_raw_int_t gri = Mavlora::ml_unpack_msg_gps_raw_int (&m.payload.front());
    pos.time_usec = gri.time_usec;
    pos.lat = gri.lat / 1e7;
    pos.lon = gri.lon / 1e7;
    pos.alt = gri.alt / 1e3;
    //pos.vx = gri.vx / 1e3;
    //pos.vy = gri.vy / 1e3;
    //pos.vz = gri.vz / 1e3;
    pos.relative_alt = -1;
    pos.heading = -1;
    pos_pub->publish(pos);
  }

  //always output gps_raw as seperate message as it gives gps_fix type, groundspeed, hdop, and vdop
  if (m.msg_id == MAVLINK_MSG_ID_GPS_RAW_INT)
  {
    mavlink_msgs::msg::MavloraGpsRaw raw;
    mavlink_gps_raw_int_t gri = Mavlora::ml_unpack_msg_gps_raw_int(&m.payload.front());
    raw.time_usec = gri.time_usec;
    raw.fix_type = gri.fix_type;
    raw.lat = gri.lat / 1e7;
    raw.lon = gri.lon / 1e7;
    raw.alt = gri.alt / 1e3; //mm -> m
    raw.eph = gri.eph;
    raw.epv = gri.epv;
    raw.vel = gri.vel / 100; //cm/s -> m/s
    raw.cog = gri.cog / 100; //cdeg -> deg
    raw.satellites_visible = gri.satellites_visible;

    gps_raw_pub->publish(raw);
  }

  //handle mission ack
  if (m.msg_id == MAVLINK_MSG_ID_MISSION_ACK)
  {
    //stop timer
    mission_command_timeout->cancel();

    //reset retries
    mission_clear_all_retries = 0;

    //unpack
    mavlink_mission_ack_t ack = Mavlora::ml_unpack_msg_mission_ack(&m.payload.front());

    //respond back with result
    mavlink_msgs::msg::MavloraMissionAck mis_ack;
    mis_ack.result = ack.type;
    mis_ack.result_text = mission_result_parser(ack.type);

    //publish ack
    mission_ack_pub->publish(mis_ack);

    // DEBUG
    RCLCPP_INFO(this->get_logger(), "%s Mission ack from FC: %s", _logging_name.c_str(), mis_ack.result_text.c_str());
    send_statustext(_logging_name + "Mission ack from FC: " + mis_ack.result_text, TextSeverity::INFO);
  }

  //handle command ack
  if (m.msg_id == MAVLINK_MSG_ID_COMMAND_ACK)
  {
    //stop timer
    sending_command = false;
    command_long_timeout->cancel();
    command_int_timeout->cancel();

    //reset confirmation
    confirmation = 0;

    //unpack
    mavlink_command_ack_t ack = Mavlora::ml_unpack_msg_command_ack(&m.payload.front());

    //respond back with result
    mavlink_msgs::msg::MavloraCommandAck ack_msg;
    ack_msg.command = ack.command;
    ack_msg.result = ack.result;
    ack_msg.result_text = command_result_parser(ack.result);

    //publish
    command_ack_pub->publish(ack_msg);

    //DEBUG
    RCLCPP_INFO(this->get_logger(), ack_msg.result_text.c_str());
  }

  //handle current mission
  if (m.msg_id == MAVLINK_MSG_ID_MISSION_CURRENT)
  {
    // make msg to MQTT format and send
    uint16_t seq = Mavlora::ml_unpack_msg_mission_current(&m.payload.front());

    std_msgs::msg::UInt16 rosmsg;
    rosmsg.data = seq;

    //publish
    mission_current_pub->publish(rosmsg);
  }

  // handle heartbeat messages
  if (m.msg_id == MAVLINK_MSG_ID_HEARTBEAT)
  {
    mavlink_heartbeat_t hb = Mavlora::ml_unpack_msg_heartbeat(&m.payload.front());

    //msg
    mavlink_msgs::msg::MavloraHeartbeat hb_msg;

    hb_msg.autopilot = hb.autopilot;
    hb_msg.base_mode = hb.base_mode;
    hb_msg.custom_mode = hb.custom_mode;
    hb_msg.type = hb.type;
    hb_msg.system_id = m.sys_id;
    hb_msg.system_status = hb.system_status;

    //publish
    heartbeat_pub->publish(hb_msg);
  }

  //handle mission_request items
  if (m.msg_id == MAVLINK_MSG_ID_MISSION_REQUEST_INT || m.msg_id == MAVLINK_MSG_ID_MISSION_REQUEST)
  {
    //unpack and update requested seq
    mavlink_mission_request_int_t request = Mavlora::ml_unpack_msg_mission_request_int(&m.payload.front());

    RCLCPP_INFO(this->get_logger(), "%s Item requested from FC: %d", _logging_name.c_str(), request.seq);

    //publish on ros system
    std_msgs::msg::UInt16 mission_item_request;
    mission_item_request.data = request.seq;

    mission_request_pub->publish(mission_item_request);
  }

  //handle radio comm status from the drone
  if (m.msg_id == MAVLINK_MSG_ID_RADIO_COMM_STATUS)
  {
    //unpack msg
    mavlink_radio_comm_status_t status = Mavlora::ml_unpack_msg_radio_comm_status(&m.payload.front());
    mavlink_msgs::msg::MavloraCommStatus commStatus;

    rclcpp::Time lastheard_fc(status.lastheard_fc,0);
    rclcpp::Time lastheard_gcs(status.lastheard_gcs, 0);

    commStatus.lastheard_fc = lastheard_fc;
    commStatus.lastheard_gcs = lastheard_gcs;
    commStatus.lost_messages_gcs = status.lost_messages_gcs;
    commStatus.lost_messages_fc = status.lost_messages_fc;
    commStatus.received_msgs_gcs = status.received_msgs_gcs;
    commStatus.received_msgs_fc = status.received_msgs_fc;
    commStatus.transmitted_msgs_gcs = status.transmitted_msgs_gcs;
    commStatus.transmitted_msgs_fc = status.transmitted_msgs_fc;
    commStatus.dualrx_status = status.dualrx_status;

    comm_status_pub->publish(commStatus);
  }

  //handle radio_status
  if (m.msg_id == MAVLINK_MSG_ID_RADIO_STATUS)
  {
    // unpack msg
    mavlink_radio_status_t status = Mavlora::ml_unpack_msg_radio_status(&m.payload.front());
    mavlink_msgs::msg::MavloraRadioStatus radioStatus;

    int rssi, remrssi, rssi_limited, remrssi_limited;

    // 3DR Si1k radio needs rssi to be converted to db. This is the same way QGroundControl calculates it
    if (m.sys_id == '3' && m.comp_id == 'D') {
      /* Per the Si1K datasheet figure 23.25 and SI AN474 code
       * samples the relationship between the RSSI register
       * and received power is as follows:
       *
       *                       10
       * inputPower = rssi * ------ 127
       *                       19
       *
       * Additionally limit to the only realistic range [-120,0] dBm
       * signal_dbm = (RSSI / 1.9) - 127.
       */

      rssi = static_cast<int>(std::round(static_cast<double>(status.rssi) / 1.9 - 127.0));
      rssi_limited = std::min(std::max(rssi, -120), 0);

      remrssi = static_cast<int>(std::round(static_cast<double>(status.remrssi) / 1.9 - 127.0));
      remrssi_limited = std::min(std::max(remrssi, -120), 0);
    } else {
      rssi_limited = (int8_t) status.rssi;
      remrssi_limited = (int8_t) status.remrssi;
    }

    // build ros package
    radioStatus.rssi = rssi_limited;
    radioStatus.remrssi = remrssi_limited;
    radioStatus.noise = status.noise;
    radioStatus.remnoise = status.remnoise;
    radioStatus.txbuf = status.txbuf;
    radioStatus.rxerrors = status.rxerrors;
    radioStatus.fixed = status.fixed;

    // publish
    radio_status_pub->publish(radioStatus);
  }

  // RC channels
  if (m.msg_id == MAVLINK_MSG_ID_RC_CHANNELS)
  {
    // unpack msg
    mavlink_rc_channels_t rcChan = Mavlora::ml_unpack_msg_rc_channels(&m.payload.front());
    mavlink_msgs::msg::MavloraRcChannels rcChannels;

    // build ros msg
    rcChannels.time_boot_ms = rcChan.time_boot_ms;
    rcChannels.chancount = rcChan.chancount;
    rcChannels.chan1_raw = rcChan.chan1_raw;
    rcChannels.chan2_raw = rcChan.chan2_raw;
    rcChannels.chan3_raw = rcChan.chan3_raw;
    rcChannels.chan4_raw = rcChan.chan4_raw;
    rcChannels.chan5_raw = rcChan.chan5_raw;
    rcChannels.chan6_raw = rcChan.chan6_raw;
    rcChannels.chan7_raw = rcChan.chan7_raw;
    rcChannels.chan8_raw = rcChan.chan8_raw;
    rcChannels.chan9_raw = rcChan.chan9_raw;
    rcChannels.chan10_raw = rcChan.chan10_raw;
    rcChannels.chan11_raw = rcChan.chan11_raw;
    rcChannels.chan12_raw = rcChan.chan12_raw;
    rcChannels.chan13_raw = rcChan.chan13_raw;
    rcChannels.chan14_raw = rcChan.chan14_raw;
    rcChannels.chan15_raw = rcChan.chan15_raw;
    rcChannels.chan16_raw = rcChan.chan16_raw;
    rcChannels.chan17_raw = rcChan.chan17_raw;
    rcChannels.chan18_raw = rcChan.chan18_raw;
    rcChannels.rssi = rcChan.rssi;

    // publish
    rc_channels_pub->publish(rcChannels);
  }
}

/***************************** COMMAND LONG ********************************/
void mavlora_node::ml_send_command_long(unsigned short cmd_id, float p1, float p2, float p3, float p4, float p5, float p6, float p7)
{
  // if already sending command, stop the current command and send new one
  if (sending_command)
  {
    command_long_timeout->cancel();
    command_int_timeout->cancel();
    confirmation = 0;
    sending_command = false;
    RCLCPP_INFO(this->get_logger(), "%s Already sending command, stopping current command transmission to send new command", _logging_name.c_str());
    send_statustext(_logging_name + "Already sending command, stopping current command transmission to send new command", TextSeverity::INFO);
  }
  // set to sending command now, so we can stop it if we want later by ros msg
  sending_command = true;

  RCLCPP_INFO(this->get_logger(), "%s Sending Command_long with id: %d, and confirmation: %d", _logging_name.c_str(), cmd_id, confirmation);
  send_statustext(_logging_name + "Sending Command_long with id: " + to_string(cmd_id) + ", and confirmation: " + to_string(confirmation), TextSeverity::INFO);

  //save command as last command
  last_cmd_long.command = cmd_id;
  last_cmd_long.param1 = p1;
  last_cmd_long.param2 = p2;
  last_cmd_long.param3 = p3;
  last_cmd_long.param4 = p4;
  last_cmd_long.param5 = p5;
  last_cmd_long.param6 = p6;
  last_cmd_long.param7 = p7;

  //queue msg, can only be sent to drone
  loraLib.ml_queue_msg_command_long(cmd_id, p1, p2, p3, p4, p5, p6, p7, confirmation);

  //start timeout
  command_long_timeout->reset();
}
void mavlora_node::ml_send_command_int(unsigned short cmd_id, unsigned char frame, float param1, float param2, float param3, float param4, int32_t x, int32_t y, float z)
{
  // if already sending command, stop the current command and send new one
  if (sending_command)
  {
    command_long_timeout->cancel();
    command_int_timeout->cancel();
    confirmation = 0;
    sending_command = false;
    RCLCPP_INFO(this->get_logger(), "%s Already sending command, stopping current command transmission to send new command", _logging_name.c_str());
    send_statustext(_logging_name + "Already sending command, stopping current command transmission to send new command", TextSeverity::INFO);
  }
  // set to sending command now, so we can stop it if we want later by ros msg
  sending_command = true;

  RCLCPP_INFO(this->get_logger(), "%s Sending Command_int with id: %d", _logging_name.c_str(), cmd_id);

  //save command as last command
  last_cmd_int.command = cmd_id;
  last_cmd_int.param1 = param1;
  last_cmd_int.param2 = param2;
  last_cmd_int.param3 = param3;
  last_cmd_int.param4 = param4;
  last_cmd_int.x = x;
  last_cmd_int.y = y;
  last_cmd_int.z = z;
  last_cmd_int.frame = frame;

  //queue msg, can only be sent to drone
  loraLib.ml_queue_msg_command_int(cmd_id, frame, param1, param2, param3, param4, x, y, z);

  //start timeout
  command_int_timeout->reset();
}

/***************************************************************************/
string mavlora_node::command_result_parser(uint8_t result)
{
  switch(result)
  {
    case 0:
      return "MAV_RESULT_ACCEPTED";
    case 1:
      return "MAV_RESULT_TEMPORARILY_REJECTED";
    case 2:
      return "MAV_RESULT_DENIED";
    case 3:
      return "MAV_RESULT_UNSUPPORTED";
    case 4:
      return "MAV_RESULT_FAILED";
    case 5:
      return "MAV_RESULT_IN_PROGRESS";
    case 6:
      return "MAV_RESULT_ACK_TIMEOUT"; //Timeout on long msgs
    default:
      return "DIDN'T RECOGNIZE RESULT CODE";
  }
}
/******************************* MISSIONS **********************************/
void mavlora_node::ml_send_single_mission_item_int(float p1, float p2, float p3, float p4, int x, int y, float z, unsigned short seq, unsigned short command, unsigned char frame, unsigned char current, unsigned char autocontinue)
{
  //send mission item. can only go to FC
  loraLib.ml_queue_msg_mission_item_int(p1, p2, p3, p4, x, y, z, seq, command, frame, current, autocontinue);
}
void mavlora_node::ml_send_mission_clear_all()
{
  // queue msg, only goes to FC
  loraLib.ml_queue_msg_mission_clear_all();

  // start timer
  mission_command_timeout->reset();
}
void mavlora_node::mission_count_send(const unsigned short count)
{
  //send mission count to the FC if mission-computer asks for it
  loraLib.ml_queue_msg_mission_count(count);
}
/************************* TIMER CALLBACKS *********************************/
void mavlora_node::mission_clear_all_timeout_callback()
{
  mission_clear_all_retries++;

  //check if retries has been exceeded
  if (mission_clear_all_retries > MISSION_MAX_RETRIES)
  {
    //publish error on ack topic
    mavlink_msgs::msg::MavloraMissionAck msg;
    msg.result = 20;
    msg.result_text = mission_result_parser(20);

    //publish on topic TODO
//        mission_ack_pub->publish(msg.SerializeAsString());

    //reset retries
    mission_clear_all_retries = 0;

    //debug
    RCLCPP_INFO(this->get_logger(), "%s MISSION_CLEAR_ALL MAX RETRIES REACHED", _logging_name.c_str());
    return;
  }

  //if timeout triggers, resend
  ml_send_mission_clear_all();
}
void mavlora_node::command_long_timeout_callback()
{
  // if sending command is false and we are in callback, we should stop the current command
  if (!sending_command)
  {
    command_long_timeout->cancel();
    command_int_timeout->cancel();
    confirmation = 0;
    return;
  }
  else
    sending_command = false; // Setting to false so the resend doesn't trigger the "already sending" message

  //if timeout triggers, increment confirmation and resend last command
  confirmation++;

  if (confirmation > MSG_LNG_MAX_RETRIES) {
    //publish error on ack topic
    mavlink_msgs::msg::MavloraCommandAck msg;
    msg.result = 6;
    msg.result_text = command_result_parser(6);

    //reset retries
    confirmation = 0;

    // publish msg
    command_ack_pub->publish(msg);

    //debug
    RCLCPP_INFO(this->get_logger(), "%s MAV_MSG_ACK_TIMEOUT", _logging_name.c_str());
    return;
  }

  send_statustext(_logging_name + "Resending command with id: " + to_string(last_cmd_long.command), TextSeverity::WARNING);
  ml_send_command_long(last_cmd_long.command, last_cmd_long.param1, last_cmd_long.param2, last_cmd_long.param3, last_cmd_long.param4, last_cmd_long.param5, last_cmd_long.param6, last_cmd_long.param7 );
}
void mavlora_node::command_int_timeout_callback()
{
  // if sending command is false and we are in callback, we should stop the current command
  if (!sending_command)
  {
    command_long_timeout->cancel();
    command_int_timeout->cancel();
    confirmation = 0;
    return;
  }
  confirmation++;
  if (confirmation > MSG_INT_MAX_RETRIES) {
    //publish error on ack topic
    mavlink_msgs::msg::MavloraCommandAck msg;
    msg.result = 6;
    msg.result_text = command_result_parser(6);

    //reset retries
    confirmation = 0;

    // publish msg
    command_ack_pub->publish(msg);

    //debug
    RCLCPP_INFO(this->get_logger(), "%s MAV_MSG_ACK_TIMEOUT", _logging_name.c_str());
    return;
  }

  //resend last command
  send_statustext(_logging_name + "Resending command with id: " + to_string(last_cmd_int.command), TextSeverity::WARNING);
  ml_send_command_int(last_cmd_int.command, last_cmd_int.frame, last_cmd_int.param1, last_cmd_int.param2, last_cmd_int.param3, last_cmd_int.param4, last_cmd_int.x, last_cmd_int.y, last_cmd_int.z );
}

/***************************************************************************/
string mavlora_node::mission_result_parser(uint8_t result)
{
  switch(result)
  {
    case 0:
      return "MAV_MISSION_ACCEPTED";
    case 1:
      return "MAV_MISSION_ERROR";
    case 2:
      return "MAV_MISSION_UNSUPPORTED_FRAME";
    case 3:
      return "MAV_MISSION_UNSUPPORTED";
    case 4:
      return "MAV_MISSION_NO_SPACE";
    case 5:
      return "MAV_MISSION_INVALID";
    case 6:
      return "MAV_MISSION_INVALID_PARAM1";
    case 7:
      return "MAV_MISSION_INVALID_PARAM2";
    case 8:
      return "MAV_MISSION_INVALID_PARAM3";
    case 9:
      return "MAV_MISSION_INVALID_PARAM4";
    case 10:
      return "MAV_MISSION_INVALID_PARAM5_X";
    case 11:
      return "MAV_MISSION_INVALID_PARAM6_Y";
    case 12:
      return "MAV_MISSION_INVALID_PARAM7";
    case 13:
      return "MAV_MISSION_INVALID_SEQUENCE";
    case 14:
      return "MAV_MISSION_DENIED";
    case 20:
      return "MAV_MISSION_MAX_RETRIES"; //Custom error indicating aborting due to max retries with no success
    case 21:
      return "MAV_MISSION_ACK_TIMEOUT"; //Timeout on ack for mission
//        case 22:
//            return "MAV_MISSION_APPEND_ACCEPTED"; //Custom ack for appending a single mission upload
//        case 23:
//            return "MAV_MISSION_ITEM_RECEIVED";
    default:
      return "DIDN'T RECOGNIZE RESULT CODE";
  }
}
/************************* INTERFACE COMMANDS ******************************/
void mavlora_node::command_arm_disarm(bool arm)
{
  //arm if true, disarm if false
  if (arm)
    send_statustext(_logging_name + "Sending Arm command", TextSeverity::INFO);
  else
    send_statustext(_logging_name + "Sending Disarm command", TextSeverity::INFO);

  //send msg
  ml_send_command_long(MAVLINK_MSG_ID_COMPONENT_ARM_DISARM, arm, 0, 0, 0, 0, 0, 0);
}
void mavlora_node::command_start_mission(int first_item, int last_item)
{
  //start mission, first and last item as parameter
  send_statustext(_logging_name + "Sending Start mission command", TextSeverity::INFO);
  //send msg
  ml_send_command_long(MAVLINK_MSG_ID_MISSION_START, first_item, last_item, 0, 0, 0, 0, 0);
}
void mavlora_node::command_set_mode(float mode, float custom_mode, float custom_sub_mode)
{
  //find mode and send to gui what mode it is trying to change to
  send_statustext(_logging_name + "Sending command to change mode to: " + decode_custom_mode(custom_mode, custom_sub_mode), TextSeverity::INFO);
  ml_send_command_long(MAVLINK_MSG_ID_DO_SET_MODE, mode, custom_mode, custom_sub_mode, 0, 0, 0, 0);
}
void mavlora_node::command_land(float abort_alt, float precision_land_mode, float yaw_angle, float lat, float lon, float altitude)
{
  send_statustext(_logging_name + "Sending land command", TextSeverity::INFO);
  ml_send_command_int(MAVLINK_MSG_ID_NAV_LAND, MavFrame::GLOBAL_INT, abort_alt, precision_land_mode, 0, yaw_angle, lat, lon, altitude);
}
void mavlora_node::command_do_reposition(float ground_speed, float yaw_heading, float lat, float lon, float alt)
{
  //param2 == 1 --> means that it switches into loiter itself when receiving the command.
  send_statustext(_logging_name + "Sending reposition command at alt: " + to_string(alt), TextSeverity::INFO);
  ml_send_command_int(MAVLINK_MSG_ID_SET_REPOSITION, MavFrame::GLOBAL_INT, ground_speed, 1, 0, yaw_heading, lat, lon, alt);
}
void mavlora_node::command_do_pause_continue(const bool continue_mission)
{
  // 1 == continue mission, 0 == hold position
  if (continue_mission)
    send_statustext(_logging_name + "Sending mission continue command", TextSeverity::INFO);
  else
    send_statustext(_logging_name + "Sending mission pause command. Should hold position", TextSeverity::INFO);

  ml_send_command_long(MAVLINK_MSG_ID_DO_PAUSE_CONTINUE, continue_mission, 0, 0, 0, 0, 0, 0);
}
void mavlora_node::command_takeoff(float pitch, float yaw_angle, float lat, float lon, float alt) {
  //takeoff
  send_statustext(_logging_name + "Sending takeoff command to altitude: " + to_string(alt), TextSeverity::INFO);
  ml_send_command_int(MAVLINK_MSG_ID_NAV_TAKEOFF, MavFrame::GLOBAL_INT, pitch, 0, 0, yaw_angle, lat, lon, alt); //unchanged yaw_angle == nanf()
}
/***************************************************************************/
/******************************** HELPERS **********************************/
/***************************************************************************/
void mavlora_node::send_statustext(string text, int severity) {
  // send statustext to frontend
  mavlink_msgs::msg::MavloraStatustext msg;
  msg.text = text;
  msg.severity = severity;
  statustext_pub->publish(msg);
}
string mavlora_node::decode_custom_mode(unsigned long custom_mode, unsigned long custom_submode) {
  // decode and return flight mode based on custom_mode

  string sub_text = "";
  string base_text = "";

  switch(custom_submode) {
    case 1:
      sub_text = "READY";
      break;
    case 2:
      sub_text = "TAKEOFF";
      break;
    case 3:
      sub_text = "LOITER";
      break;
    case 4:
      sub_text = "MISSION";
      break;
    case 5:
      sub_text = "RTL";
      break;
    case 6:
      sub_text = "LAND";
      break;
    case 7:
      sub_text = "RTGS";
      break;
    case 8:
      sub_text = "FOLLOW_TARGET";
      break;
    case 9:
      sub_text = "PRECLAND";
      break;
    default:
      break;
  }


  switch(custom_mode) {
    case 1:
      base_text = "MANUAL";
      break;
    case 2:
      base_text = "ALTITUDE CONTROL";
      break;
    case 3:
      base_text = "POSITION CONTROL";
      break;
    case 4:
      base_text = "AUTO";
      break;
    case 5:
      base_text = "ARCO";
      break;
    case 6:
      base_text = "OFFBOARD";
      break;
    case 7:
      base_text = "STABILIZED";
      break;
    case 8:
      base_text = "RATTITUDE";
      break;
    default:
      break;
  }

  if (sub_text.empty())
    return base_text;
  else
    return base_text + " - " + sub_text;

}

/***************************************************************************/
/***************************** ROS CALLBACKS *******************************/
/***************************************************************************/
void mavlora_node::ros_send_heartbeat_callback(const mavlink_msgs::msg::MavloraHeartbeat::SharedPtr msg)
{
  //queue heartbeat to FC
  loraLib.ml_queue_msg_heartbeat(msg->type, msg->autopilot, msg->base_mode, msg->custom_mode, msg->system_status, msg->system_id);
}
void mavlora_node::ros_mission_clear_all_callback(__attribute__ ((unused)) const std_msgs::msg::Empty::SharedPtr msg)
{
  RCLCPP_INFO(this->get_logger(), "%s Sending mission clear all", _logging_name.c_str());
  //queue command for clearing all missions
  ml_send_mission_clear_all();
}

void mavlora_node::ros_mission_count_send_callback(const std_msgs::msg::UInt16::SharedPtr msg)
{
  RCLCPP_INFO(this->get_logger(), "%s Sending mission count: %d", _logging_name.c_str(), msg->data);
  //queue command sending mission count
  mission_count_send(msg->data);
}

void mavlora_node::ros_send_mission_int_callback(const mavlink_msgs::msg::MavloraMissionItemInt::SharedPtr msg)
{
  RCLCPP_INFO(this->get_logger(), "%s Sending mission item with seq: %d", _logging_name.c_str(), msg->seq);
  //send mission item
  ml_send_single_mission_item_int(msg->param1, msg->param2, msg->param3, msg->param4, msg->x, msg->y, msg->z, msg->seq, msg->command, msg->frame, msg->current, msg->autocontinue);
}

void mavlora_node::ros_command_arm_disarm_callback(const std_msgs::msg::Bool::SharedPtr msg) {
  //arm if true, disarm if false
  command_arm_disarm(msg->data);
}

void mavlora_node::ros_command_start_mission_callback(const mavlink_msgs::msg::MavloraCommandStartMission::SharedPtr msg) {
  //start mission, first and last item as parameter
  command_start_mission(msg->first_item, msg->last_item);
}

void mavlora_node::ros_command_set_mode_callback(const mavlink_msgs::msg::MavloraCommandSetMode::SharedPtr msg) {
  //set mode
  command_set_mode(msg->mode, msg->custom_mode, msg->custom_sub_mode);
}

void mavlora_node::ros_command_takeoff_callback(const mavlink_msgs::msg::MavloraCommandTakeoff::SharedPtr msg) {
  //takeoff
  command_takeoff(msg->pitch, msg->yaw_angle, msg->lat, msg->lon, msg->alt);
}

void mavlora_node::ros_command_land_callback(const mavlink_msgs::msg::MavloraCommandLand::SharedPtr msg) {
  //land
  command_land(msg->abort_alt, msg->precision_land_mode, msg->yaw_angle, msg->lat, msg->lon, msg->altitude);
}

void mavlora_node::ros_command_reposition_callback(const mavlink_msgs::msg::MavloraCommandReposition::SharedPtr msg) {
  //reposition, can order the UAV to move to a certain position based on lat/lon.
  command_do_reposition(msg->ground_speed, msg->yaw_heading, msg->lat, msg->lon, msg->alt);
}

void mavlora_node::ros_command_pause_continue_callback(const std_msgs::msg::Bool::SharedPtr msg) {
  // 1 == continue mission, 0 == hold position
  command_do_pause_continue(msg->data);
}

void mavlora_node::ros_command_rtl(__attribute__ ((unused)) const mavlink_msgs::msg::MavloraCommandLong::SharedPtr msg) {
  // transmit rtl todo, for now it is just empty
  ml_send_command_long(MAVLINK_MSG_ID_RETURN_TO_LAUNCH, 0, 0, 0, 0, 0, 0, 0);
}

void mavlora_node::ros_command_message_interval(const mavlink_msgs::msg::MavloraCommandMessageInteval::SharedPtr msg) {
  // transmit message interval
  ml_send_command_long(MAVLINK_MSG_ID_SET_MESSAGE_INTERVAL, msg->message_id, msg->interval, 0, 0, 0, 0, 0);
}

void mavlora_node::ros_command_stop_current(__attribute__ ((unused)) const std_msgs::msg::Empty::SharedPtr msg) {
  // If a command is currently being sent, cancel it
  if (sending_command)
  {
    command_long_timeout->cancel();
    command_int_timeout->cancel();
    confirmation = 0;

    RCLCPP_INFO(this->get_logger(), "%s COMMAND has been canceled and will no longer try to resend", _logging_name.c_str());
    send_statustext(_logging_name + "COMMAND has been canceled and will no longer try to resend", TextSeverity::WARNING);
  } else {
    RCLCPP_INFO(this->get_logger(), "%s No command is being transmitted.... nothing to cancel", _logging_name.c_str());
    send_statustext(_logging_name + "No command is being transmitted.... nothing to cancel", TextSeverity::INFO);
  }
}

void mavlora_node::ml_send_status() {
  // send status message
  mavlink_msgs::msg::MavloraStatus status;

  status.header.stamp = this->now();
  status.last_heard = last_heard;
  status.last_heard_sys_status = last_heard_sys;
  status.batt_volt = sys_status_voltage_battery;
  status.cpu_load = sys_status_cpu_load;
  status.batt_remaining = sys_status_battery_remaining;
  status.msg_sent_gcs = loraLib.ml_messages_sent();
  status.msg_received_gcs = loraLib.ml_messages_received();
  status.msg_dropped_gcs = loraLib.ml_messages_crc_error();
  status.msg_lost_gcs = loraLib.ml_messages_lost(1); // 1 == PX4 default sys_id

  //publish
  status_pub->publish(status);

  //update when we last sent status msg
  status_msg_sent = this->now();
}

void mavlora_node::ml_send_generic(unsigned char sys_id, unsigned char comp_id, unsigned char msg_id,
                                   unsigned char payload_len, unsigned char *payload) {
  //Send a generic message over telemetry
  loraLib.ml_queue_msg_generic(sys_id, comp_id, msg_id, payload_len, payload);
}

/// Only used for debug/testing to emulate a heartbeat
void mavlora_node::ml_send_heartbeat() {
  //queue heartbeat
  loraLib.ml_queue_msg_heartbeat(6, 8, 136, 0, 4, my_system_id);

  heartbeat_msg_sent = this->now();
}

