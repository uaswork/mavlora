from http.server import executable
from launch import LaunchDescription
from launch.actions import IncludeLaunchDescription, DeclareLaunchArgument
from launch.launch_description_sources import PythonLaunchDescriptionSource
from launch.substitutions import ThisLaunchFileDir, TextSubstitution, LaunchConfiguration
from launch_ros.actions import Node


def generate_launch_description():

    serial_device_arg = DeclareLaunchArgument(
        "serial_device", default_value="/dev/ttyUSB0"
    )
    serial_baud_arg = DeclareLaunchArgument(
        "serial_baudrate", default_value='57600'
    )
    heartbeats_arg = DeclareLaunchArgument(
        "heartbeats", default_value='False'
    )

    mavlora_node = Node(
        package='mavlora_node_example', 
        executable='mavlora_node',
        arguments=[],
        parameters=[
            {'serial_device': LaunchConfiguration('serial_device')},
            {'serial_baudrate': LaunchConfiguration('serial_baudrate')},
            {'heartbeats': LaunchConfiguration('heartbeats')}
        ],
        output='screen'
    )

    return LaunchDescription([
        serial_device_arg,
        serial_baud_arg,
        heartbeats_arg,
        mavlora_node
    ])