# Mavlora Node Example
This is a c++ ros2 node, that uses the Mavlora library and acts as a ros-wrapper.   
It exposes most of the basic interfaces with mavlink. 

## Requirements 
**Only tested with ROS2 Foxy (20.04)**

Mavlora library must be installed. Follow the [readme](https://gitlab.com/uaswork/mavlora/-/blob/master/readme.md)   at the root of the repo, to see how to install it.   

mavlink_msgs must be part of the workspace, as it depends on the messages.   

If you want to upload missions without implementing the mission protocol, you can use the ``mission_uploader`` package that is also added to this example_repo. It implements the protocol and uses the mavlora_node to upload the mission to the FC.  

## Usage
### Mavlora Node
To run just the mavlora_node you can launch it with:
```bash
ros2 launch mavlora_node_example mavlora_node_launch.py 
```

Or to run the mavlora node together with the mission-uploader node:
```bash
ros2 launch mavlora_node_example mavlora_node_mission_launch.py
```

You can add arguments to all launch files as per usual. By default it exposes the ``serial_device``, ``serial_baudrate`` and ``heartbeats`` variables. example:
```bash
ros2 launch mavlora_node_example mavlora_node_mission_launch.py serial_device:="/dev/ttyACM0" serial_baudrate:=115200
```


### Mission Uploader node
The mission uploader can be started alone if required with:
```bash
ros2 launch mission_uploader mission_uploader_launch.xml
```

A python script to test the uploader and transmit a hardcoded mission, you can use the python script in the ``mission_uploader/src`` folder. It can be run with:
```bash
 ros2 run mission_uploader upload_mission_test.py
```
If you check the python file, you can see it just publishes an ``mavlink_msgs/msg/MissionItem`` type message to the uploader, which then handles the rest to get it uploaded to the FC.

**Sometimes I have experienced you have to run it a second time to proper work. However I can not reproduce the error, and it happens very rarely**