/*
 * Copyright (c) 2022, Frederik Mazur Andersen <fm@mmmi.sdu.dk>/<fm@fmazur.dk>
 *
 * SDU UAS Center, http://sdu.dk/uas, University of Southern Denmark
 * All rights reserved.
 *
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE.txt' at the root of the repo.
 */

#ifndef BUILD_MAVLORA_NODE_HPP
#define BUILD_MAVLORA_NODE_HPP


#include "rclcpp/rclcpp.hpp"
#include <std_msgs/msg/string.hpp>
#include <std_msgs/msg/empty.hpp>
#include <std_msgs/msg/bool.hpp>
#include <std_msgs/msg/u_int16.hpp>
#include <mavlink_msgs/msg/mavlora_msg.hpp>
#include <mavlink_msgs/msg/mavlora_pos.hpp>
#include <mavlink_msgs/msg/mavlora_attitude.hpp>
#include <mavlink_msgs/msg/mavlora_status.hpp>
#include <mavlink_msgs/msg/mavlora_mission_item_int.hpp>
#include <mavlink_msgs/msg/mavlora_mission_list.hpp>
#include <mavlink_msgs/msg/mavlora_mission_ack.hpp>
#include <mavlink_msgs/msg/mavlora_command_ack.hpp>
#include <mavlink_msgs/msg/mavlora_command_start_mission.hpp>
#include <mavlink_msgs/msg/mavlora_command_set_mode.hpp>
#include <mavlink_msgs/msg/mavlora_statustext.hpp>
#include <mavlink_msgs/msg/mavlora_heartbeat.hpp>
#include <mavlink_msgs/msg/mavlora_set_position_target_local_ned.hpp>
#include <mavlink_msgs/msg/mavlora_enable_offboard.hpp>
#include <mavlink_msgs/msg/mavlora_command_takeoff.hpp>
#include <mavlink_msgs/msg/mavlora_command_land.hpp>
#include <mavlink_msgs/msg/mavlora_command_reposition.hpp>
#include <mavlink_msgs/msg/mavlora_gps_raw.hpp>
#include <mavlink_msgs/msg/mavlora_radio_status.hpp>
#include <mavlink_msgs/msg/mavlora_command_message_inteval.hpp>
#include <mavlink_msgs/msg/mavlora_battery_status.hpp>
#include <mavlink_msgs/msg/mavlora_command_long.hpp>
#include <mavlink_msgs/msg/mavlora_comm_status.hpp>
#include <mavlink_msgs/msg/mavlora_rc_channels.hpp>

#include <mavlora/mavlora.hpp>

using namespace mavlora;
using namespace std;
using std::placeholders::_1;

/* Defines */
#define DEFAULT_TIMEOUT_TIME 1500ms //1.5sec
#define MISSION_MAX_RETRIES 5
#define MSG_LNG_MAX_RETRIES 5
#define MSG_INT_MAX_RETRIES 5
#define HEARTBEAT_RATE 0.5
#define HEARTBEAT_RATE_SEC 0 //0.5 //2 //hz
#define HEARTBEAT_RATE_NANO 5e8
#define SERIAL_NO_COMMS_TIMEOUT 10
#define SERIAL_REOPEN_PORTS_TIMEOUT 2

class mavlora_node : public rclcpp::Node {

public:
        mavlora_node();

        /// program workers
        bool enableSerial(string serial_fc, int baud_fc);
        bool enableUDP(const string& address, int port);
        int reopen_serial();
        void ml_send_status();
        void ml_send_heartbeat();
        void ml_send_generic(unsigned char sys_id, unsigned char comp_id, unsigned char msg_id,
                         unsigned char payload_len, unsigned char *payload);

private:
        // Logging vars
        string _logging_name = "[MAVLORA NODE] ";

        // Lora Lib
        Mavlora loraLib;
        
        void parse_msg_from_fc(unsigned char *msg);
        unsigned long update_cnt;

        /// Helpers
        void send_statustext(string text, int severity);
        string decode_custom_mode(unsigned long custom_mode, unsigned long custom_submode);

        /// System Status vars
        uint16_t sys_status_voltage_battery;
        int8_t sys_status_battery_remaining;
        uint16_t sys_status_cpu_load;
        unsigned long secs_init;
        rclcpp::Time last_heard, last_heard_sys, last_heard_serial;
        rclcpp::Time status_msg_sent;
        bool msg_id_global_position_int_received;

        rclcpp::Time reopen_serial_timeout;
        bool reopen_ports = false;

        /// system vars
        unsigned char my_system_id = 255;

        /// Heartbeat & monitor
        rclcpp::Time heartbeat_msg_sent;
        rclcpp::Time monitor_hb_msg_sent;

        /// Mission operations
        int mission_clear_all_retries = 0;
        void mission_count_send(const unsigned short count);

        /// Command Protocol
        unsigned int confirmation = 0; // increments for each timeout of same command. Useful to monitor if a command should be killed
        mavlink_command_long_t last_cmd_long; // Saving parameters for resending last command
        mavlink_command_int_t last_cmd_int; // Saving parameters for resending last command

        /// function prototypes
        void ml_send_mission_clear_all();
        void ml_send_single_mission_item_int(float p1, float p2, float p3, float p4, int x, int y, float z, unsigned short seq, unsigned short command, unsigned char frame, unsigned char current, unsigned char autocontinue);
        void ml_send_command_long(unsigned short cmd_id, float p1, float p2, float p3, float p4, float p5, float p6, float p7);
        void ml_send_command_int(unsigned short cmd_id, unsigned char frame, float param1, float param2, float param3, float param4, int32_t x, int32_t y, float z);
        static string command_result_parser(uint8_t result);
        static string mission_result_parser(uint8_t result);

        /// command interfaces
        void command_arm_disarm(bool arm);
        void command_start_mission(int first_item, int last_item);
        void command_set_mode(float mode, float custom_mode, float custom_sub_mode);
        void command_land(float abort_alt, float precision_land_mode, float yaw_angle, float lat, float lon, float altitude);
        void command_takeoff(float pitch, float yaw_angle, float lat, float lon, float alt);
        void command_do_reposition(float ground_speed, float yaw_heading, float lat, float lon, float alt);
        void command_do_pause_continue(const bool continue_mission);

        /// Timers
        rclcpp::TimerBase::SharedPtr mission_command_timeout;
        rclcpp::TimerBase::SharedPtr command_long_timeout;
        rclcpp::TimerBase::SharedPtr command_int_timeout;

        /// Timer callbacks
        void command_long_timeout_callback();
        void command_int_timeout_callback();
        void mission_clear_all_timeout_callback();

        /// Command service stop
        atomic_bool sending_command{false};

        /// ROS pubs/subs
        rclcpp::Publisher<mavlink_msgs::msg::MavloraMsg>::SharedPtr msg_pub;
        rclcpp::Publisher<mavlink_msgs::msg::MavloraPos>::SharedPtr pos_pub;
        rclcpp::Publisher<mavlink_msgs::msg::MavloraAttitude>::SharedPtr attitude_pub;
        rclcpp::Publisher<mavlink_msgs::msg::MavloraStatus>::SharedPtr status_pub;
        rclcpp::Publisher<mavlink_msgs::msg::MavloraStatustext>::SharedPtr statustext_pub;
        rclcpp::Publisher<mavlink_msgs::msg::MavloraGpsRaw>::SharedPtr gps_raw_pub;
        rclcpp::Publisher<mavlink_msgs::msg::MavloraRadioStatus>::SharedPtr radio_status_pub;
        rclcpp::Publisher<mavlink_msgs::msg::MavloraCommStatus>::SharedPtr comm_status_pub;
        rclcpp::Publisher<mavlink_msgs::msg::MavloraRcChannels>::SharedPtr rc_channels_pub;
        rclcpp::Publisher<mavlink_msgs::msg::MavloraMissionAck>::SharedPtr mission_ack_pub;
        rclcpp::Publisher<std_msgs::msg::UInt16>::SharedPtr mission_current_pub;
        rclcpp::Publisher<std_msgs::msg::UInt16>::SharedPtr mission_request_pub;
        rclcpp::Publisher<mavlink_msgs::msg::MavloraCommandAck>::SharedPtr command_ack_pub;
        rclcpp::Publisher<mavlink_msgs::msg::MavloraHeartbeat>::SharedPtr heartbeat_pub;
        
        // subs
        rclcpp::Subscription<std_msgs::msg::Empty>::SharedPtr mission_clear_all_sub;
        rclcpp::Subscription<std_msgs::msg::Bool>::SharedPtr command_arm_disarm_sub;
        rclcpp::Subscription<mavlink_msgs::msg::MavloraCommandStartMission>::SharedPtr command_start_mission_sub;
        rclcpp::Subscription<mavlink_msgs::msg::MavloraCommandSetMode>::SharedPtr command_set_mode_sub;
        rclcpp::Subscription<mavlink_msgs::msg::MavloraCommandTakeoff>::SharedPtr command_takeoff_sub;
        rclcpp::Subscription<mavlink_msgs::msg::MavloraCommandLand>::SharedPtr command_land_sub;
        rclcpp::Subscription<mavlink_msgs::msg::MavloraCommandReposition>::SharedPtr command_reposition_sub;
        rclcpp::Subscription<std_msgs::msg::Bool>::SharedPtr command_pause_continue_sub;
        rclcpp::Subscription<mavlink_msgs::msg::MavloraCommandLong>::SharedPtr command_rtl;
        rclcpp::Subscription<mavlink_msgs::msg::MavloraCommandMessageInteval>::SharedPtr command_message_interval_sub;
        rclcpp::Subscription<std_msgs::msg::Empty>::SharedPtr command_stop_current_sub;
        rclcpp::Subscription<std_msgs::msg::UInt16>::SharedPtr mission_count_sub;
        rclcpp::Subscription<mavlink_msgs::msg::MavloraMissionItemInt>::SharedPtr mission_item_int_sub;
        rclcpp::Subscription<mavlink_msgs::msg::MavloraHeartbeat>::SharedPtr heartbeat_sub;

        /// ROS callbacks
        void ros_send_heartbeat_callback(mavlink_msgs::msg::MavloraHeartbeat::SharedPtr msg);
        void ros_command_arm_disarm_callback(std_msgs::msg::Bool::SharedPtr msg);
        void ros_command_start_mission_callback(mavlink_msgs::msg::MavloraCommandStartMission::SharedPtr msg);
        void ros_command_set_mode_callback(mavlink_msgs::msg::MavloraCommandSetMode::SharedPtr msg);
        void ros_command_takeoff_callback(mavlink_msgs::msg::MavloraCommandTakeoff::SharedPtr msg);
        void ros_command_land_callback(mavlink_msgs::msg::MavloraCommandLand::SharedPtr msg);
        void ros_command_reposition_callback(mavlink_msgs::msg::MavloraCommandReposition::SharedPtr msg);
        void ros_command_pause_continue_callback(std_msgs::msg::Bool::SharedPtr msg);
        void ros_command_rtl(mavlink_msgs::msg::MavloraCommandLong::SharedPtr msg);
        void ros_command_message_interval(mavlink_msgs::msg::MavloraCommandMessageInteval::SharedPtr msg);
        void ros_command_stop_current(std_msgs::msg::Empty::SharedPtr msg);

        //missions
        void ros_mission_count_send_callback(std_msgs::msg::UInt16::SharedPtr msg);
        void ros_send_mission_int_callback(mavlink_msgs::msg::MavloraMissionItemInt::SharedPtr msg);
        void ros_mission_clear_all_callback(std_msgs::msg::Empty::SharedPtr msg);
};


#endif //BUILD_MAVLORA_NODE_HPP
