#!/usr/bin/env python3
# /*
# * Copyright (c) 2022, Frederik Mazur Andersen <fm@mmmi.sdu.dk>/<fm@fmazur.dk>
# *
# * SDU UAS Center, http://sdu.dk/uas, University of Southern Denmark
# * All rights reserved.
# *
# * This file is subject to the terms and conditions defined in
# * file 'LICENSE.txt' at the root of the repo.
# */

import rclpy
from rclpy.node import Node
from mavlink_msgs.msg import MavloraMissionList, MavloraMissionItemInt


class UploaderTest(Node):
    def __init__(self):
        super().__init__('uploader_test')
        # init
        self.missionList = MavloraMissionList()
        self.missionList.append_start_index = -1

        # pubs
        qos_policy = rclpy.qos.QoSProfile(reliability=rclpy.qos.ReliabilityPolicy.RELIABLE, history=rclpy.qos.HistoryPolicy.KEEP_LAST, depth=1)
        self.send_mission_pub = self.create_publisher(MavloraMissionList, 'mavlink_interface/mission/uploader/upload_mission', qos_policy)

        self.run()

    def run(self):
        # run

        # TODO mission assumes we are at airport
        alt = 100
        yaw = float('nan')

        # CHANGE SPEED
        speed = MavloraMissionItemInt()
        speed.target_system = 0
        speed.target_component = 0
        speed.seq = 0  # Sequence in the list. Starts from 0 and every item increments by one
        speed.frame = 2  # mission command frame
        speed.command = 178  # change speed id
        speed.param1 = 1.0  # Ground speed
        speed.param2 = 10.0  # m/s
        speed.param3 = -1.0  # no change
        speed.param4 = 0.0  # absolute or relative. relative = 1
        speed.autocontinue = 1  # automatic continue to next waypoint when this is reached
        speed.current = True

        takeoff = MavloraMissionItemInt()
        takeoff.target_system = 0
        takeoff.target_component = 0
        takeoff.seq = 1
        takeoff.frame = 6
        takeoff.command = 22
        takeoff.param1 = 15.0
        takeoff.param2 = 0.0
        takeoff.param3 = 0.0
        takeoff.param4 = yaw
        takeoff.x = int(55.4719762 * 10000000)
        takeoff.y = int(10.3248095 * 10000000)
        takeoff.z = float(alt)
        takeoff.autocontinue = 1

        # waypoint
        way1 = MavloraMissionItemInt()
        way1.target_system = 0
        way1.target_component = 0
        way1.seq = 2
        way1.frame = 6  # global pos, relative alt
        way1.command = 16
        way1.x = int(55.4736667 * 10000000)
        way1.y = int(10.3230822 * 10000000)
        way1.z = float(alt)
        way1.param1 = 0.0
        way1.param2 = 5.0
        way1.param4 = yaw
        way1.autocontinue = 1

        # WAYPOINT 1
        way2 = MavloraMissionItemInt()
        way2.target_system = 0
        way2.target_component = 0
        way2.seq = 3
        way2.frame = 6  # global pos, relative alt
        way2.command = 16
        way2.param1 = 0.0  # hold time
        way2.param2 = 5.0
        way2.param4 = float(yaw)
        way2.x = int(55.4722863 * 10000000)
        way2.y = int(10.3187584 * 10000000)
        way2.z = float(alt)
        way2.autocontinue = 1

        # WAYPOINT 2
        way3 = MavloraMissionItemInt()
        way3.target_system = 0
        way3.target_component = 0
        way3.seq = 4
        way3.frame = 6  # global pos, relative alt_int
        way3.command = 16
        way3.param1 = 0.0  # hold time
        way3.param2 = 5.0
        way3.param4 = float(yaw)
        way3.x = int(55.4716843 * 10000000)
        way3.y = int(10.3194022 * 10000000)
        way3.z = float(alt)
        way3.autocontinue = 1

        # WAYPOINT 3
        way4 = MavloraMissionItemInt()
        way4.target_system = 0
        way4.target_component = 0
        way4.seq = 5
        way4.frame = 6  # global pos, relative alt
        way4.command = 16
        way4.param1 = 0.0  # hold time
        way4.param2 = 5.0
        way4.param4 = float(yaw)
        way4.x = int(55.4712464 * 10000000)
        way4.y = int(10.3226315 * 10000000)
        way4.z = float(alt)
        way4.autocontinue = 1

        # waypoint
        way5 = MavloraMissionItemInt()
        way5.target_system = 0
        way5.target_component = 0
        way5.seq = 6
        way5.frame = 6  # global pos, relative alt
        way5.command = 16
        way5.param1 = 0.0  # hold time
        way5.param2 = 5.0
        way5.param4 = float(yaw)
        way5.x = int(55.4719093 * 10000000)
        way5.y = int(10.3247022 * 10000000)
        way5.z = float(alt)
        way5.autocontinue = 1

        # waypoint
        # way6 = mavlink_lora_mission_item_int()
        # way6.target_system = 0
        # way6.target_component = 0
        # way6.seq = 7
        # way6.frame = 6  # global pos, relative alt
        # way6.command = 16
        # way6.param1 = 0  # hold time
        # way6.x = int(55.4719299 * 10000000)
        # way6.y = int(10.3249537 * 10000000)
        # way6.z = 100
        # way6.autocontinue = 1

        # WAYPOINT 4 LANDING
        land = MavloraMissionItemInt()
        land.target_system = 0
        land.target_component = 0
        land.seq = 7
        land.frame = 6  # global pos, relative alt
        land.command = 21
        land.param1 = 0.0  # abort alt
        land.param2 = 0.0
        land.param3 = 0.0
        land.x = int(55.4719093 * 10000000)
        land.y = int(10.3247022 * 10000000)
        land.z = 0.0
        land.autocontinue = 1

        # add waypoints to list
        self.missionList.waypoints.append(speed)
        self.missionList.waypoints.append(takeoff)
        self.missionList.waypoints.append(way1)
        self.missionList.waypoints.append(way2)
        self.missionList.waypoints.append(way3)
        self.missionList.waypoints.append(way4)
        self.missionList.waypoints.append(way5)
        self.missionList.waypoints.append(land)

        # upload mission
        print("sending mission to uploader")
        self.send_mission_pub.publish(self.missionList)


def main(args=None):
    rclpy.init(args=args)

    uploader = UploaderTest()

    rclpy.spin(uploader)

    # Destroy the node explicitly
    # (optional - otherwise it will be done automatically
    # when the garbage collector destroys the node object)
    uploader.destroy_node()
    rclpy.shutdown()


if __name__ == '__main__':
    main()
