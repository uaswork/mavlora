/*
 * Copyright (c) 2022, Frederik Mazur Andersen <fm@mmmi.sdu.dk>/<fm@fmazur.dk>
 *
 * SDU UAS Center, http://sdu.dk/uas, University of Southern Denmark
 * All rights reserved.
 *
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE.txt' at the root of the repo.
 */

#ifndef MISSION_UPLOADER_MISSION_UPLOADER_HPP
#define MISSION_UPLOADER_MISSION_UPLOADER_HPP

#include <utility>
#include <algorithm>    // std::max
#include <vector>
#include <string>
#include <chrono>
#include <iostream>
//#include <functional>
#include "rclcpp/rclcpp.hpp"

#include <std_msgs/msg/u_int16.hpp>
#include "mavlink_msgs/msg/mavlora_mission_item_int.hpp"
#include "mavlink_msgs/msg/mavlora_mission_list.hpp"
#include "mavlink_msgs/msg/mavlora_mission_ack.hpp"

using namespace std;
using std::placeholders::_1;

/* Defines */
#define DEFAULT_TIMEOUT_TIME 1500ms //1.5sec
#define MISSION_ITEM_TIMEOUT_TIME 5 //5sec
#define MISSION_MAX_RETRIES 5

class mission_uploader : public rclcpp::Node {

public:
    mission_uploader();
    void setup_ros_topics();

private:
    // precurser for logging purpose
    string log_precursor = "[UPLOADER]";

    /// Mission upload operations vars
    unsigned int mission_up_count = 0; /*< Total count of mission elements to be uploaded*/
    int mission_up_index = -1; /*< Current mission item getting uploaded */
    vector<mavlink_msgs::msg::MavloraMissionItemInt> missionlist; /*< list of all waypoints for upload */
    bool mission_uploading = false; /*< Are we uploading a mission atm. Needed to know what messages/timeouts to react to*/
    int mission_msg_retries = 0;
    mavlink_msgs::msg::MavloraMissionItemInt last_single_mission_upload;

    /// function prototypes
    void ml_send_mission_count();
    void ml_send_mission_item_int();
    string mission_result_parser(unsigned int result);

    /// Timers
    rclcpp::TimerBase::SharedPtr mission_msg_timer;


    /// Timer callbacks
    void mission_count_timeout();
    void mission_item_timeout();

    /// ROS pubs/subs
    rclcpp::Publisher<std_msgs::msg::UInt16>::SharedPtr mission_count_pub;
    rclcpp::Publisher<mavlink_msgs::msg::MavloraMissionItemInt>::SharedPtr mission_item_pub;
    rclcpp::Publisher<mavlink_msgs::msg::MavloraMissionAck>::SharedPtr mission_ack_pub;

    rclcpp::Subscription<mavlink_msgs::msg::MavloraMissionList>::SharedPtr mission_upload_sub;
    rclcpp::Subscription<std_msgs::msg::UInt16>::SharedPtr mission_request_sub;
    rclcpp::Subscription<mavlink_msgs::msg::MavloraMissionAck>::SharedPtr mission_ack_sub;

    /// ROS callbacks
    void ros_mission_upload_callback(mavlink_msgs::msg::MavloraMissionList::SharedPtr msg);
    void ros_mission_request_callback(std_msgs::msg::UInt16::SharedPtr msg);
    void ros_mission_ack_callback(mavlink_msgs::msg::MavloraMissionAck::SharedPtr msg);
};


#endif
