from setuptools import setup

package_name = 'px4_examples_py'

setup(
    name=package_name,
    version='0.0.0',
    packages=[package_name],
    data_files=[
        ('share/ament_index/resource_index/packages',
         ['resource/' + package_name]),
        ('share/' + package_name, ['package.xml']),
    ],
    install_requires=['setuptools'],
    zip_safe=True,
    maintainer='aumad',
    maintainer_email='aumad@todo.todo',
    description='TODO: Package description',
    license='TODO: License declaration',
    tests_require=['pytest'],
    entry_points={
        'console_scripts': [
            'offboard_node = px4_examples_py.offboard_crtl_node:main',
            'imu_node = px4_examples_py.imu_listener_node:main',
        ],
    },
)
