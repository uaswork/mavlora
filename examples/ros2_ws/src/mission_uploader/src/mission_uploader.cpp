/*
 * Copyright (c) 2022, Frederik Mazur Andersen <fm@mmmi.sdu.dk>/<fm@fmazur.dk>
 *
 * SDU UAS Center, http://sdu.dk/uas, University of Southern Denmark
 * All rights reserved.
 *
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE.txt' at the root of the repo.
 */

#include <mission_uploader.hpp>

mission_uploader::mission_uploader() : Node("mission_uploader") {
  setup_ros_topics();

  // make timers and stop them to avoid firing first time. But are ready for use
  mission_msg_timer = this->create_wall_timer(DEFAULT_TIMEOUT_TIME, std::bind(&mission_uploader::mission_count_timeout, this));
  mission_msg_timer->cancel();
}

void mission_uploader::setup_ros_topics()
{
  auto qos_reliable = rclcpp::QoS(
      rclcpp::QoSInitialization(
          RMW_QOS_POLICY_HISTORY_KEEP_LAST,
          // Depth represents how many messages to store in history when the
          // history policy is KEEP_LAST.
          1
      ));
  qos_reliable.reliable().durability_volatile();

  mission_upload_sub = this->create_subscription<mavlink_msgs::msg::MavloraMissionList>("mavlink_interface/mission/uploader/upload_mission", qos_reliable, std::bind(&mission_uploader::ros_mission_upload_callback, this, _1));
  mission_request_sub = this->create_subscription<std_msgs::msg::UInt16>("mavlink_interface/mission/uploader/request", qos_reliable, std::bind(&mission_uploader::ros_mission_request_callback, this, _1));
  mission_ack_sub = this->create_subscription<mavlink_msgs::msg::MavloraMissionAck>("mavlink_interface/mission/ack", qos_reliable, std::bind(&mission_uploader::ros_mission_ack_callback, this, _1));

  /* Interface publishers */
  mission_count_pub = this->create_publisher<std_msgs::msg::UInt16>("mavlink_interface/mission/uploader/mission_count", qos_reliable);
  mission_item_pub = this->create_publisher<mavlink_msgs::msg::MavloraMissionItemInt>("mavlink_interface/mission/uploader/mission_item", qos_reliable);
  mission_ack_pub = this->create_publisher<mavlink_msgs::msg::MavloraMissionAck>("mavlink_interface/mission/ack", qos_reliable);
}

void mission_uploader::ml_send_mission_count()
{
  RCLCPP_INFO(get_logger(), "%s Sending mission count", log_precursor.c_str());

  // publish msg
  std_msgs::msg::UInt16 msg;
  msg.data = mission_up_count;

  mission_count_pub->publish(msg);

  // start timer
  mission_msg_timer->reset();
}

void mission_uploader::ml_send_mission_item_int()
{
  // send mission
  mavlink_msgs::msg::MavloraMissionItemInt item = missionlist[mission_up_index];
  mission_item_pub->publish(item);

  RCLCPP_INFO(get_logger(), "%s Sent mission item seq: %d", log_precursor.c_str(), item.seq);

  // start timer
  mission_msg_timer->reset();
}

void mission_uploader::mission_count_timeout() {
  // stop timer
  mission_msg_timer->cancel();

  //increment retries
  mission_msg_retries++;

  //check if retries has been exceeded
  if (mission_msg_retries > MISSION_MAX_RETRIES)
  {
    //publish error on ack topic
    mavlink_msgs::msg::MavloraMissionAck msg;
    msg.result = 20;
    msg.result_text = mission_result_parser(20);
    mission_ack_pub->publish(msg);

    //cancel command
    mission_up_index = -1;
    mission_up_count = 0;
    missionlist.clear();
    mission_uploading = false;

    //reset retries
    mission_msg_retries = 0;

    //debug
    RCLCPP_INFO(get_logger(), "%s MAX RETRIES REACHED", log_precursor.c_str());
    return;
  }

  //if timeout triggers, resend count message
  ml_send_mission_count();
}

void mission_uploader::mission_item_timeout() {
  // stop timer
  mission_msg_timer->cancel();

  //increment retries
  mission_msg_retries++;

  //check if retries has been exceeded
  if (mission_msg_retries > MISSION_MAX_RETRIES)
  {
    //publish error on ack topic
    mavlink_msgs::msg::MavloraMissionAck msg;
    msg.result = 20;
    msg.result_text = mission_result_parser(20);
    mission_ack_pub->publish(msg);

    //cancel command
    mission_up_index = -1;
    mission_up_count = 0;
    missionlist.clear();
    mission_uploading = false;

    //reset retries
    mission_msg_retries = 0;

    //debug
    RCLCPP_INFO(get_logger(), "%s MAX RETRIES REACHED", log_precursor.c_str());
    return;
  }

  //if timeout triggers, resend last mission item
  ml_send_mission_item_int();
}

void mission_uploader::ros_mission_upload_callback(const mavlink_msgs::msg::MavloraMissionList::SharedPtr msg) {
  //Set status to uploading mission
  mission_uploading = true;

  //Received new mission on topic, start uploading
  mission_up_count = msg->waypoints.size();
  mission_up_index = -1;
  missionlist = msg->waypoints;

  RCLCPP_INFO(get_logger(), "%s New mission. Length: %d", log_precursor.c_str(), mission_up_count);

  //Send mission count
  ml_send_mission_count();
}

void mission_uploader::ros_mission_request_callback(const std_msgs::msg::UInt16::SharedPtr msg) {

  //if not uploading, something is wrong, but don't react to it
  if (!mission_uploading)
    return;

  //stop timer
  mission_msg_timer->cancel();

  //reset retries
  mission_msg_retries = 0;

  //save request as next msg
  mission_up_index = msg->data;

  //send next item
  ml_send_mission_item_int();

  RCLCPP_INFO(get_logger(), "%s Next mission item asked for: %d", log_precursor.c_str(), mission_up_index);
}

void mission_uploader::ros_mission_ack_callback(const mavlink_msgs::msg::MavloraMissionAck::SharedPtr msg) {
  //stop timer
  mission_msg_timer->cancel();

  RCLCPP_INFO(get_logger(), "%s Mission ACK received with status: %s", log_precursor.c_str(), msg->result_text.c_str());

  //reset retries
  mission_msg_retries = 0;

  //stop uploading flag
  mission_uploading = false;

  //reset vars
  mission_up_index = -1;
  mission_up_count = 0;
  missionlist.clear();
}

string mission_uploader::mission_result_parser(unsigned int result) {
  //find correct error msg based on result
  switch(result)
  {
    case 0:
      return "MAV_MISSION_ACCEPTED";
    case 1:
      return "MAV_MISSION_ERROR";
    case 2:
      return "MAV_MISSION_UNSUPPORTED_FRAME";
    case 3:
      return "MAV_MISSION_UNSUPPORTED";
    case 4:
      return "MAV_MISSION_NO_SPACE";
    case 5:
      return "MAV_MISSION_INVALID";
    case 6:
      return "MAV_MISSION_INVALID_PARAM1";
    case 7:
      return "MAV_MISSION_INVALID_PARAM2";
    case 8:
      return "MAV_MISSION_INVALID_PARAM3";
    case 9:
      return "MAV_MISSION_INVALID_PARAM4";
    case 10:
      return "MAV_MISSION_INVALID_PARAM5_X";
    case 11:
      return "MAV_MISSION_INVALID_PARAM6_Y";
    case 12:
      return "MAV_MISSION_INVALID_PARAM7";
    case 13:
      return "MAV_MISSION_INVALID_SEQUENCE";
    case 14:
      return "MAV_MISSION_DENIED";
    case 20:
      return "MAV_MISSION_MAX_RETRIES"; //Custom error indicating aborting due to max retries with no success
    default:
      return "DIDN'T RECOGNIZE RESULT CODE";
  }
}