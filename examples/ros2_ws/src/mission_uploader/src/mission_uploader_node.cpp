/*
 * Copyright (c) 2022, Frederik Mazur Andersen <fm@mmmi.sdu.dk>/<fm@fmazur.dk>
 *
 * SDU UAS Center, http://sdu.dk/uas, University of Southern Denmark
 * All rights reserved.
 *
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE.txt' at the root of the repo.
 */

/* includes */

#include "rclcpp/rclcpp.hpp"
#include "mission_uploader.hpp"

/******************************* MAIN **************************************/
int main(int argc, char ** argv)
{
  // Launch ROS 2
  rclcpp::init(argc, argv);

  // make a node and spin - Static Executor as we have no runtime changes in creation of timers and subs
  rclcpp::executors::StaticSingleThreadedExecutor executor;
  //rclcpp::executors::MultiThreadedExecutor executor;

  rclcpp::Node::SharedPtr node1 = std::make_shared<mission_uploader>();
  executor.add_node(node1);
  executor.spin();

  //rclcpp::spin(std::make_shared<mission_uploader>());

  // Shutdown routine for ROS2
  rclcpp::shutdown();
  return 0;
}
/***************************************************************************/
