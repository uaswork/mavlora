//
// Created by FrederikMazur on 26/11/18.
//

/*****************************************************************************
** Includes
*****************************************************************************/

#include <ros/ros.h>
#include <ros/network.h>
#include <string>
#include <std_msgs/String.h>
#include <iostream>
#include <string>
#include "../include/lora_ground_control/qnode.hpp"
#include <mavlink_msgs/mavlink_lora_statustext.h>
#include <mavlink_msgs/mavlink_lora_pos.h>
#include <mavlink_msgs/mavlink_lora_heartbeat.h>
#include <mavlink_msgs/mavlink_lora_mission_list.h>
#include <mavlink_msgs/mavlink_lora_mission_item_int.h>
#include <ctime>
#include <include/lora_ground_control/qnode.hpp>


/*****************************************************************************
** Namespaces
*****************************************************************************/
namespace patch
{
    template < typename T > std::string to_string( const T& n )
    {
        std::ostringstream stm ;
        stm << n ;
        return stm.str() ;
    }
}


namespace LoraGroundControl {

/*****************************************************************************
** Implementation
*****************************************************************************/
QNode::QNode(int argc, char** argv ) :
	init_argc(argc),
    init_argv(argv),
    closeDown(false)
	{}

QNode::~QNode() {
    if(ros::isStarted()) {
      ros::shutdown(); // explicitly needed since we use ros::start();
      ros::waitForShutdown();
    }
	wait();
}

bool QNode::init() {
	ros::init(init_argc,init_argv,"lora_ground_control");
	if ( ! ros::master::check() ) {
		return false;
	}
	ros::start(); // explicitly needed since our nodehandle is going out of scope.
	ros::NodeHandle n;
	// Add your ros communications here.

    this->_statustext_sub = n.subscribe<mavlink_msgs::mavlink_lora_statustext>("/mavlink_statustext",1,&QNode::handle_statustext,this);
    this->_status_sub = n.subscribe<mavlink_msgs::mavlink_lora_status>("/mavlink_status",1,&QNode::handle_sys_status,this);
    this->_global_pos_sub = n.subscribe<mavlink_msgs::mavlink_lora_pos>("/mavlink_pos",1,&QNode::handle_global_pos,this);
    this->_gps_raw_sub = n.subscribe<mavlink_msgs::mavlink_lora_gps_raw>("/mavlink_gps_raw",1,&QNode::handle_gps_raw,this);
    this->_heartbeat_sub = n.subscribe<mavlink_msgs::mavlink_lora_heartbeat>("/mavlink_heartbeat_rx",1,&QNode::handle_heartbeat,this);

    this->_heartbeat_nodes_sub = n.subscribe<lora_ground_control::heartbeat_node>("/lora_ground_control/heartbeat_nodes_rx", 10, &QNode::handle_nodes_heartbeat,this);

    qRegisterMetaType<ros::Time*>("ros::Time");


    std::cout << "lora_ground_control started" << std::endl << std::flush;

	start();
	return true;
}

void QNode::run() {
	ros::Rate loop_rate(100);
	int count = 0;
    while ( ros::ok() && !closeDown ) {
        /* check if it is time to send a status message */
        if (status_msg_sent + ros::Duration(0.5) <= ros::Time::now()) {
            send_sys_status();
        }

        //check nodes to see if within interval
        checkNodesTiming();

        ros::spinOnce();
        loop_rate.sleep();
    }
    if(!closeDown){
        std::cout << "Ros shutdown, proceeding to close the gui." << std::endl;
        Q_EMIT rosShutdown(); // used to signal the gui for a shutdown (useful to roslaunch)
    }else{
        std::cout << "Gui shutdown proceeding to close thread" << endl;
    }
}

void QNode::close(){
    closeDown = true;
}

 void QNode::handle_statustext(mavlink_msgs::mavlink_lora_statustext msg){
    //map severity to level
    string sev = "";
    switch(msg.severity)
    {
        case 0:
            sev = "EMERGENCY";
            break;
        case 1:
            sev = "ALERT";
            break;
        case 2:
            sev = "CRITICAL";
            break;
        case 3:
            sev = "ERROR";
            break;
        case 4:
            sev = "WARNING";
            break;
        case 5:
            sev = "NOTICE";
            break;
        case 6:
            sev = "INFO";
            break;
        case 7:
            sev = "DEBUG";
            break;
        default:
            break;
    }

     string text =  "[" + sev + "][" + patch::to_string(ros::Time::now().sec) + "]: " + msg.text+"\n";
     Q_EMIT sig_statustext(msg.severity, text.c_str());
 }

    void QNode::handle_global_pos(mavlink_msgs::mavlink_lora_pos msg) {
        // round elements to doubles for proper showing
        //Emit lat, lon, alt
        Q_EMIT sig_position(msg.lat, msg.lon, msg.relative_alt, msg.alt, msg.heading);
    }

    void QNode::handle_heartbeat(mavlink_msgs::mavlink_lora_heartbeat msg) {
        //decode custom_mode
        QString flight_mode = decode_custom_mode(msg.custom_mode);
        bool armed = decode_armed(msg.base_mode);

        //emit signals
        Q_EMIT sig_armed(armed);
        Q_EMIT sig_flightmode(flight_mode);
    }

    void QNode::handle_sys_status(mavlink_msgs::mavlink_lora_status msg) {
        //update local vars instead, and send this once every second to ui. Only way to see if last heard changes
        this->batt_volt = double(msg.batt_volt)/1000;
        this->batt_remaining = msg.batt_remaining;
        this->cpu_load = double(msg.cpu_load)/10;
        this->sys_last_heard = msg.last_heard;
        this->sys_last_heard_sys_status = msg.last_heard_sys_status;
        this->msg_dropped_gcs = msg.msg_dropped_gcs;
        this->msg_dropped_uas = msg.msg_dropped_uas;
    }

    void QNode::send_sys_status() {
        //Update GUI
        Q_EMIT sig_sys_status(this->batt_volt, this->batt_remaining, this->cpu_load, this->sys_last_heard, this->sys_last_heard_sys_status, this->msg_dropped_gcs, this->msg_dropped_uas);

        //update timestamp
        this->status_msg_sent = ros::Time::now();
    }

    void QNode::handle_nodes_heartbeat(lora_ground_control::heartbeat_node msg)
    {
        //if first time we hear from this node, make new object and expect to hear from it.
        addOrUpdateNodes(msg);
    }

    QString QNode::decode_custom_mode(unsigned long custom_mode) {
        //cast custommode to int
        unsigned int sub_mode = (custom_mode >> 24);
        unsigned int base_mode = (custom_mode >> 16) & 0xFF;

        QString sub_text = "";
        QString base_text = "";

        switch(sub_mode) {
            case 1:
                sub_text = "READY";
                break;
            case 2:
                sub_text = "TAKEOFF";
                break;
            case 3:
                sub_text = "LOITER";
                break;
            case 4:
                sub_text = "MISSION";
                break;
            case 5:
                sub_text = "RTL";
                break;
            case 6:
                sub_text = "LAND";
                break;
            case 7:
                sub_text = "RTGS";
                break;
            case 8:
                sub_text = "FOLLOW_TARGET";
                break;
            case 9:
                sub_text = "PRECLAND";
                break;
            default:
                break;
        }


        switch(base_mode) {
            case 1:
                base_text = "MANUAL";
                break;
            case 2:
                base_text = "ALTITUDE CONTROL";
                break;
            case 3:
                base_text = "POSITION CONTROL";
                break;
            case 4:
                base_text = "AUTO";
                break;
            case 5:
                base_text = "ARCO";
                break;
            case 6:
                base_text = "OFFBOARD";
                break;
            case 7:
                base_text = "STABILIZED";
                break;
            case 8:
                base_text = "RATTITUDE";
                break;
            default:
                break;
        }

        if (sub_text.isEmpty())
            return QString(base_text);
        else
            return QString(base_text + " - " + sub_text);
    }

    bool QNode::decode_armed(unsigned int base_mode) {
        //decode to bool
        bool armed = bool(base_mode & 0x80);

        return armed;
    }

    void QNode::addOrUpdateNodes(lora_ground_control::heartbeat_node &node) {
        //check if exists. Requires both name and id to be the same. Maybe only check id? Name and ID combo does allow
        //almost always having unique nodes, even if they somehow choose same id
        auto it = std::find_if(_nodes.begin(), _nodes.end(), [&](NodeItem const& obj){
            return (obj.name() == node.name.c_str()) && (obj.id() == node.id);
        });

        if(it != _nodes.end()){
            //update node
            it->updateItem(node.main_status.c_str(), node.current_task.c_str(), node.has_error);
        }
        else
        {
            //create new node
            NodeItem newNode(node.id, node.name.c_str(), node.expected_interval, node.main_status.c_str(), node.current_task.c_str());
            //add to list
            _nodes << newNode;
        }

        //EMIT
        Q_EMIT sig_update_node_monitoring(_nodes);
    }

    void QNode::checkNodesTiming() {
        //Check nodes heartbeat
        for( auto &node : _nodes) {

            //check if expected time has been crossed
            if (node.lastReceived() + ros::Duration(node.expected_interval()) <= ros::Time::now())
            {
                //if it isn't already in timeout error, throw timeout error, TODO change to hardcoded string in one place only
                //Limiting amount of signals when no changes has happened.
                if (!node.hasError() || node.main_status() != "TIMEOUT")
                {
                    //throw error, not heard within expected period
                    node.timeoutError();

                    //Emit changes
                    Q_EMIT sig_update_node_monitoring(_nodes);
                }
            }
        }
    }

    void QNode::handle_gps_raw(mavlink_msgs::mavlink_lora_gps_raw msg) {
        //send signals with information needed

        //GPS FIX
        Q_EMIT sig_gps_fix(msg.fix_type);

        //DOPS
        Q_EMIT sig_gps_dop(msg.eph, msg.epv);

        //GROUNDSPEED
        Q_EMIT sig_groundspeed(msg.vel);
    }

}  // namespace LoraGroundControl
