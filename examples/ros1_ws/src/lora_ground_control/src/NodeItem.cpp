//
// Created by FrederikMazur on 26/11/18.
//

#include <include/lora_ground_control/NodeItem.hpp>
#include <QtCore/QArgument>

#include "include/lora_ground_control/NodeItem.hpp"


NodeItem::NodeItem(unsigned int id, QString name, float expected_interval, QString main_status, QString current_task) {
    _id = id;
    _name = name;
    _expected_interval = expected_interval;
    _main_status = main_status;
    _current_task = current_task;
    _lastReceived = ros::Time::now();
}

void NodeItem::updateItem(QString main_status, QString current_task, bool hasError) {
    //Update values
    _main_status = main_status;
    _current_task = current_task;

    //update error flag. As we received msg from node, then only node knows if it has an error
    _hasError = hasError;

    //update timestamp of received heartbeat
    _lastReceived = ros::Time::now();
}

unsigned int NodeItem::id() const {
    return _id;
}

QString NodeItem::name() const {
    return _name;
}

float NodeItem::expected_interval() const {
    return _expected_interval;
}

ros::Time NodeItem::lastReceived() const {
    return _lastReceived;
}

void NodeItem::timeoutError() {
    //Set status to error, and flag to error
    _main_status = "TIMEOUT";
    _hasError = true;
}

bool NodeItem::hasError() const {
    return _hasError;
}

QString NodeItem::main_status() const {
    return _main_status;
}

QString NodeItem::current_task() const {
    return _current_task;
}

NodeItem::~NodeItem() {

}

NodeItem::NodeItem() {

}
