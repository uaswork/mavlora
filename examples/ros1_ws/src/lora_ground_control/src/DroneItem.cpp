//
// Created by Frederik Mazur on 26/11/18.
//

#include "include/lora_ground_control/DroneItem.hpp"

QString DroneItem::name() const
{
    return _name;
}

void DroneItem::setName(const QString &name)
{
    _name = name;
}

QGeoCoordinate DroneItem::getDronePos() const
{
    return _dronePos;
}

void DroneItem::setDronePos(const QGeoCoordinate &pos)
{

    if(_dronePos.isValid())
        appendHistory(_dronePos);
    _dronePos = pos;
}

void DroneItem::appendHistory(const QGeoCoordinate &value)
{
    _history<< value;
}

QList<QGeoCoordinate> DroneItem::getHistory() const{
    return _history;
}

QColor DroneItem::getColor() const
{
    return _color;
}

void DroneItem::setColor(const QColor &color)
{
    _color = color;
}

int DroneItem::getAngle() const
{
    return _angle;
}

void DroneItem::setAngle(int angle)
{
    _angle = angle;
}