//
// Created by FrederikMazur on 17/11/18.
//

#include "include/lora_ground_control/MapController.hpp"
#include <QtPositioning>
#include <include/lora_ground_control/MapController.hpp>


MapController::MapController() {

    //set default position at Tek building
    QGeoCoordinate default_pos(55.367232, 10.430229);
    this->setViewCenter(default_pos);

    //Init DroneListModel
    _droneListModel = new DroneListModel();
}

void MapController::bindModelToView(QQmlContext *context) {
    // bind mapcontroller. might not be needed
    context->setContextProperty("mapController", this);

    // bind drone model, what we actually use
    _droneListModel->register_objects("droneModel", context);
}

void MapController::update_position(double lat, double lon, double alt_rel, double alt_msl, int heading) {
    QGeoCoordinate pos(lat, lon);
    QString name = "drone1"; //TODO change so name comes from node, to support multiple drones

    //If gps is too low, don't update pos, or just clear path when gps is good enough to be counted on. TODO change to handle multiple systems?
    if (_gps_fix)
        if (!_droneListModel->addOrUpdateDrone(pos, heading, name))
            ROS_INFO_STREAM("ERROR: adding/updating new drone to map");


    //    this->setViewPosition(pos);
//    this->setViewCenter(pos);

    //set heading
//    this->_droneHeading = heading;
}

void MapController::setViewCenter(const QGeoCoordinate &pos) {
    //check if first time we get pos from drone, then center view, TODO or if setting of always center on drone is set
    if (!_firstDronePosReceived) {
        _firstDronePosReceived = true;
        this->_centerViewPos = pos;
    }
}

void MapController::set_gps_fix(unsigned int gps_fix) {
    //if gps fix is >= 3, then we got 3D fix, or better and will show it on the map.
    if (gps_fix >= 3)
        _gps_fix = true;
}

MapController::~MapController() = default;

