//
// Created by FrederikMazur on 26/11/18.
//
/*****************************************************************************
** Includes
*****************************************************************************/

#include <QtGui>
#include <QMessageBox>
#include <iostream>
#include <QtQuickWidgets/QQuickWidget>
#include <QQmlContext>
#include <include/lora_ground_control/MapController.hpp>
#include <include/lora_ground_control/main_window.hpp>
#include <include/lora_ground_control/qnode.hpp>

#include "../include/lora_ground_control/main_window.hpp"

/*****************************************************************************
** Namespaces
*****************************************************************************/

namespace LoraGroundControl {

using namespace Qt;

/*****************************************************************************
** Implementation [MainWindow]
*****************************************************************************/

MainWindow::MainWindow(int argc, char** argv, QWidget *parent)
	: QMainWindow(parent)
	, qnode(argc,argv)
{
	ui.setupUi(this); // Calling this incidentally connects all ui's triggers to on_...() callbacks in this class.
	setWindowIcon(QIcon(":/images/icon_lora.png"));

    //############### Element initialization #####################
//    ui.statustext->setStyleSheet("background-color: black");
    // ui.label_droneHandler->setWordWrap(true);


    // bind map and controller to map
    MapController* mapController = new MapController();

    QQuickView *view = new QQuickView();
    QWidget *container = QWidget::createWindowContainer(view, parent);
    container->setMinimumSize(300, 300);
    container->setMaximumSize(1200, 800);
    container->setSizePolicy(QSizePolicy::Preferred, QSizePolicy::Preferred);
    container->setAccessibleName("map_plugin");

    //bind controller
    mapController->bindModelToView(view->rootContext());

    //set source
    view->setSource(QUrl("qrc:/map/osm_map.qml")); // Fetch this url by right clicking on your resource file.
    ui.map_layout->addWidget(container);


    //#################### REGISTER CUSTOM TYPES ########################
    qRegisterMetaType< QList<NodeItem> >();

    //############### Manualy connected SLOTS and SIGNALS ###############
    QObject::connect(&qnode, SIGNAL(rosShutdown()), this, SLOT(close()));

    // Heartbeat signals
    QObject::connect(&qnode, &QNode::sig_flightmode, this, &MainWindow::set_flightmode);
    QObject::connect(&qnode, &QNode::sig_armed, this, &MainWindow::set_arm_status);

    // global pos signals
    QObject::connect(&qnode, &QNode::sig_position, this, &MainWindow::set_position);
    QObject::connect(&qnode, &QNode::sig_position, mapController, &MapController::update_position);

    // gps raw
    QObject::connect(&qnode, &QNode::sig_gps_fix, mapController, &MapController::set_gps_fix);
    QObject::connect(&qnode, &QNode::sig_gps_fix, this, &MainWindow::set_gps_fix);
    QObject::connect(&qnode, &QNode::sig_groundspeed, this, &MainWindow::set_groundspeed);
    QObject::connect(&qnode, &QNode::sig_gps_dop, this, &MainWindow::set_gps_dop);


    // statustext
    QObject::connect(&qnode, &QNode::sig_statustext, this, &MainWindow::set_statustext);

    // Sys status
    QObject::connect(&qnode, &QNode::sig_sys_status, this, &MainWindow::set_sys_status);

    //Node monitoring
    QObject::connect(&qnode, &QNode::sig_update_node_monitoring, this, &MainWindow::set_node_monitoring);

    qnode.init();
}

MainWindow::~MainWindow() {}

/*****************************************************************************
** Implementation [Slots]
*****************************************************************************/


/*****************************************************************************
** Implemenation [Slots][manually connected]
*****************************************************************************/

 void MainWindow::set_arm_status(bool isArmed){
     if(isArmed){
         ui.label_armed->setText(QString("<p align=\"center\"><span style=' font-size:12pt; font-weight:600; color:#bf0f26;'>ARMED</span></p>"));
     }else{
         ui.label_armed->setText(QString("<p align=\"center\"><span style=' font-size:12pt; font-weight:600; color:#6bc153;'>DISARMED</span></p>"));
     }
 }

 void MainWindow::set_position(double lat, double lon, double alt_rel, double alt_msl, double heading) {
     QString text;
     QLocale obj;

     //POS
     text.push_back(obj.toString(lat,'f',6));
     text.push_back(QString("°"));
     ui.label_latitude->setText(text);
     text.clear();

     text.push_back(obj.toString(lon,'f',6));
     text.push_back(QString("°"));
     ui.label_longitude->setText(text);
     text.clear();

     //alt
     text.push_back(obj.toString(alt_rel, 'f', 1));
     text.push_back(QString("m"));
     ui.label_height_rel->setText(text);
     text.clear();

     text.push_back(obj.toString(alt_msl, 'f', 1));
     text.push_back(QString("m"));
     ui.label_height_amsl->setText(text);
     text.clear();

     // HEADING
     text.push_back(obj.toString(heading,'f',1));
     text.push_back(QString("°"));
     ui.label_heading->setText(text);


 }

 void MainWindow::set_groundspeed(double speed)
 {
     QString text;
     QLocale obj;

     //ground speed
     text.push_back(obj.toString(speed,'f',1));
     text.push_back(QString("m/s"));
     ui.label_ground_speed->setText(text);
 }

 void MainWindow::set_gps_dop(unsigned int hdop, unsigned int vdop) {
     QLocale obj;

     //HDOP
     ui.label_hdop->setText(obj.toString(hdop));

     //VDOP
     ui.label_vdop->setText(obj.toString(vdop));
 }

 void MainWindow::set_gps_fix(unsigned int gps_fix) {
    //Convert gps fix to string
    QString fix_text = "";
    switch(gps_fix)
    {
        case 0:
            fix_text = "NO GPS";
            break;
        case 1:
            fix_text = "NO FIX";
            break;
        case 2:
            fix_text = "2D FIX";
            break;
        case 3:
            fix_text = "3D FIX";
            break;
        case 4:
            fix_text = "DGPS";
            break;
        case 5:
            fix_text = "RTK FLOAT";
            break;
        case 6:
            fix_text = "RTK FIXED";
            break;
        case 7:
            fix_text = "STATIC";
            break;
        case 8:
            fix_text = "PPP (3D)";
            break;
        default:
            fix_text = "N/A";
    }

    //set ui
    ui.label_gps_fix->setText(fix_text);
 }

 void MainWindow::set_flightmode(QString flightmode) {
    //add formatting to string
    ui.label_flightmode->setText(QString("<p align=\"center\"><span style=\" font-size:12pt; font-weight:600; color:#f4aa42;\">" + flightmode + "</span></p>"));
 }

 void MainWindow::set_statustext(int severity, QString text){

     //set colour based on severity
     if(severity == 5){
         ui.statustext->setTextColor(QColor(255,255,0));
     }else if(severity == 4){
         ui.statustext->setTextColor(QColor(255,200,0));
     }else if(severity == 3){
         ui.statustext->setTextColor(QColor(255,150,0));
     }else if(severity == 2){
         ui.statustext->setTextColor(QColor(255,100,0));
     }else if(severity == 1){
         ui.statustext->setTextColor(QColor(255,50,0));
     }else if(severity == 0){
         ui.statustext->setTextColor(QColor(255,0,0));
     }else{
         ui.statustext->setTextColor(QColor(0,0,0));
     }

     // Insert text at bottom
     bool atBtn = false;
     if(ui.statustext->verticalScrollBar()->value() == ui.statustext->verticalScrollBar()->maximum())
         atBtn = true;
     ui.statustext->insertPlainText(text);
     if(atBtn)
         ui.statustext->verticalScrollBar()->setValue(ui.statustext->verticalScrollBar()->maximum());
 }

    void MainWindow::set_sys_status(double batt, int batt_remaining, double cpu_load, ros::Time last_heard, ros::Time last_sys_status, unsigned int msg_drop_gcs, unsigned int msg_drop_uav) {
        QString text;
        QLocale obj;

        //Battery
        text.push_back(obj.toString(batt,'f',2));
        text.push_back(QString("V"));
        ui.label_battery->setText(text);
        text.clear();

        //batt remaining
        text.push_back(obj.toString(batt_remaining));
        text.push_back(QString("%"));
        ui.label_battery_remaining->setText(text);
        text.clear();

        //last heard
        ros::Time now = ros::Time::now();
        int t = (now - last_heard).toSec();

        text.push_back(obj.toString(t));
        text.push_back(QString("s"));
        ui.label_last_heard->setText(text);
        text.clear();

        //last heard sys status
        t = (now - last_sys_status).toSec();
        text.push_back(obj.toString(t));
        text.push_back(QString("s"));
        ui.label_last_sys->setText(text);
        text.clear();

        //cpu time
        text.push_back(obj.toString(cpu_load, 'f', 1));
        text.push_back(QString("%"));
        ui.label_cpu_load->setText(text);
        text.clear();

        //msg_dropped
        text.push_back(obj.toString(msg_drop_gcs));
        ui.label_msg_drop_gcs->setText(text);
        text.clear();
        text.push_back(obj.toString(msg_drop_uav));
        ui.label_msg_drop_uav->setText(text);
    }

    void MainWindow::set_node_monitoring(QList<NodeItem> nodes) {
        //TODO dynamically make layout as a horizontal list. color code based on error or not.
        //TODO showing: name, main_status, current_task
        const QString style_error = "background-color: rgb(204, 0, 0)";
        const QString style_ok = "background-color: rgb(78, 154, 6)";

        for (auto &node : nodes) {
            //update or add
            //find if already exists child with that name + id
            QString nodeID = node.name() + node.id();

            QList<QLabel *> labels = ui.node_group->findChildren<QLabel*>(nodeID);

            //If we have all three children
            if (labels.size() == 3)
            {
                //update labels
                QString name = "<html><head/><body><p><span style=\" font-weight:600; color:#ffffff;\">" + node.main_status() + "</span></p></body></html>";
                labels[1]->setText(name);
                name = "<html><head/><body><p><span style=\" font-weight:600; color:#ffffff;\">" + node.current_task() + "</span></p></body></html>";
                labels[2]->setText(name);

                labels[0]->setStyleSheet(node.hasError() ? style_error : style_ok);
                labels[1]->setStyleSheet(node.hasError() ? style_error : style_ok);
                labels[2]->setStyleSheet(node.hasError() ? style_error : style_ok);
            }
            else if (labels.empty())
                //create new label and append
                create_new_node_labels(&node);
            else
                //missed a node somehow, completely redraw nodes TODO
                int i = 0;
        }
    }

    void MainWindow::create_new_node_labels(NodeItem *node) {
        //create labels and append
        const QString style_error = "background-color: rgb(204, 0, 0)";
        const QString style_ok = "background-color: rgb(78, 154, 6)";

        //NAME
        QLabel *label =  new QLabel();
        QString name = "<html><head/><body><p><span style=\" font-weight:600; color:#ffffff;\">" + node->name() + ":" + "</span></p></body></html>";
        label->setText(name);
        label->setStyleSheet(node->hasError() ? style_error : style_ok);
        label->setObjectName(QString(node->name() + node->id()));
        ui.nodes_name->addWidget(label);

        //MAIN STATUS
        label =  new QLabel();
        name = "<html><head/><body><p><span style=\" font-weight:600; color:#ffffff;\">" + node->main_status() + "</span></p></body></html>";
        label->setText(name);
        label->setStyleSheet(node->hasError() ? style_error : style_ok);
        label->setObjectName(QString(node->name() + node->id()));
        ui.nodes_main_status->addWidget(label);

        //CURRENT TASK
        label =  new QLabel();
        name = "<html><head/><body><p><span style=\" font-weight:600; color:#ffffff;\">" + node->current_task() + "</span></p></body></html>";
        label->setText(name);
        label->setStyleSheet(node->hasError() ? style_error : style_ok);
        label->setObjectName(QString(node->name() + node->id()));
        ui.nodes_current_task->addWidget(label);
    }

/*****************************************************************************
** Implementation [Menu]
*****************************************************************************/


/*****************************************************************************
** Implementation [Configuration]
*****************************************************************************/

    void MainWindow::closeEvent(QCloseEvent *event)
    {
        QMainWindow::closeEvent(event);
        qnode.close();
    }
}

