//
// Created by FrederikMazur on 26/11/18.
//

#include "include/lora_ground_control/DroneListModel.hpp"
#include <QQmlContext>

DroneListModel::DroneListModel(QObject *parent):QAbstractListModel(parent)
{}

void DroneListModel::register_objects(const QString &droneName, QQmlContext *context)
{
    context->setContextProperty(droneName, this);
}

bool DroneListModel::addOrUpdateDrone(QGeoCoordinate coord, int angle, const QString &name)
{
    auto it = std::find_if(_drones.begin(), _drones.end(), [&](DroneItem const& obj){
        return obj.name() == name;
    });

    if(it != _drones.end()){
        //update
        int row = it - _drones.begin();

        QModelIndex ix = index(row);
//        QGeoCoordinate c = ix.data(PositionRole).value<QGeoCoordinate>();
//        int a = ix.data(AngleRole).toInt();
        Data data{coord, angle};
        bool result = setData(ix, QVariant::fromValue(data), PositionRole);
        return result;
    }
    else
    {
        //Add Drone
        createDrone(coord, QColor("red"), name); //TODO change to support multiple drones?
    }
    //failed
    return false;
}

bool DroneListModel::createDrone(QGeoCoordinate coord, const QColor &color, const QString &name)
{
    beginInsertRows(QModelIndex(), rowCount(), rowCount());

    DroneItem drone;
    drone.setName(name);
    drone.setDronePos(coord);
    drone.setColor(color);

    _drones<< drone;
    endInsertRows();

    return true;
}


int DroneListModel::rowCount(const QModelIndex &parent) const
{
    Q_UNUSED(parent)
    return _drones.count();
}

QVariant DroneListModel::data(const QModelIndex &index, int role) const
{
    if (!index.isValid())
        return QVariant();

    if(index.row() >= 0 && index.row()<rowCount()){
        const DroneItem &drone = _drones[index.row()];

        if(role==NameRole)
            return drone.name();
        else if (role == PositionRole)
            return  QVariant::fromValue(drone.getDronePos());
        else if(role == HistoryRole){
            QVariantList history_list;
            QList<QGeoCoordinate> coords = drone.getHistory();
            for(const QGeoCoordinate & coord: coords){
                history_list<<QVariant::fromValue(coord);
            }
            history_list<< QVariant::fromValue(drone.getDronePos());
            return history_list;
        }
        else if(role == ColorRole){
            return drone.getColor();
        }
        else if (role == AngleRole) {
            return drone.getAngle();
        }
    }
    return QVariant();
}


QHash<int, QByteArray> DroneListModel::roleNames() const
{
    QHash<int, QByteArray> roles;
    roles[NameRole] = "nameData";
    roles[PositionRole]= "positionData";
    roles[HistoryRole] = "historyData";
    roles[AngleRole] = "angleData";
    roles[ColorRole] = "colorData";
    return roles;
}

bool DroneListModel::setData(const QModelIndex &index, const QVariant &value, int role)
{
    if (!index.isValid())
        return false;
    if(index.row() >= 0 && index.row()<rowCount()){
        if (role == PositionRole) {
            const Data & data = value.value<Data>();

            QGeoCoordinate new_pos(data.coord);
            _drones[index.row()].setDronePos(new_pos);
            _drones[index.row()].setAngle(data.angle);
            emit dataChanged(index, index, QVector<int>{PositionRole});
            return true;
        }
    }
    return false;
}