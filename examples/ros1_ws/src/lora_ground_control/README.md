# Lora Ground Control
LoraGroundControl is a gui application that shows information about the drone and its system. Current state it is 
purely a passive tool displaying information, and does not have the capabilities to send commands to the drone. 
Made to be used together with Mavlink_Lora and thus long distance telemetry. LoraGroundControl also supports monitoring the state
of local nodes in the ros system. Simply publish a heartbeat from your other nodes to the right topic and it will monitor and alert if a node errors or timeouts. 

![LoraGroundControl](/uploads/950ff32f516134b57bc15675daac04d5/Screenshot_from_2018-12-06_10-05-57.png)


##Requirements
* Ros-Melodic (Built and tested with Melodic on 18.04, might work on kinetic, but not tested)
* Qt5 with Location and Position package. (Used to show the map)
* Mavlink_Lora long distance communication ros package

#### Installing Requirements
Install ROS and Qt5 from their official website and install guides. If installing bare-bones Qt, make sure qt packages ``location`` and 
``position`` is installed, or install them with: 
```bash
sudo apt install qtlocation5-dev qtpositioning5-dev
```

##Install
1. Git clone repository
2. Put root folder into your catkin repository  
Hint: symlink the folder: ``ln -s /full/path/to/package/ /full/path/to/catkin_workspace/src/ ``)

3. run ``catkin_make``

##Add nodes to heartbeat monitoring
1. Import the ``heartbeat_node.msg`` from LoraGroundControl to your node with the format.
2. Ensure to publish the heartbeat from your node to topic: ``/lora_ground_control/heartbeat_nodes_rx``
3. After first heartbeat is received, the monitor will expect to hear from that node within expected interval in that lifetime of
the monitor.   
**Note:** Restarting LoraGroundControl, means all nodes need to give a single heartbeat again to get recognized and monitored. 

### Heartbeat format
```cpp
uint8 id //Id of the node. Chose one to give the node. The monitor uses id + name to recognize nodes
string name //name of node
float32 expected_interval //expected interval to receive heartbeats from the node. given in seconds float
string main_status //main status, like "Running, Error, Rebooting, Crashed and so on"
string current_task //current task the node is performing. To monitor what the nodes is doing
bool has_error //flag to set if node has detected error itself, but is still sending heartbeats. 
```

## Run
Make sure the catkin setup.bash file is sourced, together with ros.
```bash
source <catkin_workspace>/devel/setup.bash
source /opt/ros/<ros-distro>/setup.bash
```
Run with the launch file:
```bash
roslaunch lora_ground_control groundcontrol.launch
```