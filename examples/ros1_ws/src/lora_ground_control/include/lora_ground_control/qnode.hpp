/**
 * @file /include/drone_monitor/qnode.hpp
 *
 * @brief Communications central!
 *
 * @date February 2011
 **/
/*****************************************************************************
** Ifdefs
*****************************************************************************/

#ifndef LoraGroundControl_QNODE_HPP_
#define LoraGroundControl_QNODE_HPP_

/*****************************************************************************
** Includes
*****************************************************************************/

// To workaround boost/qt4 problems that won't be bugfixed. Refer to
//    https://bugreports.qt.io/browse/QTBUG-22829
#ifndef Q_MOC_RUN
  #include <ros/ros.h>
  #include <mavlink_msgs/mavlink_lora_statustext.h>
  #include <mavlink_msgs/mavlink_lora_pos.h>
  #include <mavlink_msgs/mavlink_lora_heartbeat.h>
  #include <mavlink_msgs/mavlink_lora_mission_list.h>
  #include <mavlink_msgs/mavlink_lora_mission_item_int.h>
  #include <mavlink_msgs/mavlink_lora_status.h>
  #include <mavlink_msgs/mavlink_lora_gps_raw.h>
  #include <lora_ground_control/heartbeat_node.h>
#endif

#include <string>
#include <QThread>
#include <QString>
#include <map>
#include "NodeItem.hpp"
Q_DECLARE_METATYPE(ros::Time);
/*****************************************************************************
** Namespaces
*****************************************************************************/
using namespace std;
namespace LoraGroundControl {

/*****************************************************************************
** Class
*****************************************************************************/
class QNode : public QThread {
Q_OBJECT
public:
    QNode(int argc, char** argv );
    virtual ~QNode();
    bool init();
    void run();
    void close();


Q_SIGNALS:
	// void loggingUpdated();
    void rosShutdown();

	//heartbeat signals
	void sig_armed(bool armed);
    void sig_flightmode(QString text);

    //global pos signals
    void sig_position(double lat, double lon, double alt_rel, double alt_msl, double heading);
    void sig_gps_fix(unsigned int gps_fix);
    void sig_groundspeed(double vel);
    void sig_gps_dop(unsigned int hdop, unsigned int vdop);

    //Sys Status TODO add more batt monitoring and cpu load
    void sig_sys_status(double batt, int batt_remaining, double cpu_load, ros::Time last_heard, ros::Time last_sys_status, unsigned int msg_drop_gcs, unsigned int msg_drop_uav);

    //status text
    void sig_statustext(int severity, QString text);

    //Node Monitoring
    void sig_update_node_monitoring(QList<NodeItem> nodes);

private:
	int init_argc;
	char** init_argv;

	ros::Subscriber _statustext_sub;
	ros::Subscriber _status_sub;
	ros::Subscriber _global_pos_sub;
	ros::Subscriber _gps_raw_sub;
	ros::Subscriber _heartbeat_sub;
	ros::Subscriber _heartbeat_nodes_sub;

	void handle_statustext(mavlink_msgs::mavlink_lora_statustext msg);
	void handle_global_pos(mavlink_msgs::mavlink_lora_pos msg);
	void handle_gps_raw(mavlink_msgs::mavlink_lora_gps_raw msg);
	void handle_heartbeat(mavlink_msgs::mavlink_lora_heartbeat msg);
	void handle_sys_status(mavlink_msgs::mavlink_lora_status msg);
	void send_sys_status();

	//Nodes heartbeat
	void handle_nodes_heartbeat(lora_ground_control::heartbeat_node msg);

	// helpers
	QString decode_custom_mode(unsigned long custom_mode);
	bool decode_armed(unsigned int base_mode);

	//Sys status
	double batt_volt = 0.0;
	int batt_remaining = 0;
	double cpu_load = 0.0;
	ros::Time sys_last_heard;
	ros::Time sys_last_heard_sys_status;
	unsigned int msg_dropped_gcs;
	unsigned int msg_dropped_uas;
	ros::Time status_msg_sent;

	//Node Monitoring
	void addOrUpdateNodes(lora_ground_control::heartbeat_node &node);
	void checkNodesTiming();
	QList<NodeItem> _nodes;

	bool closeDown;
};

}  // namespace LoraGroundControl

#endif /* LoraGroundControl_QNODE_HPP_ */
