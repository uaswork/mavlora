//
// Created by crow on 17/11/18.
//

#ifndef LORA_GROUND_CONTROL_MAPCONTROLLER_H
#define LORA_GROUND_CONTROL_MAPCONTROLLER_H

#include <QObject>
#include <QtCore/QEasingCurve>
#include <QGeoCoordinate>
#include <QtPositioning>
#include <ros/ros.h>
#include <QQmlContext>
#include "DroneListModel.hpp"

class MapController: public QObject {
    Q_OBJECT
//    Q_PROPERTY(QGeoCoordinate dronePosition READ dronePosition NOTIFY dronePositionChanged)
    Q_PROPERTY(QGeoCoordinate centerViewPos READ centerViewPos NOTIFY centerViewPosChanged)
//    Q_PROPERTY(double droneHeading READ droneHeading NOTIFY droneHeadingChanged)
public:
    MapController();
    void setViewPosition(const QGeoCoordinate &c);
    void setViewCenter(const QGeoCoordinate &c);

    //binding
    void bindModelToView(QQmlContext * context);

    //drone pos
    QGeoCoordinate dronePosition(void) {return _currentPosition;}
    QGeoCoordinate centerViewPos(void) {return _centerViewPos;}
    double droneHeading(void) {return _droneHeading;}

    //update when we got gps fix
    void set_gps_fix(unsigned int gps_fix);

    ~MapController() override;

Q_SIGNALS:
    void dronePositionChanged();
    void centerViewPosChanged();
    void droneHeadingChanged();

public Q_SLOTS:
    void update_position(double lat, double lon, double alt_rel, double alt_msl, int heading);

private:
    QGeoCoordinate _currentPosition;
    QGeoCoordinate _centerViewPos;
    double _droneHeading;
    bool _firstDronePosReceived = false;
    DroneListModel* _droneListModel;
    bool _gps_fix = false;
};


#endif //LORA_GROUND_CONTROL_MAPCONTROLLER_H
