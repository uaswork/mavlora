/**
 * @file /include/drone_monitor/main_window.hpp
 *
 * @brief Qt based gui for drone_monitor.
 *
 * @date November 2010
 **/
#ifndef LoraGroundControl_MAIN_WINDOW_H
#define LoraGroundControl_MAIN_WINDOW_H

/*****************************************************************************
** Includes
*****************************************************************************/

#include <QtWidgets>
#include "qnode.hpp"
#include "ui_main_window.h"
#include "NodeItem.hpp"
#include <QtQuick/QQuickView>

/*****************************************************************************
** Namespace
*****************************************************************************/

namespace LoraGroundControl {



/*****************************************************************************
** Interface [MainWindow]
*****************************************************************************/
/**
 * @brief Qt central, all operations relating to the view part here.
 */

class MainWindow : public QMainWindow {
Q_OBJECT

public:
	MainWindow(int argc, char** argv, QWidget *parent = 0);
	~MainWindow();

	void closeEvent(QCloseEvent *event); // Overloaded function

public Q_SLOTS:
	/******************************************
	** Auto-connections (connectSlotsByName())
	*******************************************/


    /******************************************
    ** Manual connections
    *******************************************/

    void set_position(double lat, double lon, double alt_rel, double alt_msl, double heading);
    void set_groundspeed(double vel);
    void set_gps_dop(unsigned int hdop, unsigned int vdop);
    void set_gps_fix(unsigned int gps_fix);

    void set_sys_status(double batt, int batt_remaining, double cpu_load, ros::Time last_heard, ros::Time last_sys_status, unsigned int msg_drop_gcs, unsigned int msg_drop_uav);

	void set_flightmode(QString text);
	void set_arm_status(bool armed);
	void set_statustext(int severity, QString text);

	//Node monitoring handling
	void set_node_monitoring(QList<NodeItem> nodes);
	void create_new_node_labels(NodeItem* node);

private:
	Ui::MainWindowDesign ui;
	QNode qnode;
};

}  // namespace LoraGroundControl

#endif // drone_monitor_MAIN_WINDOW_H
