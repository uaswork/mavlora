# MAVLORA
A library made for long-range communication with a drone using mavlink.

## Build
1. Install dependencies
    ```
    sudo apt-get update -y
    sudo apt-get install cmake build-essential git -y
    ```
2.  Update submodules
    ```
    git submodule update --init --recursive
    ```
3.  Build the C++ library by calling
    ```
    cmake -DCMAKE_BUILD_TYPE=Debug -DBUILD_SHARED_LIBS=ON -Bbuild/default -H.
    cmake --build build/default
    ``` 
    This build debug library. For release run:
    ```
    cmake -DCMAKE_BUILD_TYPE=Release -DBUILD_SHARED_LIBS=ON -Bbuild/default -H.
    cmake --build build/default
    ```

## Install
### **system-wide install**
``` 
# sudo is required to install to system directories!
sudo cmake --build build/default --target install
# First installation only - update linker cache
sudo ldconfig
```

### **local install**
Local installation copies the headers/library to a user-specified location.
```
cmake -DCMAKE_BUILD_TYPE=Release -DBUILD_SHARED_LIBS=ON -DCMAKE_INSTALL_PREFIX=install -Bbuild/default -H.
cmake --build build/default --target install
```

If you already have run cmake without setting CMAKE_INSTALL_PREFIX, you may need to clean the build first:
```
rm -rf build/default
```

## Use in ROS nodes  
Add it to the cmakelist. Here its told to look for it with the given path, or in the default system-wide lib installation path. The given path can be changed to point at the local install location of the lib, if you do not use system-wide install.   
```cmake
find_package(MAVLORA REQUIRED PATHS ${CMAKE_CURRENT_SOURCE_DIR}/../../../../install/lib/cmake/ /usr/local/lib/cmake/ NO_DEFAULT_PATH)
find_package(MAVLORA)
```